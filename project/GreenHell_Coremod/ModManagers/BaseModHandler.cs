﻿using Harmony;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace GHML
{
    public class BaseModHandler : MonoBehaviour
    {
        public static Dictionary<string, ModData> dataCache = new Dictionary<string, ModData>();

        public virtual async Task<ModData> GetModData(FileInfo file) { throw new NotImplementedException(); }

        public virtual async Task<Assembly> GetModAssembly(ModData moddata)
        {
            Assembly assembly = null;
            try
            {
                if (!moddata.modinfo.isShortcut)
                {
                    string potentialCachedVersion = Path.Combine(GHML_Main.path_cacheFolder_mods, moddata.modinfo.fileHash + ".dll");
                    if (File.Exists(potentialCachedVersion))
                    {
                        try
                        {
                            assembly = Assembly.Load(File.ReadAllBytes(potentialCachedVersion));
                            return assembly;
                        }
                        catch { }
                    }
                }

                if (ModManagerPage.loadedAssemblies.ContainsKey(moddata.modinfo.modFile.Name))
                {
                    ModManagerPage.loadedAssemblies.Remove(moddata.modinfo.modFile.Name);
                }
                moddata.modinfo.modState = ModInfo.ModStateEnum.compiling;
                ModManagerPage.RefreshModState(moddata);
                StringBuilder cmdArguments = new StringBuilder();
                string outPath = string.Format("{0}\\modassembly_{1}.dll", GHML_Main.path_cacheFolder_temp, DateTime.Now.Ticks);
                List<string> assemblies = new List<string>();
                string path = Path.Combine(Application.dataPath, "../GH_Data/Managed");
                assemblies = Directory.GetFiles(path).Where(file => file.EndsWith(".dll") && !file.Contains("System.")).ToList();
                assemblies.Add(Path.Combine(GHML_Main.path_binariesFolder, "coremod.dll"));

                cmdArguments.Append(string.Format("\"{0}\"", assemblies[0]));
                for (int i = 1; i < assemblies.Count; i++)
                {
                    cmdArguments.Append(string.Format(",\"{0}\"", assemblies[i]));
                }
                string TempDirectory = Path.Combine(GHML_Main.path_cacheFolder_temp, moddata.modinfo.modFile.Name);
                Directory.CreateDirectory(TempDirectory);
                moddata.modinfo.modFiles.ToList().Where(t => t.Key.ToLower().EndsWith(".cs")).ToList().ForEach((modfile) =>
                {
                    File.WriteAllBytes(Path.Combine(TempDirectory, Path.GetFileName(modfile.Key)), modfile.Value);
                });
                Process process = new Process();
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.FileName = RawSharp.cscPath;
                process.StartInfo.Arguments = string.Format("/r:{0} /out:\"{1}\" /preferreduilang:en-US /target:library /nostdlib \"{2}\"", cmdArguments.ToString(), outPath, TempDirectory + "\\*.cs");
                process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                process.StartInfo.RedirectStandardOutput = true;
                process.Start();
                while (process.HasExited != true) { await Task.Delay(1); }
                string output = process.StandardOutput.ReadToEnd();
                string[] outputFixed = output.Split(new string[] { "http://go.microsoft.com/fwlink/?LinkID=533240" }, StringSplitOptions.None);
                try
                {
                    assembly = Assembly.Load(File.ReadAllBytes(outPath));
                    if (!moddata.modinfo.isShortcut)
                    {
                        try
                        {
                            File.Copy(outPath, Path.Combine(GHML_Main.path_cacheFolder_mods, moddata.modinfo.fileHash + ".dll"), true);
                        }
                        catch { }
                    }
                }
                catch (Exception e)
                {
                    outputFixed[1] = Regex.Replace(outputFixed[1], @"^\s+$[\r\n]*", string.Empty, RegexOptions.Multiline);
                    if (!File.Exists(outPath))
                    {
                        Debug.LogError("[ModCompiler] " + moddata.modinfo.modFile.Name + " > The mod failed to compile!\n" + outputFixed[1].ReplaceCaseInsensitive(TempDirectory + "\\", "").TrimEnd('\n'));
                    }
                    else
                    {
                        string error = e.ToString();
                        if (e.InnerException != null)
                        {
                            error = e.InnerException.Message;
                        }
                        error.ReplaceCaseInsensitive(TempDirectory + "\\", "");
                        Debug.LogError("[ModCompiler] " + moddata.modinfo.modFile.Name + " > The mod failed to compile!\n" + error.TrimEnd('\n'));
                    }
                }
                if (Directory.Exists(TempDirectory))
                {
                    Directory.Delete(TempDirectory, true);
                }
                if (File.Exists(outPath))
                {
                    File.Delete(outPath);
                }
            }
            catch (Exception e)
            {
                Debug.LogError("[ModCompiler] " + moddata.modinfo.modFile.Name + "> The mod failed to compile!\n" + e.ToString().TrimEnd('\n'));
            }
            return assembly;
        }

        public virtual async void LoadMod(ModData moddata)
        {
            try
            {
                if (moddata == null || moddata.modinfo.modState == ModInfo.ModStateEnum.running) { return; }
                bool modHasUpdated = moddata.modinfo.isShortcut ? moddata.modinfo.fileHash != Utils.CreateSHA512ForFolder(moddata.modinfo.shortcutFolder) : moddata.modinfo.fileHash != Utils.GetFileSHA512Hash(moddata.modinfo.modFile.FullName);
                if (modHasUpdated)
                {
                    await ModManagerPage.RefreshMod(moddata.modinfo.modFile);
                    moddata = ModManagerPage.modList.Find((m) => m.modinfo.modFile.Name == moddata.modinfo.modFile.Name);
                }
                if (!ModManagerPage.ModsGameObjectParent.transform.Find(moddata.modinfo.modFile.Name.ToLower()))
                {
                    if (modHasUpdated || moddata.modinfo.assembly == null) { moddata.modinfo.assembly = await GetModAssembly(moddata); }
                    if (!ModManagerPage.loadedAssemblies.ContainsKey(moddata.modinfo.modFile.Name))
                    {
                        ModManagerPage.loadedAssemblies.Add(moddata.modinfo.modFile.Name, moddata.modinfo.assembly);
                    }
                    else
                    {
                        ModManagerPage.loadedAssemblies[moddata.modinfo.modFile.Name] = moddata.modinfo.assembly;
                    }
                    if (moddata.modinfo.assembly == null) { moddata.modinfo.modState = ModInfo.ModStateEnum.errored; }
                    else
                    {
                        IEnumerable<Type> types = moddata.modinfo.assembly.GetTypes().Where(t => t.IsSubclassOf(typeof(Mod)));
                        if (types.Count() != 1)
                        {
                            Debug.LogError("[ModManager] " + moddata.jsonmodinfo.name + "> The mod codebase doesn't specify a mod class or specifies more than one!");
                            moddata.modinfo.modState = ModInfo.ModStateEnum.errored;
                        }
                        else
                        {
                            GameObject ModObj = new GameObject();
                            ModObj.name = moddata.modinfo.modFile.Name.ToLower();
                            ModObj.transform.parent = ModManagerPage.ModsGameObjectParent.transform;
                            ModObj.AddComponent(types.FirstOrDefault());
                            moddata.modinfo.modState = ModInfo.ModStateEnum.running;
                        }
                    }
                }
                if (modHasUpdated)
                    ModManagerPage.RefreshMod(moddata.modinfo.modFile);
                else
                    ModManagerPage.RefreshModState(moddata);
            }
            catch (Exception e)
            {
                moddata.modinfo.modState = ModInfo.ModStateEnum.errored;
                Debug.LogError("[ModManager] " + moddata.jsonmodinfo.name + " > A fatal error occured while loading the mod!\nStacktrace : " + e.ToString());
            }
            GHConsole.RefreshCommands();
        }

        public virtual async void UnloadMod(ModData moddata)
        {
            try
            {
                if (moddata.modinfo.modState == ModInfo.ModStateEnum.running)
                {
                    Transform mod = ModManagerPage.ModsGameObjectParent.transform.Find(moddata.modinfo.modFile.Name.ToLower());
                    try
                    {
                        if (mod != null && moddata.modinfo.assembly != null)
                        {
                            IEnumerable<Type> types = moddata.modinfo.assembly.GetTypes().Where(t => t.IsSubclassOf(typeof(Mod)));
                            if (mod.GetComponent(types.First()))
                            {
                                MethodInfo methodInfo = AccessTools.Method(types.First(), "OnModUnload");
                                if (methodInfo != null)
                                    if (methodInfo.IsStatic) { methodInfo.Invoke(null, null); } else { methodInfo.Invoke(mod.GetComponent(types.First()), null); }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogError("[ModManager] " + moddata.jsonmodinfo.name + " > An error occured while running the mod OnModUnload() method!\nStacktrace : " + e.ToString());
                    }
                    if (mod != null)
                    {
                        Destroy(mod.gameObject);
                    }
                    if (ModManagerPage.loadedAssemblies.ContainsKey(moddata.modinfo.modFile.Name)) { ModManagerPage.loadedAssemblies.Remove(moddata.modinfo.modFile.Name); }
                    moddata.modinfo.modState = ModInfo.ModStateEnum.idle;
                    ModManagerPage.RefreshModState(moddata);
                    GHConsole.RefreshCommands();
                }
            }
            catch (Exception e)
            {
                Debug.LogError("[ModManager] " + moddata.jsonmodinfo.name + " > An error occured while unloading the mod!\nStacktrace : " + e.ToString());
            }
        }
    }
}