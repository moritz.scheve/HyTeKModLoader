﻿using GHML;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class GHNotify : MonoBehaviour
{
    GameObject content;
    GameObject _errorNotificationPrefab;
    GameObject _spinningNotificationPrefab;
    GameObject _scalingNotificationPrefab;
    GameObject _normalNotificationPrefab;

    public static Sprite ErrorSprite;
    public static GHNotify instance;
    public static Sprite LoadingSprite;
    public static Sprite CheckSprite;

    public static GHNotify Get()
    {
        return instance;
    }

    public static List<GHNotification> notifications = new List<GHNotification>();

    public static GHNotification errornotification;

    public enum NotificationType
    {
        error,
        spinning,
        scaling,
        normal
    }

    async void Start()
    {
        instance = this;
        content = transform.Find("Content").gameObject;
        _errorNotificationPrefab = await GHML_Main.bundle.TaskLoadAssetAsync<GameObject>("ErrorNotification");
        _spinningNotificationPrefab = await GHML_Main.bundle.TaskLoadAssetAsync<GameObject>("SpinningNotification");
        _scalingNotificationPrefab = await GHML_Main.bundle.TaskLoadAssetAsync<GameObject>("ScalingNotification");
        _normalNotificationPrefab = await GHML_Main.bundle.TaskLoadAssetAsync<GameObject>("NormalNotification");
        ErrorSprite = await GHML_Main.bundle.TaskLoadAssetAsync<Sprite>("IconError");
        LoadingSprite = await GHML_Main.bundle.TaskLoadAssetAsync<Sprite>("RefreshIcon");
        CheckSprite = await GHML_Main.bundle.TaskLoadAssetAsync<Sprite>("ToggleIcon");
        errornotification = AddNotification(NotificationType.error, "");
    }

    public GHNotification AddNotification(NotificationType type, int closeDelay = 0, Sprite icon = null)
    {
        return AddNotification(type, "", closeDelay, icon);
    }

    public GHNotification AddNotification(NotificationType type, string text = "", int closeDelay = 0, Sprite icon = null)
    {
        GameObject prefab = null;
        if (type == NotificationType.error)
        {
            if (errornotification != null) { return errornotification; }
            prefab = _errorNotificationPrefab;
        }
        else if (type == NotificationType.normal)
        {
            prefab = _normalNotificationPrefab;
        }
        else if (type == NotificationType.scaling)
        {
            prefab = _scalingNotificationPrefab;
        }
        else if (type == NotificationType.spinning)
        {
            prefab = _spinningNotificationPrefab;
        }

        GameObject nobj = Instantiate(prefab, content.transform);
        GHNotification n = nobj.AddComponent<GHNotification>();

        n.closeDelay = closeDelay;
        if (closeDelay > 0)
        {
            n.StartCoroutine(n.AutoClose());
        }

        n.text = n.transform.Find("Message").GetComponent<Text>();
        n.icon = n.transform.Find("Icon").GetComponent<Image>();
        n.Background = n.GetComponent<RawImage>();

        if (text != "")
            n.text.text = text;
        if (icon != null)
            n.icon.sprite = icon;

        if (type == NotificationType.error)
        {
            n.StartCoroutine(n.ErrorChecker());
            n.SetText("").Hide();
        }
        else
        {
            notifications.Add(n);
        }
        return n;
    }

    public static void ClearNotifications()
    {
        foreach (GHNotification rn in notifications)
        {
            if (rn != null)
                rn.Close();
        }
        notifications.Clear();
    }
}

public class GHNotification : MonoBehaviour
{
    public int closeDelay = 0;

    public bool isError;
    public int errorAmount;
    public DateTime lastError = DateTime.Now;
    public Text text;
    public Image icon;
    public RawImage Background;
    
    public async void AddNewError()
    {
        lastError = DateTime.Now;
        errorAmount++;
        SetText("An error occured! (x"+errorAmount+")");
        Show();
        await Task.Delay(0);
    }

    public IEnumerator ErrorChecker()
    {
        yield return new WaitForSeconds(2);
        if (lastError.AddSeconds(2) < DateTime.Now)
        {
            if (errorAmount > 0)
            {
                errorAmount = 0;
                Hide();
            }
        }
        StartCoroutine(ErrorChecker());
    }

    public void Hide()
    {
        GetComponent<Animation>().Play("NotifyClose");
    }

    public void Show()
    {
        if (errorAmount == 1)
        {
            GetComponent<Animation>().Play("NotifyOpen");
        }
    }

    public IEnumerator AutoClose()
    {
        if(GHNotify.errornotification == this) { yield break; }
        yield return new WaitForSeconds(closeDelay);
        GetComponent<Animation>().Play("NotifyClose");
        yield return new WaitForSeconds(0.20f);
        Destroy(gameObject);
        GHNotify.notifications.Remove(this);
        yield break;
    }

    public void Close()
    {
        if (GHNotify.errornotification == this) { return; }
        closeDelay = 0;
        StartCoroutine(AutoClose());
    }

    public GHNotification SetColor(Color color)
    {
        Background.color = color;
        return this;
    }

    public GHNotification SetText(string val)
    {
        text.text = val;
        return this;
    }

    public GHNotification SetIcon(Sprite val)
    {
        icon.sprite = val;
        return this;
    }

    public GHNotification SetNormal()
    {
        if (icon.GetComponent<Animation>())
        {
            Destroy(icon.GetComponent<Animation>());
            icon.GetComponent<RectTransform>().rotation = Quaternion.identity;
            icon.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        }
        return this;
    }

    public GHNotification SetFontSize(int size = 10)
    {
        if (size != 10)
            text.fontSize = size;
        return this;
    }

    public GHNotification SetTextColor(Color color = default)
    {
        if (color != default)
            text.color = color;
        return this;
    }

    public GHNotification SetCloseDelay(int delay)
    {
        if (GHNotify.errornotification == this) { return this; }
        StopAllCoroutines();
        if (delay > 0)
        {
            closeDelay = delay;
            StartCoroutine(AutoClose());
        }
        return this;
    }
}
