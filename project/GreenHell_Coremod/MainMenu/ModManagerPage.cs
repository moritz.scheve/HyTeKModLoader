﻿using ICSharpCode.SharpZipLib.Zip;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

namespace GHML
{
    public class ModManagerPage : MenuPage
    {
        public static ModManagerPage instance;
        public static List<ModData> modList = new List<ModData>();
        public static Dictionary<string, Assembly> loadedAssemblies = new Dictionary<string, Assembly>();
        public static Transform ModListContent;
        public static GameObject ModEntryPrefab;
        public static Button LoadModsBtn;
        public static Button UnloadModsBtn;
        public static Button ModListRefreshBtn;
        public static GameObject ModsGameObjectParent;
        public static bool bypassValueChangeCheckAll;
        public static GameObject modInfoObj;
        public static GameObject noModsText;
        public static Color greenColor = new Color(67.0f / 255, 181.0f / 255, 129.0f / 255);
        public static Color orangeColor = new Color(181.0f / 255, 135.0f / 255, 67.0f / 255);
        public static Color redColor = new Color(181.0f / 255, 67.0f / 255, 67.0f / 255);
        public static Color blueColor = new Color(67.0f / 255, 129.0f / 255, 181.0f / 255);
        public static Color yellowColor = new Color(232.0f / 255, 226.0f / 255, 58.0f / 255);
        public static bool canRefreshModlist = true;

        static int listRefreshAmount = 0;
        public static PermanentModsList UserPermanentMods;
        public static PermanentModsList OnStartUserPermanentMods;

        public override async void Initialize()
        {
            instance = this;
            ModsGameObjectParent = new GameObject();
            ModsGameObjectParent.name = "GHMLModsPrefabs";
            DontDestroyOnLoad(ModsGameObjectParent);
            modInfoObj = transform.Find("ModScrollView").Find("Viewport").Find("ModInfo").gameObject;
            modInfoObj.SetActive(false);
            noModsText = transform.Find("ModScrollView").Find("Viewport").Find("GHML_FindMods").gameObject;
            noModsText.GetComponent<Button>().onClick.AddListener(() => Application.OpenURL("https://www.greenhellmodding.com/mods"));
            noModsText.SetActive(false);
            ModEntryPrefab = await GHML_Main.bundle.TaskLoadAssetAsync<GameObject>("ModEntry");
            ModListContent = transform.Find("ModScrollView").Find("Viewport").Find("GHMLModManager_ModListContent");
            transform.Find("InfoBar").Find("GHML_CheckAllToggle").GetComponent<Toggle>().onValueChanged.AddListener(var => SelectAllMods(var));
            LoadModsBtn = transform.Find("GHML_LoadModsBtn").GetComponent<Button>();
            LoadModsBtn.onClick.AddListener(LoadSelectedMods);
            UnloadModsBtn = transform.Find("GHML_UnloadModsBtn").GetComponent<Button>();
            UnloadModsBtn.onClick.AddListener(UnloadSelectedMods);
            ModListRefreshBtn = transform.Find("Buttons").Find("GHML_RefreshModsBtn").GetComponent<Button>();
            ModListRefreshBtn.onClick.AddListener(() => RefreshMods());
            transform.Find("Buttons").Find("GHML_OpenModsFolderBtn").GetComponent<Button>().onClick.AddListener(OpenModsFolder);
            RefreshMods();
        }

        public static void OpenModsFolder()
        {
            Process.Start("explorer.exe", Path.GetFullPath(GHML_Main.path_modsFolder));
        }

        public static void RefreshModsStates()
        {
            modList.ForEach(m => RefreshModState(m));
        }

        public static void RefreshModState(ModData md)
        {
            if (md.modinfo.ModlistEntry == null) { return; }
            if (md.modinfo.modState == ModInfo.ModStateEnum.running)
            {
                if (!md.jsonmodinfo.isModPermanent)
                {
                    md.modinfo.permanentModWarning.SetActive(false);
                    md.modinfo.unloadBtn.SetActive(true);
                    md.modinfo.loadBtn.SetActive(false);
                    if (OnStartUserPermanentMods.list.Contains(md.modinfo.modFile.Name.ToLower()))
                    {
                        md.modinfo.permanentModWarning.SetActive(true);
                        ColorBlock block = md.modinfo.permanentModWarning.transform.GetComponent<Button>().colors;
                        block.disabledColor = new Color(84.0f / 255.0f, 166.0f / 255.0f, 87.0f / 255.0f, 200.0f / 255.0f);
                        md.modinfo.permanentModWarning.transform.GetComponent<Button>().colors = block;
                        md.modinfo.permanentModWarning.transform.Find("Tooltip").Find("Text").GetComponent<Text>().text = "Loaded at startup";
                    }
                }
                else
                {
                    md.modinfo.unloadBtn.SetActive(false);
                    md.modinfo.loadBtn.SetActive(false);
                    md.modinfo.permanentModWarning.SetActive(true);
                }
            }
            else
            {
                if (!md.jsonmodinfo.isModPermanent)
                {
                    md.modinfo.permanentModWarning.SetActive(false);
                    md.modinfo.unloadBtn.SetActive(false);
                    md.modinfo.loadBtn.SetActive(true);
                    if (OnStartUserPermanentMods.list.Contains(md.modinfo.modFile.Name.ToLower()))
                    {
                        md.modinfo.permanentModWarning.SetActive(true);
                        ColorBlock block = md.modinfo.permanentModWarning.transform.GetComponent<Button>().colors;
                        block.disabledColor = new Color(84.0f / 255.0f, 166.0f / 255.0f, 87.0f / 255.0f, 200.0f / 255.0f);
                        md.modinfo.permanentModWarning.transform.GetComponent<Button>().colors = block;
                        md.modinfo.permanentModWarning.transform.Find("Tooltip").Find("Text").GetComponent<Text>().text = "Loaded at startup";
                    }
                }
                else
                {
                    md.modinfo.unloadBtn.SetActive(false);
                    md.modinfo.loadBtn.SetActive(false);
                    if (md.modinfo.modState != ModInfo.ModStateEnum.needrestart)
                        md.modinfo.permanentModWarning.SetActive(true);
                }
            }

            Text Statustext = md.modinfo.ModlistEntry.transform.Find("ModStatusText").GetComponent<Text>();
            switch (md.modinfo.modState)
            {
                case ModInfo.ModStateEnum.running:
                    Statustext.text = "Running...";
                    Statustext.color = greenColor;
                    break;
                case ModInfo.ModStateEnum.errored:
                    Statustext.text = "Errored";
                    Statustext.color = redColor;
                    break;
                case ModInfo.ModStateEnum.idle:
                    Statustext.text = "Not loaded";
                    Statustext.color = blueColor;
                    break;
                case ModInfo.ModStateEnum.compiling:
                    Statustext.text = "Compiling...";
                    Statustext.color = yellowColor;
                    break;
                case ModInfo.ModStateEnum.needrestart:
                    Statustext.text = "Needs Game Restart";
                    Statustext.color = redColor;
                    break;
            }
        }

        public async static void LoadSelectedMods()
        {
            foreach (ModData md in modList.ToArray())
            {
                if (md.jsonmodinfo.isModPermanent) { continue; }
                if (md.modinfo.ModlistEntry.GetComponentInChildren<Toggle>().isOn)
                {
                    md.modinfo.modHandler.LoadMod(md);
                }
                await Task.Delay(1);
            }
        }

        public async static void UnloadSelectedMods()
        {
            foreach (ModData md in modList.ToArray())
            {
                if (md.jsonmodinfo.isModPermanent) { continue; }
                if (md.modinfo.ModlistEntry.GetComponentInChildren<Toggle>().isOn)
                {
                    md.modinfo.modHandler.UnloadMod(md);
                }
                await Task.Delay(1);
            }
        }

        public static void SelectAllMods(bool var)
        {
            if (!bypassValueChangeCheckAll)
            {
                foreach (ModData md in modList.ToArray())
                {
                    md.modinfo.ModlistEntry.GetComponentInChildren<Toggle>().isOn = var;
                }
            }
            else
            {
                bypassValueChangeCheckAll = false;
            }
        }

        public static async void RefreshMods()
        {
            if (!canRefreshModlist) { return; }
            listRefreshAmount++;
            canRefreshModlist = false;
            noModsText.SetActive(true);
            foreach (Transform t in ModsGameObjectParent.transform)
            {
                bool found = false;
                foreach (ModData moddata in modList.ToArray())
                {
                    if (moddata.modinfo.modFile.Name.ToLower() == t.gameObject.name)
                    {
                        found = true;
                    }
                }
                if (!found)
                {
                    Destroy(t.gameObject);
                }
            }
            modList.Clear();
            LoadModsBtn.interactable = false;
            UnloadModsBtn.interactable = false;
            ModListRefreshBtn.interactable = false;
            foreach (Transform t in ModListContent.transform)
            {
                Destroy(t.gameObject);
            }
            DirectoryInfo d = new DirectoryInfo(Path.Combine(Application.dataPath, "..\\mods"));
            FileInfo[] mods = d.GetFiles("*", SearchOption.TopDirectoryOnly);
            foreach (FileInfo file in mods)
            {
                UnityMainThreadDispatcher.Instance().Enqueue(async () =>
                {
                    try
                    {
                        RefreshMod(file);
                    }
                    catch (Exception e)
                    {
                        Debug.LogError("[ModManager] " + file.Name + " > A fatal error occured while loading the mod!\nStacktrace : " + e.ToString());
                    }
                });
                await Task.Delay(1);
            }
            LoadModsBtn.interactable = true;
            UnloadModsBtn.interactable = true;
            ModListRefreshBtn.interactable = true;
            RefreshModsStates();
            canRefreshModlist = true;
        }

        public static async Task RefreshMod(FileInfo file)
        {
            try
            {
                ModData _md = modList.Where(m => m.modinfo.modFile == file).FirstOrDefault();
                if (_md != null && _md.modinfo.ModlistEntry != null)
                {
                    Destroy(_md.modinfo.ModlistEntry);
                    modList.Remove(_md);
                }
                ModData md = null;
                string filename = file.Name.ToLower();
                if (filename.EndsWith(".ghmod"))
                {
                    md = await new ZipModHandler().GetModData(file);
                }
                else if (filename.EndsWith(".lnk"))
                {
                    md = await new FolderModHandler().GetModData(file);
                }
                if (md == null) { return; }
                if (md.jsonmodinfo == null)
                {
                    Debug.LogError("[ModManager] " + file.Name + " > The mod is missing a modinfo.json file!");
                    return;
                }

                md.modinfo.ModlistEntry = Instantiate(ModEntryPrefab, ModListContent.transform.position, ModListContent.transform.rotation, ModListContent.transform);

                if (ModsGameObjectParent.transform.Find(md.modinfo.modFile.Name.ToLower()))
                {
                    md.modinfo.modState = ModInfo.ModStateEnum.running;
                }
                else
                {
                    if (md.modinfo.modState != ModInfo.ModStateEnum.errored)
                    {
                        md.modinfo.modState = ModInfo.ModStateEnum.idle;
                    }
                }

                md.modinfo.ModlistEntry.transform.Find("ModName").GetComponent<Text>().text = (md.modinfo.isShortcut ? "✦ " : "") + Utils.StripRichText(md.jsonmodinfo.name);
                md.modinfo.ModlistEntry.transform.Find("ModName").GetComponent<Text>().color = md.modinfo.isShortcut ? new Color(40f / 255f, 199f / 255f, 69f / 255f) : Color.white;
                md.modinfo.ModlistEntry.transform.Find("ModAuthor").GetComponent<Text>().text = Utils.StripRichText(md.jsonmodinfo.author);
                md.modinfo.ModlistEntry.transform.Find("ModVersionText").GetComponent<Text>().text = Utils.StripRichText(md.jsonmodinfo.version);
                md.modinfo.buttons = md.modinfo.ModlistEntry.transform.Find("Buttons").gameObject;
                md.modinfo.infoBtn = md.modinfo.buttons.transform.Find("ModInfoBtn").gameObject;
                md.modinfo.infoBtnTooltip = md.modinfo.buttons.transform.Find("ModInfoBtn").Find("Tooltip").gameObject;
                md.modinfo.infoBtn.AddComponent<TooltipHandler>().tooltip = md.modinfo.infoBtnTooltip;
                md.modinfo.permanentModWarning = md.modinfo.buttons.transform.Find("PermanentModWarning").gameObject;
                md.modinfo.permanentTooltip = md.modinfo.buttons.transform.Find("PermanentModWarning").Find("Tooltip").gameObject;
                md.modinfo.permanentModWarning.AddComponent<TooltipHandler>().tooltip = md.modinfo.permanentTooltip;
                md.modinfo.loadBtn = md.modinfo.buttons.transform.Find("LoadModBtn").gameObject;
                md.modinfo.loadBtnTooltip = md.modinfo.buttons.transform.Find("LoadModBtn").Find("Tooltip").gameObject;
                md.modinfo.loadBtn.AddComponent<TooltipHandler>().tooltip = md.modinfo.loadBtnTooltip;
                md.modinfo.unloadBtn = md.modinfo.buttons.transform.Find("UnloadModBtn").gameObject;
                md.modinfo.unloadBtnTooltip = md.modinfo.buttons.transform.Find("UnloadModBtn").Find("Tooltip").gameObject;
                md.modinfo.unloadBtn.AddComponent<TooltipHandler>().tooltip = md.modinfo.unloadBtnTooltip;
                md.modinfo.versionTooltip = md.modinfo.ModlistEntry.transform.Find("ModVersionText").Find("VersionTooltip").gameObject;

                md.modinfo.ModlistEntry.transform.Find("ModVersionText").gameObject.AddComponent<TooltipHandler>().tooltip = md.modinfo.versionTooltip;

                md.modinfo.infoBtnTooltip.SetActive(false);
                md.modinfo.loadBtnTooltip.SetActive(false);
                md.modinfo.unloadBtnTooltip.SetActive(false);
                md.modinfo.versionTooltip.SetActive(false);
                md.modinfo.permanentTooltip.SetActive(false);
                md.modinfo.permanentModWarning.SetActive(false);
                md.modinfo.loadBtn.SetActive(false);
                md.modinfo.unloadBtn.SetActive(false);
                if (md.modinfo.modFiles.ContainsKey(md.jsonmodinfo.icon))
                {
                    Texture2D potentialIcon = new Texture2D(2, 2);
                    potentialIcon.LoadImage(md.modinfo.modFiles[md.jsonmodinfo.icon]);
                    md.modinfo.ModIcon = potentialIcon;
                    md.modinfo.ModlistEntry.transform.Find("ModIconMask").Find("ModIcon").GetComponent<RawImage>().texture = potentialIcon;
                }
                else
                {
                    md.modinfo.ModIcon = GHML_Main.missingTexture;
                    md.modinfo.ModlistEntry.transform.Find("ModIconMask").Find("ModIcon").GetComponent<RawImage>().texture = GHML_Main.missingTexture;
                }
                if (md.modinfo.modFiles.ContainsKey(md.jsonmodinfo.banner))
                {
                    Texture2D potentialBanner = new Texture2D(2, 2);
                    potentialBanner.LoadImage(md.modinfo.modFiles[md.jsonmodinfo.banner]);
                    md.modinfo.ModBanner = potentialBanner;
                }
                else
                {
                    md.modinfo.ModBanner = GHML_Main.missingTexture;
                }

                Utils.GetModVersion(md.jsonmodinfo.updateUrl).ContinueWith((t) =>
                {
                    UnityMainThreadDispatcher.Instance().Enqueue(async () =>
                    {
                        try
                        {
                            if (t.Result == "Unknown" || t.Result == "")
                            {
                                md.modinfo.versionTooltip.GetComponentInChildren<Text>().text = "Unknown";
                                md.modinfo.ModlistEntry.transform.Find("ModVersionText").GetComponent<Text>().color = orangeColor;
                                md.modinfo.versionTooltip.GetComponentInChildren<Text>().color = orangeColor;
                            }
                            else
                            {
                                if (t.Result != md.jsonmodinfo.version)
                                {
                                    md.modinfo.versionTooltip.GetComponentInChildren<Text>().text = "New version available!";
                                    Debug.LogWarning("[ModManager] " + md.jsonmodinfo.name + " > The current installed version is outdated! A new version is available! (" + t.Result + ")");
                                    GHNotify.Get().AddNotification(GHNotify.NotificationType.scaling, "<color=#41f46b>" + md.jsonmodinfo.name + " has a new version available!</color>", 5, await GHML_Main.bundle.TaskLoadAssetAsync<Sprite>("DownloadIcon"));
                                    md.modinfo.ModlistEntry.transform.Find("ModVersionText").GetComponent<Text>().color = redColor;
                                    md.modinfo.versionTooltip.GetComponentInChildren<Text>().color = redColor;
                                }
                                else
                                {
                                    md.modinfo.versionTooltip.GetComponentInChildren<Text>().text = "Up to date!";
                                    md.modinfo.ModlistEntry.transform.Find("ModVersionText").GetComponent<Text>().color = greenColor;
                                    md.modinfo.versionTooltip.GetComponentInChildren<Text>().color = greenColor;
                                }
                            }
                        }
                        catch { }
                    });
                });

                md.modinfo.unloadBtn.GetComponent<Button>().onClick.AddListener(() => md.modinfo.modHandler.UnloadMod(md));
                md.modinfo.loadBtn.GetComponent<Button>().onClick.AddListener(() => md.modinfo.modHandler.LoadMod(md));
                md.modinfo.infoBtn.GetComponent<Button>().onClick.AddListener(() =>
                {
                    ShowModInfo(md);
                });

                if ((md.jsonmodinfo.isModPermanent || (OnStartUserPermanentMods.list.Contains(md.modinfo.modFile.Name.ToLower()))) && listRefreshAmount == 1)
                {
                    md.modinfo.modHandler.LoadMod(md);
                }
                else if (md.jsonmodinfo.isModPermanent && listRefreshAmount > 1 && md.modinfo.modState == ModInfo.ModStateEnum.idle)
                {
                    md.modinfo.modState = ModInfo.ModStateEnum.needrestart;
                }

                RefreshModState(md);
                modList.Add(md);
                noModsText.SetActive(false);
                
            }
            catch (Exception e)
            {
                Debug.LogError("[ModManager] " + file.Name + " > A fatal error occured while loading the mod!\nStacktrace : " + e.ToString());
            }
        }

        public static ModData[] GetModDependencies(List<string> b)
        {
            List<ModData> dependencies = new List<ModData>();

            foreach (string a in b)
            {
                foreach (ModData md in modList.ToArray())
                {
                    if (md.jsonmodinfo.name == a.ToLower())
                    {
                        dependencies.Add(md);
                    }
                }
            }
            return dependencies.ToArray();
        }

        public static void ShowModInfo(ModData md)
        {
            modInfoObj.transform.Find("ModName").GetComponent<Text>().text = md.jsonmodinfo.name;
            modInfoObj.transform.Find("Author").GetComponent<Text>().text = "Author : " + md.jsonmodinfo.author;
            modInfoObj.transform.Find("ModVersion").GetComponent<Text>().text = "Version : " + md.jsonmodinfo.version;
            modInfoObj.transform.Find("GreenHellVersion").GetComponent<Text>().text = "Green Hell Version : " + md.jsonmodinfo.gameVersion;
            modInfoObj.transform.Find("BannerMask").Find("Banner").GetComponent<RawImage>().texture = md.modinfo.ModBanner;
            modInfoObj.transform.Find("BannerMask").Find("IconMask").Find("Icon").GetComponent<RawImage>().texture = md.modinfo.ModIcon;
            modInfoObj.transform.Find("Description").GetComponent<Text>().text = "Description : \n\n" + md.jsonmodinfo.description;
            modInfoObj.transform.Find("MakePermanent").gameObject.SetActive(!md.jsonmodinfo.isModPermanent);
            modInfoObj.transform.Find("MakePermanent").GetComponent<Toggle>().onValueChanged.RemoveAllListeners();
            modInfoObj.transform.Find("MakePermanent").GetComponent<Toggle>().isOn = UserPermanentMods.list.Contains(md.modinfo.modFile.Name.ToLower());
            modInfoObj.transform.Find("MakePermanent").GetComponent<Toggle>().onValueChanged.AddListener((val) =>
            {
                SetModPermanent(val, md.modinfo.modFile.Name.ToLower());
            });
            modInfoObj.SetActive(true);
        }

        public static void SetModPermanent(bool val, string fileName)
        {
            if (val)
            {
                if (!UserPermanentMods.list.Contains(fileName))
                {
                    UserPermanentMods.list.Add(fileName);
                }
            }
            else
            {
                if (UserPermanentMods.list.Contains(fileName))
                {
                    UserPermanentMods.list.Remove(fileName);
                }
            }

            PlayerPrefs.SetString("ghmlSettings_UserPermanentMods", JsonUtility.ToJson(UserPermanentMods));
        }
    }

    [Serializable]
    public class ModData
    {
        public JsonModInfo jsonmodinfo = new JsonModInfo();
        public ModInfo modinfo = new ModInfo();
    }

    [Serializable]
    public class JsonModInfo
    {
        public string name = "Default Mod Name";
        public string author = "Unknown";
        public string description = "";
        public string version = "1.0";
        public string license = "GNU AGPLv3";
        public string icon = "";
        public string banner = "";
        public string gameVersion = "1.0";
        public string updateUrl = "";
        public bool isModPermanent = false;
        public List<string> excludedFiles = new List<string>() { "*.csproj", "*.cache", "*.sln" };
    }

    [Serializable]
    public class ModInfo
    {
        public BaseModHandler modHandler;
        public FileInfo modFile;
        public string fileHash;
        public string shortcutFolder;
        public GameObject ModlistEntry;
        public Assembly assembly;
        public string assemblyFile;
        public Dictionary<string, byte[]> modFiles = new Dictionary<string, byte[]>();

        public ModStateEnum modState;
        public enum ModStateEnum
        {
            idle,
            running,
            errored,
            compiling,
            needrestart
        }

        public bool isShortcut;

        public Texture2D ModIcon;
        public Texture2D ModBanner;
        public GameObject buttons;
        public GameObject infoBtn;
        public GameObject loadBtn;
        public GameObject unloadBtn;
        public GameObject permanentModWarning;
        public GameObject permanentTooltip;
        public GameObject infoBtnTooltip;
        public GameObject loadBtnTooltip;
        public GameObject unloadBtnTooltip;
        public GameObject versionTooltip;
    }

    [Serializable]
    public class PermanentModsList
    {
        public List<string> list = new List<string>();
    }

}