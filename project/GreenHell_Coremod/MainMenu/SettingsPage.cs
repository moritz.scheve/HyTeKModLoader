﻿using Microsoft.Win32;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
namespace GHML
{
    public class SettingsPage : MenuPage
    {
        public static SettingsPage instance;
        public static bool EnableMenuInGame;
        Toggle ShowMenuOnStart;
        public static GameObject setting_consolekeybind;
        public static GameObject setting_menukeybind;

        public override async void Initialize()
        {
            ShowMenuOnStart = transform.Find("Slots_SETTINGS").Find("GHMLSetting_ShowMainMenuOnStartup").GetComponent<Toggle>();
            instance = this;

            setting_consolekeybind = transform.Find("Slots_KEYBINDS").Find("GHMLSetting_Consolekeybind").gameObject;
            setting_menukeybind = transform.Find("Slots_KEYBINDS").Find("GHMLSetting_Menukeybind").gameObject;

            if (PlayerPrefs.HasKey("ghmlSettings_StartInModManagerTab"))
            {
                if (PlayerPrefs.GetString("ghmlSettings_StartInModManagerTab") == "True")
                {
                    transform.Find("Slots_SETTINGS").Find("GHMLSetting_StartInModManagerTab").GetComponent<Toggle>().isOn = true;
                }
                else
                {
                    transform.Find("Slots_SETTINGS").Find("GHMLSetting_StartInModManagerTab").GetComponent<Toggle>().isOn = false;
                }
            }
            else
            {
                PlayerPrefs.SetString("ghmlSettings_StartInModManagerTab", "False");
            }

            if (PlayerPrefs.HasKey("ghmlSettings_EnableThisMenuIngame"))
            {
                if (PlayerPrefs.GetString("ghmlSettings_EnableThisMenuIngame") == "True")
                {
                    transform.Find("Slots_SETTINGS").Find("GHMLSetting_EnableThisMenuIngame").GetComponent<Toggle>().isOn = true;
                    EnableMenuInGame = true;
                }
                else
                {
                    transform.Find("Slots_SETTINGS").Find("GHMLSetting_EnableThisMenuIngame").GetComponent<Toggle>().isOn = false;
                    EnableMenuInGame = false;
                }
            }
            else
            {
                PlayerPrefs.SetString("ghmlSettings_EnableThisMenuIngame", "True");
                EnableMenuInGame = true;
            }

            if (PlayerPrefs.HasKey("ghmlSettings_UserPermanentMods"))
            {
                try
                {
                    ModManagerPage.OnStartUserPermanentMods = JsonUtility.FromJson<PermanentModsList>(PlayerPrefs.GetString("ghmlSettings_UserPermanentMods"));
                }
                catch
                {
                    ModManagerPage.OnStartUserPermanentMods = new PermanentModsList();
                    PlayerPrefs.SetString("ghmlSettings_UserPermanentMods", JsonUtility.ToJson(ModManagerPage.OnStartUserPermanentMods));
                }
            }
            else
            {
                ModManagerPage.OnStartUserPermanentMods = new PermanentModsList();
                PlayerPrefs.SetString("ghmlSettings_UserPermanentMods", JsonUtility.ToJson(ModManagerPage.OnStartUserPermanentMods));
            }
            ModManagerPage.UserPermanentMods = new PermanentModsList();
            ModManagerPage.UserPermanentMods.list = ModManagerPage.OnStartUserPermanentMods.list.ToArray().ToList();

            transform.Find("Slots_SETTINGS").Find("GHMLSetting_StartInModManagerTab").GetComponent<Toggle>().onValueChanged.AddListener(var => PlayerPrefs.SetString("ghmlSettings_StartInModManagerTab", var.ToString()));
            transform.Find("Slots_SETTINGS").Find("GHMLSetting_EnableThisMenuIngame").GetComponent<Toggle>().onValueChanged.AddListener(var => ToggleEnableThisMenuIngame(var));
            transform.Find("Slots_SETTINGS").Find("GHMLSetting_ShowMainMenuOnStartup").GetComponent<Toggle>().onValueChanged.AddListener(var => PlayerPrefs.SetString("ghmlSettings_ShowMainMenuOnStart", var.ToString()));

            if (transform.Find("Slots_SETTINGS").Find("GHMLSetting_StartInModManagerTab").GetComponent<Toggle>().isOn)
            {
                GHMainMenu.ChangeMenu("ModManager");
            }
            else
            {
                GHMainMenu.ChangeMenu("Home");
            }

            if (PlayerPrefs.HasKey("ghmlSettings_ShowMainMenuOnStart"))
            {
                if (PlayerPrefs.GetString("ghmlSettings_ShowMainMenuOnStart") != "True")
                {
                    ShowMenuOnStart.isOn = false;
                    GHMainMenu.CloseMenu();
                }
            }
            else
            {
                PlayerPrefs.SetString("ghmlSettings_ShowMainMenuOnStart", "True");
            }

            if (PlayerPrefs.HasKey("ghmlSettings_ConsoleKeybind"))
            {
                KeyCode keycode = Utils.KeyCodeFromString(PlayerPrefs.GetString("ghmlSettings_ConsoleKeybind"));
                if (keycode != KeyCode.None)
                {
                    GHML_Main.ConsoleKey = keycode;
                }
                else
                {
                    PlayerPrefs.SetString("ghmlSettings_ConsoleKeybind", "F10");
                    GHML_Main.MenuKey = KeyCode.F10;
                }
            }
            else
            {
                PlayerPrefs.SetString("ghmlSettings_ConsoleKeybind", "F10");
                GHML_Main.MenuKey = KeyCode.F10;
            }

            if (PlayerPrefs.HasKey("ghmlSettings_MenuKeybind"))
            {
                KeyCode keycode = Utils.KeyCodeFromString(PlayerPrefs.GetString("ghmlSettings_MenuKeybind"));
                if (keycode != KeyCode.None)
                {
                    GHML_Main.MenuKey = keycode;
                }
                else
                {
                    PlayerPrefs.SetString("ghmlSettings_MenuKeybind", "F9");
                    GHML_Main.MenuKey = KeyCode.F9;
                }
            }
            else
            {
                PlayerPrefs.SetString("ghmlSettings_MenuKeybind", "F9");
                GHML_Main.MenuKey = KeyCode.F9;
            }

            setting_consolekeybind.GetComponentInChildren<Dropdown>().onValueChanged.AddListener(OnConsoleKeybindChange);
            setting_menukeybind.GetComponentInChildren<Dropdown>().onValueChanged.AddListener(OnMenuKeybindChange);

            UpdateSettingsKeybinds();
        }

        void OnConsoleKeybindChange(int val)
        {
            KeyCode consoleKeycode = KeyCode.F10;
            string keycode = setting_consolekeybind.GetComponentInChildren<Dropdown>().options[val].text;
            consoleKeycode = Utils.KeyCodeFromString(keycode);
            if (consoleKeycode != KeyCode.None)
            {
                if (consoleKeycode != GHML_Main.MenuKey)
                {
                    PlayerPrefs.SetString("ghmlSettings_ConsoleKeybind", keycode);
                    GHML_Main.ConsoleKey = consoleKeycode;
                }
                else
                {
                    PlayerPrefs.SetString("ghmlSettings_ConsoleKeybind", "F10");
                    GHML_Main.ConsoleKey = KeyCode.F10;
                    PlayerPrefs.SetString("ghmlSettings_MenuKeybind", "F9");
                    GHML_Main.MenuKey = KeyCode.F9;
                }
            }
            else
            {
                PlayerPrefs.SetString("ghmlSettings_ConsoleKeybind", "F10");
                GHML_Main.ConsoleKey = KeyCode.F10;
                PlayerPrefs.SetString("ghmlSettings_MenuKeybind", "F9");
                GHML_Main.MenuKey = KeyCode.F9;
            }

            UpdateSettingsKeybinds();
        }

        void OnMenuKeybindChange(int val)
        {
            KeyCode menuKeycode = KeyCode.F9;
            string keycode = setting_menukeybind.GetComponentInChildren<Dropdown>().options[val].text;
            menuKeycode = Utils.KeyCodeFromString(keycode);
            if (menuKeycode != KeyCode.None)
            {
                if (menuKeycode != GHML_Main.ConsoleKey)
                {
                    PlayerPrefs.SetString("ghmlSettings_MenuKeybind", keycode);
                    GHML_Main.MenuKey = menuKeycode;
                }
                else
                {
                    PlayerPrefs.SetString("ghmlSettings_ConsoleKeybind", "F10");
                    GHML_Main.ConsoleKey = KeyCode.F10;
                    PlayerPrefs.SetString("ghmlSettings_MenuKeybind", "F9");
                    GHML_Main.MenuKey = KeyCode.F9;
                }
            }
            else
            {
                PlayerPrefs.SetString("ghmlSettings_ConsoleKeybind", "F10");
                GHML_Main.ConsoleKey = KeyCode.F10;
                PlayerPrefs.SetString("ghmlSettings_MenuKeybind", "F9");
                GHML_Main.MenuKey = KeyCode.F9;
            }

            UpdateSettingsKeybinds();
        }

        void UpdateSettingsKeybinds()
        {
            int consoleValue = 0;
            switch (GHML_Main.ConsoleKey)
            {
                case KeyCode.F1:
                    consoleValue = 0;
                    break;
                case KeyCode.F2:
                    consoleValue = 1;
                    break;
                case KeyCode.F3:
                    consoleValue = 2;
                    break;
                case KeyCode.F4:
                    consoleValue = 3;
                    break;
                case KeyCode.F5:
                    consoleValue = 4;
                    break;
                case KeyCode.F6:
                    consoleValue = 5;
                    break;
                case KeyCode.F7:
                    consoleValue = 6;
                    break;
                case KeyCode.F8:
                    consoleValue = 7;
                    break;
                case KeyCode.F9:
                    consoleValue = 8;
                    break;
                case KeyCode.F10:
                    consoleValue = 9;
                    break;
                case KeyCode.F11:
                    consoleValue = 10;
                    break;
                case KeyCode.F12:
                    consoleValue = 11;
                    break;
                default:
                    consoleValue = 9;
                    PlayerPrefs.SetString("ghmlSettings_ConsoleKeybind", "F10");
                    GHML_Main.MenuKey = KeyCode.F10;
                    break;
            }
            setting_consolekeybind.GetComponentInChildren<Dropdown>().value = consoleValue;

            int mainmenuValue = 0;
            switch (GHML_Main.MenuKey)
            {
                case KeyCode.F1:
                    mainmenuValue = 0;
                    break;
                case KeyCode.F2:
                    mainmenuValue = 1;
                    break;
                case KeyCode.F3:
                    mainmenuValue = 2;
                    break;
                case KeyCode.F4:
                    mainmenuValue = 3;
                    break;
                case KeyCode.F5:
                    mainmenuValue = 4;
                    break;
                case KeyCode.F6:
                    mainmenuValue = 5;
                    break;
                case KeyCode.F7:
                    mainmenuValue = 6;
                    break;
                case KeyCode.F8:
                    mainmenuValue = 7;
                    break;
                case KeyCode.F9:
                    mainmenuValue = 8;
                    break;
                case KeyCode.F10:
                    mainmenuValue = 9;
                    break;
                case KeyCode.F11:
                    mainmenuValue = 10;
                    break;
                case KeyCode.F12:
                    mainmenuValue = 11;
                    break;
                default:
                    mainmenuValue = 8;
                    PlayerPrefs.SetString("ghmlSettings_MenuKeybind", "F9");
                    GHML_Main.MenuKey = KeyCode.F9;
                    break;
            }

            setting_menukeybind.GetComponentInChildren<Dropdown>().value = mainmenuValue;


            GHMainMenu.VersionText.GetComponentInChildren<Text>().text = @"GreenHellModLoader <color=#36d1a8>v" + GHML_Main.modloader_config.coreVersion + @"</color>
Press <color=#36d1a8>" + GHML_Main.MenuKey.ToString() + "</color> to open the main menu\n" +
    "Press <color=#36d1a8>" + GHML_Main.ConsoleKey.ToString() + "</color> to open the console";

        }

        void ToggleEnableThisMenuIngame(bool var)
        {
            PlayerPrefs.SetString("ghmlSettings_EnableThisMenuIngame", var.ToString());
            EnableMenuInGame = var;
        }
    }
}