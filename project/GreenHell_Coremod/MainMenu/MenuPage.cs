﻿using UnityEngine;

namespace GHML
{
    public class MenuPage : MonoBehaviour
    {
        public virtual void Initialize() { }

        public virtual void OnPageOpen() { }

        public virtual void OnPageClose() { }
    }
}