﻿using GHML;
using Harmony;
using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GHAPI
{



}

namespace GHML
{
    public class InternalGHAPI
    {
        public static bool isCursorShown = false;
        public static async void GHInternal_ShowCursor(bool value)
        {
            if (SceneManager.GetActiveScene().name == "MainMenu") { return; }
            isCursorShown = value;
            CursorManager.Get().SetCursor(CursorManager.TYPE.Normal);
            CursorManager.Get().ShowCursor(value, false);
        }
    }
}