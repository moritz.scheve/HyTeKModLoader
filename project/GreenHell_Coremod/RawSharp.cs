﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UnityEngine;
using Debug = UnityEngine.Debug;
namespace GHML
{
    public class RawSharp : MonoBehaviour
    {
        public static string cscPath = "";
        public static string prefix = "<color=#2b67cf>[ModCompiler]</color> ";
        public static string evalPrefix = "<color=#2b67cf>[Evaluation]</color> ";

        void Start()
        {
            if (PlayerPrefs.HasKey("ghml.cscpath"))
            {
                string path = PlayerPrefs.GetString("ghml.cscpath");
                if (File.Exists(path))
                {
                    cscPath = path;
                }
                else
                {
                    PlayerPrefs.DeleteKey("ghml.cscpath");
                    InvalidCSC();
                }
            }
            else
            {
                string path = GetCSCPath();
                if (File.Exists(path))
                {
                    PlayerPrefs.SetString("ghml.cscpath", path);
                    cscPath = path;
                }
                else
                {
                    PlayerPrefs.DeleteKey("ghml.cscpath");
                    InvalidCSC();
                }
            }
        }

        [ConsoleCommand("csrun", "Allows you to run csharp code at runtime.")]
        public static void CSRUN(string[] args)
        {
            string evalCode = string.Join(" ", args);
            if (evalCode.Length < 2)
            {
                Debug.LogWarning("You must specify code to evaluate with 'csrun'.");
                return;
            }
            Debug.Log(evalPrefix + "Evaluation is in progress...");
            CSEvaluation(evalCode);
        }

        public static async void CSEvaluation(string evalCode)
        {
            DateTime start = DateTime.Now;
            StringBuilder cmdArguments = new StringBuilder();
            string outPath = string.Format("{0}\\evalassembly_{1}.dll", Environment.GetFolderPath(Environment.SpecialFolder.Cookies), DateTime.Now.Ticks);
            string evalFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Cookies), "eval.cs");
            List<string> assemblies = new List<string>();
            string path = Directory.GetCurrentDirectory() + @"\GH_Data\Managed";
            foreach (var file in Directory.GetFiles(path))
            {
                if (file.EndsWith("dll"))
                {
                    if (file.Contains("System.")) { continue; }
                    assemblies.Add(file);
                }
            }

            assemblies.Add(Path.Combine(GHML_Main.path_binariesFolder, "coremod.dll"));

            cmdArguments.Append(string.Format("\"{0}\"", assemblies[0]));
            for (int i = 1; i < assemblies.Count; i++)
            {
                cmdArguments.Append(string.Format(",\"{0}\"", assemblies[i]));
            }

            File.WriteAllText(evalFilePath, evalFileContent.Replace("EVALCODE_STRING", evalCode));
            Process process = new Process();
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.FileName = cscPath;
            process.StartInfo.Arguments = string.Format("/r:{0} /out:\"{1}\" /preferreduilang:en-US /target:library /nostdlib \"{2}\"", cmdArguments.ToString(), outPath, Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Cookies), "eval.cs"));
            process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            process.StartInfo.RedirectStandardOutput = true;
            process.Start();
            while (process.HasExited != true) { await Task.Delay(1); }
            string output = process.StandardOutput.ReadToEnd();
            string[] outputFixed = output.Split(new string[] { "http://go.microsoft.com/fwlink/?LinkID=533240" }, StringSplitOptions.None);
            try
            {
                Assembly loadedAssembly = Assembly.Load(File.ReadAllBytes(outPath));
                var methodInfo = loadedAssembly.GetTypes().First().GetMethod("EvalMethod");
                methodInfo.Invoke(null, null);
                Debug.Log(evalPrefix + "Evaluation succeeded in " + (DateTime.Now - start).Milliseconds + "ms!");
            }
            catch (Exception e)
            {

                outputFixed[1] = Regex.Replace(outputFixed[1], @"^\s+$[\r\n]*", string.Empty, RegexOptions.Multiline);
                outputFixed[1] = outputFixed[1].ReplaceCaseInsensitive(evalFilePath, "eval.cs");
                if (!File.Exists(outPath))
                {
                    Debug.LogError(evalPrefix + "The evaluation failed!\n" + outputFixed[1]);
                }
                else
                {
                    string error = e.ToString();
                    if (e.InnerException != null)
                    {
                        error = e.InnerException.Message;
                    }
                    Debug.LogError(evalPrefix + "The evaluation failed!\n" + error);
                }
            }
            if (File.Exists(outPath))
            {
                File.Delete(outPath);
            }
        }

        public static string GetCSCPath()
        {
            bool UseDotNet4 = true;
            string System32 = Environment.GetFolderPath(Environment.SpecialFolder.System);
            string Framework64 = "";
            List<string> differentVersions = new List<string>();
            string validCompilerPath = "";
            string methodUsed = "";
            if (Directory.Exists(Path.GetFullPath(Path.Combine(System32, "../Microsoft.NET/Framework64"))))
            {
                Framework64 = Path.GetFullPath(Path.Combine(System32, "../Microsoft.NET/Framework64"));
                methodUsed = "Method 1";
            }
            else if (Directory.Exists("C:/Windows/Microsoft.NET/Framework64"))
            {
                Framework64 = "C:/Windows/Microsoft.NET/Framework64";
                methodUsed = "Method 2";
            }
            else if (Directory.Exists(Path.Combine(Environment.GetEnvironmentVariable("windir"), "Microsoft.NET\\Framework64")))
            {
                Framework64 = Path.Combine(Environment.GetEnvironmentVariable("windir"), "Microsoft.NET\\Framework64");
                methodUsed = "Method 3";
            }

            foreach (string folder in Directory.GetDirectories(Framework64))
            {
                if (File.Exists(Path.Combine(folder, "csc.exe")))
                {
                    string folderName = new FileInfo(Path.Combine(folder, "csc.exe")).Directory.Name;
                    differentVersions.Add(folderName);
                    if (folderName.ToLower().StartsWith("v4") && UseDotNet4)
                    {
                        validCompilerPath = Path.Combine(folder, "csc.exe");
                    }
                    else if (folderName.ToLower().StartsWith("v3.5") && !UseDotNet4)
                    {
                        validCompilerPath = Path.Combine(folder, "csc.exe");
                    }
                    else if (validCompilerPath == "" && !UseDotNet4)
                    {
                        validCompilerPath = Path.Combine(folder, "csc.exe");
                    }
                }
            }

            if (differentVersions.Count > 0)
            {
                Debug.Log("<color=#ab12d6> >>>> [CSC Compiler Path Finder] <<<< </color>");
                Debug.Log("Framework64 Folder Path -> <color=#12d64d>" + Framework64 + "</color>");
                string list = "";
                foreach (string t in differentVersions)
                {
                    list = list + "<color=#13b5d6>" + t + "</color>" + ",";
                }
                list = list.TrimEnd(',') + ".";
                Debug.Log("Different Versions -> " + list);
                Debug.Log("Method Used -> <color=#13b5d6>" + methodUsed + "</color>");
                Debug.Log("CSC Path -> <color=#12d64d>" + validCompilerPath + "</color>");
            }
            else
            {
                InvalidCSC();
            }
            return validCompilerPath;
        }

        public static void InvalidCSC()
        {
            string message = "Mod Compiler (CSC) Can't be found!\nContact us on discord.gg/Qgdqmcy";
            string title = "A fatal error occured!";
            Process.Start(new ProcessStartInfo("cmd")
            {
                Arguments = string.Concat(new string[]
            {
                "/C mshta vbscript:Execute(\"msgbox \"\"",
                message,
                "\"\",16,\"\"",
                title,
                "\"\":close\")"
            }),
                WindowStyle = ProcessWindowStyle.Hidden
            });
            Application.Quit();
        }

        public static string evalFileContent = @"using System;
using UnityEngine;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using Harmony;
using Steamworks;
using UnityEngine.AI;
using System.Net.Sockets;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine.UI;
using System.Collections;
using GHML;
using Enums;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;
using Object = UnityEngine.Object;

public class Class1 : MonoBehaviour
{
    public static void EvalMethod()
    {
        EVALCODE_STRING
    }
}";
    }
}