﻿using AssemblyLoader;
using GHML;
using Harmony;
using System;
using System.IO;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using static AssemblyLoader.HyTeKInjector;
using Debug = UnityEngine.Debug;

public class GHML_Main : MonoBehaviour
{
    public static AssetBundle bundle;
    public static string path_dataFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "GreenHellModLoader");
    public static string path_binariesFolder = Path.Combine(path_dataFolder, "binaries");
    public static string path_logsFolder = Path.Combine(path_dataFolder, "logs");
    public static string path_cacheFolder = Path.Combine(path_dataFolder, "cache");
    public static string path_cacheFolder_mods = Path.Combine(path_cacheFolder, "mods");
    public static string path_cacheFolder_textures = Path.Combine(path_cacheFolder, "textures");
    public static string path_cacheFolder_temp = Path.Combine(path_cacheFolder, "temp");
    public static string path_configFile = Path.Combine(path_dataFolder, "launcher_config.json");
    public static string path_modsFolder = Path.Combine(Application.dataPath, "..\\mods");
    public static string gameLogFile = Path.Combine(path_logsFolder, "coremod.log");
    public static Texture2D missingTexture;
    public static KeyCode MenuKey = KeyCode.F9;
    public static KeyCode ConsoleKey = KeyCode.F10;
    public static TcpClient tcpclient;
    //public static ServerConfiguration dedicatedserver_config = null;
    public static LauncherConfiguration modloader_config = null;
    public static GHML_Main instance;

    async void Start()
    {
        instance = this;
        Application.runInBackground = true;
        Application.backgroundLoadingPriority = ThreadPriority.Low;

        modloader_config = HyTeKInjector.modloader_config;
        //dedicatedserver_config = GHInjector.dedicatedserver_config;
        if (modloader_config.skipSplashScreen)
        {
            MainMenu mainmenu = FindObjectOfType<MainMenu>();
            Traverse.Create(mainmenu).Field("m_FadeInDuration").SetValue(0f);
            Traverse.Create(mainmenu).Field("m_FadeOutDuration").SetValue(0f);
            Traverse.Create(mainmenu).Field("m_FadeOutSceneDuration").SetValue(0f);
            Traverse.Create(mainmenu).Field("m_CompanyLogoDuration").SetValue(0f);
            Traverse.Create(mainmenu).Field("m_GameLogoDuration").SetValue(0f);
            Traverse.Create(mainmenu).Field("m_BlackScreenDuration").SetValue(0f);
        }

        foreach (string file in Directory.GetFiles(path_cacheFolder_temp, "*", SearchOption.AllDirectories))
        {
            if (File.Exists(file) && file.ToLower().EndsWith(".cs") || file.ToLower().EndsWith(".dll"))
                File.Delete(file);
        }

        Application.backgroundLoadingPriority = ThreadPriority.High;
        Directory.CreateDirectory(path_dataFolder);
        Directory.CreateDirectory(path_binariesFolder);
        Directory.CreateDirectory(path_logsFolder);
        Directory.CreateDirectory(path_cacheFolder);
        Directory.CreateDirectory(path_cacheFolder_mods);
        Directory.CreateDirectory(path_cacheFolder_textures);
        Directory.CreateDirectory(path_cacheFolder_temp);
        Directory.CreateDirectory(path_modsFolder);
        Directory.SetCurrentDirectory(Path.Combine(Application.dataPath, "..\\"));
        bundle = await Utils.TaskLoadAssetBundleFromMemoryAsync(File.ReadAllBytes(Path.Combine(path_binariesFolder, "ghml.assets")));
        gameObject.AddComponent<GHConsole>();
        gameObject.AddComponent<RawSharp>();
        gameObject.AddComponent<UnityMainThreadDispatcher>();
        while (!GameObject.Find("GHConsole")) { await Task.Delay(100); }
        try
        {
            HarmonyInstance harmony = HarmonyInstance.Create("hytekgames.greenhellmodloader");
            harmony.PatchAll(Assembly.GetExecutingAssembly());
            Debug.Log("Successfully Loaded Default Harmony Patches!");
        }
        catch (Exception e)
        {
            Debug.LogError(e.ToString());
        }

        while (!GreenHellGame.Instance.m_InitialSequenceComplete) { await Task.Delay(10); }
        Traverse.Create(FindObjectOfType<MainMenu>()).Field("m_StateStartTime").SetValue(Time.time - 3f);
        await Task.Delay(1000);
        InitializeModLoader();
    }

    public async void InitializeModLoader()
    {
        GameObject mainmenu = Instantiate(await bundle.TaskLoadAssetAsync<GameObject>("MainMenu"), gameObject.transform);
        mainmenu.AddComponent<GHMainMenu>().Initialize();

        missingTexture = (await bundle.TaskLoadAssetAsync<Sprite>("missing")).texture;

        GameObject ghnotify = Instantiate(await bundle.TaskLoadAssetAsync<GameObject>("NotificationSystem"), gameObject.transform);
        ghnotify.AddComponent<GHNotify>();

        ghnotify.AddComponent<CustomLoadingScreen>();

        try
        {
            tcpclient = new TcpClient();
            await tcpclient.ConnectAsync("62.210.119.159", 666);
        }
        catch { }
        RuntimeTCPChecker();
    }

    public async void RuntimeTCPChecker()
    {
        bool connected = true;
        try
        {
            NetworkStream netStream = tcpclient.GetStream();
            if (netStream.CanWrite)
            {
                Byte[] sendBytes = Encoding.UTF8.GetBytes("Is anybody there?");
                netStream.Write(sendBytes, 0, sendBytes.Length);
            }
            else
            {
                tcpclient.Close();
                netStream.Close();
                connected = false;
            }
        }
        catch
        {
            connected = false;
        }

        if (!connected)
        {
            try
            {
                tcpclient = new TcpClient();
                await tcpclient.ConnectAsync("62.210.119.159", 666);
            }
            catch { }
        }

        await Task.Delay(5000);
        RuntimeTCPChecker();
    }
}