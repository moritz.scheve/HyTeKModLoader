﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace GHML
{
    static class Utils
    {
        public async static Task<AssetBundle> TaskLoadAssetBundleFromMemoryAsync(byte[] bytes)
        {
            AssetBundleCreateRequest request = AssetBundle.LoadFromMemoryAsync(bytes);
            while (!request.isDone) { await Task.Delay(1); }
            return request.assetBundle;
        }

        public async static Task<T> TaskLoadAssetAsync<T>(this AssetBundle bundle, string name)
        {
            AssetBundleRequest request = bundle.LoadAssetAsync<T>(name);
            while (!request.isDone) { await Task.Delay(1); }
            return (T)Convert.ChangeType(request.asset,typeof(T));
        }

        public static KeyCode KeyCodeFromString(string keyString)
        {
            if (keyString.Length == 1)
            {
                keyString = keyString.ToUpper();
            }

            KeyCode key = KeyCode.None;
            try
            {
                key = (KeyCode)Enum.Parse(typeof(KeyCode), keyString);
            }
            catch (ArgumentException)
            {
                Debug.LogError("Key '" + keyString + "' does not specify a key code.");
            }
            return key;
        }

        public static string CreateSHA512ForFolder(string path)
        {
            // assuming you want to include nested folders
            var files = Directory.GetFiles(path, "*.*", SearchOption.AllDirectories)
                                 .OrderBy(p => p).ToList();

            SHA512 sha512 = SHA512.Create();

            for (int i = 0; i < files.Count; i++)
            {
                string file = files[i];

                // hash path
                string relativePath = file.Substring(path.Length + 1);
                byte[] pathBytes = Encoding.UTF8.GetBytes(relativePath.ToLower());
                sha512.TransformBlock(pathBytes, 0, pathBytes.Length, pathBytes, 0);

                // hash contents
                byte[] contentBytes = File.ReadAllBytes(file);
                if (i == files.Count - 1)
                    sha512.TransformFinalBlock(contentBytes, 0, contentBytes.Length);
                else
                    sha512.TransformBlock(contentBytes, 0, contentBytes.Length, contentBytes, 0);
            }

            return BitConverter.ToString(sha512.Hash).Replace("-", "").ToLower();
        }

        public static string ReplaceCaseInsensitive(this string input, string search, string replacement)
        {
            string result = Regex.Replace(
                input,
                Regex.Escape(search),
                replacement.Replace("$", "$$"),
                RegexOptions.IgnoreCase
            );
            return result;
        }

        public static string StripRichText(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }

        public static string GetStringSHA512Hash(string strToEncrypt)
        {
            UTF8Encoding ue = new UTF8Encoding();
            byte[] bytes = ue.GetBytes(strToEncrypt);

            System.Security.Cryptography.SHA512CryptoServiceProvider sha512 = new SHA512CryptoServiceProvider();
            byte[] hashBytes = sha512.ComputeHash(bytes);

            string hashString = "";

            for (int i = 0; i < hashBytes.Length; i++)
            {
                hashString += Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
            }

            return hashString.PadLeft(32, '0');
        }

        public static string GetFileSHA512Hash(string filename)
        {
            using (var sha512 = SHA512.Create())
            {
                using (var stream = File.OpenRead(filename))
                {
                    var hash = sha512.ComputeHash(stream);
                    return BitConverter.ToString(hash).Replace("-", "");
                }
            }
        }

        public static async Task<int> MakeAsync()
        {
            await Task.Delay(1);
            return 0;
        }

        public static async Task<string> GetModVersion(string url)
        {
            if (url != "Unknown" && !string.IsNullOrWhiteSpace(url))
            {
                DateTime now = DateTime.Now;
                using (UnityWebRequest www = UnityWebRequest.Get(url))
                {
                    www.certificateHandler = new GHMLCertificateHandler();
                    www.SendWebRequest();
                    bool cancelTimeOut = false;
                    while (!cancelTimeOut && !www.isDone && !www.isNetworkError && !www.isHttpError)
                    {
                        if (now.AddSeconds(5) > DateTime.Now)
                        {
                            await Task.Delay(1);
                        }
                        else
                        {
                            await Task.Delay(1);
                            cancelTimeOut = true;
                        }
                    }

                    if (!www.isNetworkError && !www.isHttpError && string.IsNullOrEmpty(www.error))
                    {
                        string latestVersion = www.downloadHandler.text;
                        return www.downloadHandler.text;
                    }
                }
                return "";
            }
            else
            {
                return "";
            }
        }

        public static async Task<Texture> DownloadCachedTexture(string url)
        {
            string cachedFile = Path.Combine(GHML_Main.path_cacheFolder_textures, GetStringSHA512Hash(url) + ".png");
            if (File.Exists(cachedFile))
            {
                try
                {
                    Texture2D texture = new Texture2D(2, 2, TextureFormat.RGB24, false);
                    texture.LoadImage(File.ReadAllBytes(cachedFile));
                    if (texture.height > 8 && texture.width > 8)
                    {
                        return texture;
                    }
                    else
                    {
                        File.Delete(cachedFile);
                    }
                }
                catch { File.Delete(cachedFile); }
            }
            DateTime now = DateTime.Now;
            UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
            www.certificateHandler = new GHMLCertificateHandler();
            www.SendWebRequest();
            bool cancelTimeOut = false;
            while (!cancelTimeOut && !www.isDone && !www.isNetworkError && !www.isHttpError)
            {
                if (now.AddSeconds(5) > DateTime.Now)
                {
                    await Task.Delay(1);
                }
                else
                {
                    await Task.Delay(1);
                    cancelTimeOut = true;
                }
            }

            if (!www.isNetworkError && !www.isHttpError && !cancelTimeOut)
            {
                Texture texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                if (texture.width > 8 && texture.height > 8)
                {
                    File.WriteAllBytes(cachedFile, www.downloadHandler.data);
                    return texture;
                }
                else
                {
                    return GHML_Main.missingTexture;
                }
            }
            else
            {
                return GHML_Main.missingTexture;
            }
        }

        public static string GetDateFormatted(string datetimestamp)
        {
            DateTime date = DateTime.Parse(datetimestamp);
            const int SECOND = 1;
            const int MINUTE = 60 * SECOND;
            const int HOUR = 60 * MINUTE;
            const int DAY = 24 * HOUR;
            const int MONTH = 30 * DAY;

            var ts = new TimeSpan(DateTime.Now.Ticks - date.Ticks);
            double delta = Math.Abs(ts.TotalSeconds);

            if (delta < 1 * MINUTE)
                return ts.Seconds == 1 ? "one second ago" : ts.Seconds + " seconds ago";

            if (delta < 2 * MINUTE)
                return "a minute and " + ts.Seconds + " seconds ago";

            if (delta < 45 * MINUTE)
                return ts.Minutes + " minutes and " + ts.Seconds + " seconds ago";

            if (delta < 90 * MINUTE)
                return "an hour and " + ts.Minutes + " minutes ago";

            if (delta < 24 * HOUR)
                return ts.Hours + " hours and " + ts.Minutes + " minutes ago";

            if (delta < 48 * HOUR)
                return "yesterday";

            if (delta < 30 * DAY)
                return ts.Days + " days and " + ts.Hours + " hours ago";

            if (delta < 12 * MONTH)
            {
                int months = Convert.ToInt32(Math.Floor((double)ts.Days / 30));
                return months <= 1 ? "one month ago" : months + " months and " + ts.Days + " days ago";
            }
            else
            {
                int years = Convert.ToInt32(Math.Floor((double)ts.Days / 365));
                return years <= 1 ? "one year ago" : years + " years ago";
            }
        }

        public static async Task<Texture> DownloadUncachedTexture(string url)
        {
            DateTime now = DateTime.Now;
            UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
            www.certificateHandler = new GHMLCertificateHandler();
            www.SendWebRequest();
            bool cancelTimeOut = false;
            while (!cancelTimeOut && !www.isDone && !www.isNetworkError && !www.isHttpError)
            {
                if (now.AddSeconds(5) > DateTime.Now)
                {
                    await Task.Delay(1);
                }
                else
                {
                    await Task.Delay(1);
                    cancelTimeOut = true;
                }
            }

            if (!www.isNetworkError && !www.isHttpError && !cancelTimeOut)
            {
                Texture texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                if (texture.width > 8 && texture.height > 8)
                {
                    return texture;
                }
                else
                {
                    return GHML_Main.missingTexture;
                }
            }
            else
            {
                return GHML_Main.missingTexture;
            }
        }

        public static void CopyToClipboard(this string s)
        {
            TextEditor te = new TextEditor();
            te.text = s;
            te.SelectAll();
            te.Copy();
        }
    }

    public class TooltipHandler : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public GameObject tooltip;

        public void Start()
        {
            tooltip.SetActive(false);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            tooltip.SetActive(true);
        }
        public void OnPointerExit(PointerEventData eventData)
        {
            if (gameObject.GetComponent<Button>() != null)
            {
                gameObject.GetComponent<Button>().OnDeselect(null);
            }
            tooltip.SetActive(false);
        }
    }
}