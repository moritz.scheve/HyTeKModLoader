﻿using GHML;
using Harmony;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GHML
{
    [HarmonyPatch(typeof(InputsManager))]
    [HarmonyPatch("GetActionValue")]
    [HarmonyPatch(new Type[] { typeof(int) })]
    class Patch_InputsManager_GetActionValue
    {
        static bool Prefix(InputsManager __instance, int action, ref float __result)
        {
            if (InternalGHAPI.isCursorShown)
            {
                __result = 0f;
                return false;
            }
            else
            {
                return true;
            }
        }
    }

    [HarmonyPatch(typeof(InputsManager))]
    [HarmonyPatch("OnAction")]
    class Patch_InputsManager_OnAction
    {
        static bool Prefix(InputsManager __instance)
        {
            return !InternalGHAPI.isCursorShown;
        }
    }

}