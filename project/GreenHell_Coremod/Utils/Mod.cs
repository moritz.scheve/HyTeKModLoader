﻿using UnityEngine;
using GHML;
using System.Linq;

public class Mod : MonoBehaviour
{
    private static ModData listReference;

    public Mod()
    {
        listReference = ModManagerPage.modList.Find(t => t.modinfo.assembly == GetType().Assembly);
    }

    public virtual void UnloadMod()
    {
        listReference.modinfo.modHandler.UnloadMod(listReference);
    }

    public virtual byte[] GetEmbeddedFileBytes(string path)
    {
        byte[] value = null;
        listReference.modinfo.modFiles.TryGetValue(path, out value);
        if (value == null)
            Debug.LogError("[ModManager] " + listReference.jsonmodinfo.name + " > The file " + path + " doesn't exist!");
        return value;
    }
}