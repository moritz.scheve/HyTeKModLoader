﻿using Harmony;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.UI;
namespace GHML
{
    public class CustomLoadingScreen : MonoBehaviour
    {
        public static CustomLoadingScreen instance;
        public static GameObject patronPrefab;
        public static GameObject customloadingscreen;
        private static bool initialized = false;

        async void Awake()
        {
            instance = this;
            customloadingscreen = Instantiate(await GHML_Main.bundle.TaskLoadAssetAsync<GameObject>("LoadingScreen"));
            DontDestroyOnLoad(customloadingscreen);
            customloadingscreen.SetActive(false);
            patronPrefab = await GHML_Main.bundle.TaskLoadAssetAsync<GameObject>("PatronEntry");
            try
            {
                string json = await new WebClient().DownloadStringTaskAsync("https://fastdl.greenhellmodding.com/data/patrons.json");
                List<PatronsEntries> patrons = JsonConvert.DeserializeObject<List<PatronsEntries>>(json);
                InitLoadingScreen(patrons);
            }
            catch { }
            initialized = true;
        }

        void Update()
        {
            if (!initialized) { return; };
            LoadingScreen ls = LoadingScreen.Get();
            customloadingscreen.SetActive(ls.m_Image.gameObject.activeSelf);
        }

        static void InitLoadingScreen(List<PatronsEntries> patrons)
        {
            try
            {
                foreach (Transform t in customloadingscreen.transform.Find("Background").Find("Patronsbar"))
                {
                    Destroy(t.gameObject);
                }
                patrons.ForEach(patron =>
                {
                    string name = patron.name;
                    string amount = patron.amount;
                    string imageUrl = patron.imageUrl;
                    bool isNitroBooster = patron.isNitroBooster;
                    bool avatarAnimated = patron.avatarAnimated;
                    bool usernameAnimated = patron.usernameAnimated;
                    GameObject item = Instantiate(patronPrefab, Vector3.zero, Quaternion.identity, customloadingscreen.transform.Find("Background").Find("Patronsbar"));
                    item.transform.Find("username").GetComponent<Text>().text = name;
                    if (amount != "")
                    {
                        item.transform.Find("username").GetComponent<Text>().text += " : <color=#4287f5>" + amount + "</color>";
                    }
                    item.transform.Find("IsNitroUser").gameObject.SetActive(isNitroBooster);
                    item.transform.Find("AvatarMask").GetComponent<Animation>().enabled = avatarAnimated;
                    item.transform.Find("username").GetComponent<Animation>().enabled = usernameAnimated;

                    if (imageUrl != "" && imageUrl.Length > 5)
                    {
                        RawImage image = item.transform.Find("AvatarMask").Find("avatar").GetComponent<RawImage>();
                        Utils.DownloadCachedTexture(imageUrl).ContinueWith((t) =>
                        {
                            image.texture = t.Result;
                        });
                    }
                });
            }
            catch (Exception e) { Debug.Log(e.ToString()); }
        }
    }

    [Serializable]
    class PatronsEntries
    {
        public string name = "";
        public string amount = "";
        public string imageUrl = "";
        public bool isNitroBooster = false;
        public bool avatarAnimated = false;
        public bool usernameAnimated = false;
    }

}