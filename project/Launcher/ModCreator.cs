﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static LauncherConfiguration;

namespace Launcher
{
    public partial class ModCreator : Form
    {
        Main defaultForm = null;
        public string devFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), FOLDERS_DEVENV);
        public string coremodPath;
        public string gamePath;

        public ModCreator(Main t)
        {
            Opacity = 0;
            FadeIn(this, 1).ContinueWith((v) => { });
            coremodPath = Path.Combine(Main.FOLDER_BINARIES, FILES_COREMOD);
            gamePath = Main.CurrentConfig.gamePath + "\\" + GAME_DATAFOLDERNAME;
            defaultForm = t;
            InitializeComponent();
            windowTitle.Text = GAME_NAME + " Mod C# Project Creator";
            info.Text = @"Welcome to the " + GAME_NAME + @" Mod Project Creator!

This tool will allow you to generate a development environment for your mod.

Mods are coded in the C#/CSharp language. If you want to learn more about how to make mods you can join our modding discord.

";
            projectpath.Text = devFolder;
            InitializeTheme();
        }

        public void InitializeTheme()
        {
#if GAME_IS_RAFT
            windowIcon.BackgroundImage = Properties.Resources.raft_rml_logo;
            pictureBox7.BackColor = THEME_TOPBAR_COLOR;
            windowIcon.BackColor = THEME_TOPBAR_COLOR;
            windowTitle.BackColor = THEME_TOPBAR_COLOR;
            closeBtn.BackColor = THEME_TOPBAR_COLOR;
            BackColor = THEME_BACKGROUND_COLOR;
            info.BackColor = THEME_BACKGROUND_COLOR;

            pictureBox1.BackColor = THEME_TOPBAR_COLOR;
            label1.BackColor = THEME_TOPBAR_COLOR;
            label3.BackColor = THEME_TOPBAR_COLOR;
            projectname.BackColor = THEME_BACKGROUND_COLOR;
            projectpath.BackColor = THEME_BACKGROUND_COLOR;
            label5.BackColor = THEME_TOPBAR_COLOR;
            label6.BackColor = THEME_TOPBAR_COLOR;
            dots.BackColor = THEME_BACKGROUND_COLOR;
            custompathbutton.BackColor = THEME_BACKGROUND_COLOR;
            custompathbutton.FlatAppearance.BorderColor = THEME_BACKGROUND_COLOR;
            custompathbutton.FlatAppearance.MouseOverBackColor = THEME_BACKGROUND_COLOR;
            custompathbutton.FlatAppearance.MouseDownBackColor = THEME_BACKGROUND_COLOR;
#endif
        }

        private async Task FadeIn(Form o, int interval = 80)
        {
            await Task.Delay(100);
            while (o.Opacity < 1.0)
            {
                await Task.Delay(interval);
                o.Opacity += 0.09;
            }
            o.Opacity = 1;
        }

        private async void CloseForm(Form o, int interval = 80)
        {
            defaultForm.ReopenForm(1);
            while (o.Opacity > 0.0)
            {
                await Task.Delay(interval);
                o.Opacity -= 0.07;
            }
            o.Opacity = 0;
            this.Close();
        }

        bool mouseDown;
        int mouseX = 0, mouseY = 0;
        int mouseinX = 0, mouseinY = 0;

        private void DragMouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
            mouseinX = MousePosition.X - Bounds.X;
            mouseinY = MousePosition.Y - Bounds.Y;
        }

        private void closeBtn_Click(object sender, EventArgs e)
        {
            CloseForm(this, 1);
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(DISCORD_INVITEURL);
        }

        private void createBtn_Click(object sender, EventArgs e)
        {
            CreateProject();
        }

        public void CreateProject()
        {
            if (projectname.Text.Contains(" "))
            {
                MessageBox.Show("The project name title can't contain spaces!", "The title is not valid!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!IsFileNameCorrect(projectname.Text))
            {
                MessageBox.Show("The project name is invalid!", "The title is not valid!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (string.IsNullOrEmpty(projectname.Text))
            {
                MessageBox.Show("The project name can't contain blank characters.", "The title is not valid!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (projectname.Text.Length < 5 || projectname.Text.Length > 45)
            {
                MessageBox.Show("The project name should contain be between 5 and 45 characters.", "The title is not valid!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Directory.CreateDirectory(devFolder);
            string projectfolder = Path.Combine(devFolder, projectname.Text);
            Directory.CreateDirectory(projectfolder);
#if GAME_IS_GREENHELL
            try
            {
                File.WriteAllBytes(Path.Combine(projectfolder, "zip.exe"), Program.GetEmbeddedFile("Resources.ModCreatorProjectFiles.zip.exe"));
                File.WriteAllBytes(Path.Combine(projectfolder, "build.bat"), Program.GetEmbeddedFile("Resources.ModCreatorProjectFiles.build.bat"));
                File.WriteAllText(Path.Combine(projectfolder, projectname.Text + ".sln"), Encoding.UTF8.GetString(Program.GetEmbeddedFile("Resources.ModCreatorProjectFiles._MODNAME_.sln")).Replace("_MODNAME_", projectname.Text));

                string secondfolder = Path.Combine(projectfolder, projectname.Text);
                Directory.CreateDirectory(secondfolder);
                File.WriteAllText(Path.Combine(secondfolder, projectname.Text + ".cs"), Encoding.UTF8.GetString(Program.GetEmbeddedFile("Resources.ModCreatorProjectFiles._MODNAME_._MODNAME_.cs")).Replace("_MODNAME_", projectname.Text));
                File.WriteAllText(Path.Combine(secondfolder, projectname.Text + ".csproj"), Encoding.UTF8.GetString(Program.GetEmbeddedFile("Resources.ModCreatorProjectFiles._MODNAME_._MODNAME_.csproj")).Replace("_MODNAME_", projectname.Text).Replace("_GAMEPATH_", Main.CurrentConfig.gamePath).Replace("_COREMODPATH_", Path.Combine(Main.FOLDER_BINARIES, "coremod.dll")));
                File.WriteAllText(Path.Combine(secondfolder, "modinfo.json"), Encoding.UTF8.GetString(Program.GetEmbeddedFile("Resources.ModCreatorProjectFiles._MODNAME_.modinfo.json")).Replace("_MODNAME_", projectname.Text));
                File.WriteAllBytes(Path.Combine(secondfolder, "banner.jpg"), Program.GetEmbeddedFile("Resources.ModCreatorProjectFiles._MODNAME_.banner.jpg"));
                File.WriteAllBytes(Path.Combine(secondfolder, "icon.png"), Program.GetEmbeddedFile("Resources.ModCreatorProjectFiles._MODNAME_.icon.png"));
            }
            catch(Exception e)
            {
                MessageBox.Show(e.ToString());
            }
#elif GAME_IS_RAFT
            Directory.CreateDirectory(Path.Combine(projectfolder, projectname.Text));
            File.WriteAllText(Path.Combine(projectfolder, projectname.Text + ".sln"), slnFile.Replace("samplemodproject1", projectname.Text));
            File.WriteAllText(Path.Combine(Path.Combine(projectfolder, projectname.Text), projectname.Text + ".cs"), csFile.Replace("samplemodproject1", projectname.Text));
            File.WriteAllText(Path.Combine(Path.Combine(projectfolder, projectname.Text), projectname.Text + ".csproj"), csprojFile.Replace("samplemodproject1", projectname.Text).Replace("_RAFTPATH_", Main.CurrentConfig.gamePath).Replace("_COREMODDLLFILE_", coremodPath));
#endif
            Process.Start(projectfolder);
            MessageBox.Show("Your mod project has been created in \n" + devFolder + "\nThe folder has been opened! Have a good time modding " + GAME_NAME + "!", "Your mod project has been successfully created!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            CloseForm(this, 1);
        }

        bool IsFileNameCorrect(string fileName)
        {
            return fileName.All(f => !Path.GetInvalidFileNameChars().Contains(f));
        }

        private void DragMouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        private void DragMouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                mouseX = MousePosition.X - mouseinX;
                mouseY = MousePosition.Y - mouseinY;

                SetDesktopLocation(mouseX, mouseY);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var dialog = new FolderSelectDialog
            {
                InitialDirectory = devFolder,
                Title = "Please select a folder to create the mod project inside"
            };
            if (dialog.Show(Handle))
            {
                string path = dialog.FileName;
                if (Directory.Exists(path))
                {
                    devFolder = path;
                    projectpath.Text = path;
                }
            }
        }
        private void dots_MouseClick(object sender, MouseEventArgs e)
        {
            button1_Click(null, null);
        }

#if GAME_IS_RAFT
        string slnFile = @"
Microsoft Visual Studio Solution File, Format Version 12.00
\# Visual Studio 15
VisualStudioVersion = 15.0.28307.421
MinimumVisualStudioVersion = 10.0.40219.1
Project(""{FAE04EC0-301F-11D3-BF4B-00C04F79EFBC}"") = ""samplemodproject1"", ""samplemodproject1\samplemodproject1.csproj"", ""{41D2BDD7-E52C-4470-AC81-5F4B496CC559}""
EndProject
Global
	GlobalSection(SolutionConfigurationPlatforms) = preSolution
		Debug|Any CPU = Debug|Any CPU
		Release|Any CPU = Release|Any CPU
	EndGlobalSection
	GlobalSection(ProjectConfigurationPlatforms) = postSolution
		{41D2BDD7-E52C-4470-AC81-5F4B496CC559}.Debug|Any CPU.ActiveCfg = Debug|Any CPU
		{41D2BDD7-E52C-4470-AC81-5F4B496CC559}.Debug|Any CPU.Build.0 = Debug|Any CPU
		{41D2BDD7-E52C-4470-AC81-5F4B496CC559}.Release|Any CPU.ActiveCfg = Release|Any CPU
		{41D2BDD7-E52C-4470-AC81-5F4B496CC559}.Release|Any CPU.Build.0 = Release|Any CPU
	EndGlobalSection
	GlobalSection(SolutionProperties) = preSolution
		HideSolutionNode = FALSE
	EndGlobalSection
	GlobalSection(ExtensibilityGlobals) = postSolution
		SolutionGuid = {1513B1BC-AF36-42E5-94C4-A1575DADF180}
	EndGlobalSection
EndGlobal
";

        string csFile = @"using UnityEngine;

[ModTitle(""Your Mod Name"")] // The mod name.
[ModDescription(""Your Mod Description"")] // Short description for the mod.
[ModAuthor(""Author Name"")] // The author name of the mod.
[ModIconUrl(""Icon Url"")] // An icon for your mod. Its recommended to be 128x128px and in .jpg format.
[ModWallpaperUrl(""Banner Url"")] // A banner for your mod. Its recommended to be 330x100px and in .jpg format.
[ModVersionCheckUrl(""Version File Url"")] // This is for update checking. Needs to be a .txt file with the latest mod version.
[ModVersion(""1.0"")] // This is the mod version.
[RaftVersion(""Update Name"")] // This is the recommended raft version.
[ModIsPermanent(false)] // If your mod add new blocks, new items or just content you should set that to true. It loads the mod on start and prevents unloading.
public class samplemodproject1 : Mod
{
    // The Start() method is being called when your mod gets loaded.
    public void Start()
    {
        RConsole.Log(""samplemodproject1 has been loaded!"");
    }

    // The Update() method is being called every frame. Have fun!
    public void Update()
    {
        
    }

    // The OnModUnload() method is being called when your mod gets unloaded.
    public void OnModUnload()
    {
        RConsole.Log(""samplemodproject1 has been unloaded!"");
        Destroy(gameObject); // Please do not remove that line!
    }
}
";

        string csprojFile = @"<?xml version=""1.0"" encoding=""utf-8""?>
<Project ToolsVersion=""15.0"" xmlns=""http://schemas.microsoft.com/developer/msbuild/2003"">
  <Import Project=""$(MSBuildExtensionsPath)\$(MSBuildToolsVersion)\Microsoft.Common.props"" Condition=""Exists('$(MSBuildExtensionsPath)\$(MSBuildToolsVersion)\Microsoft.Common.props')"" />
  <PropertyGroup>
    <Configuration Condition="" '$(Configuration)' == '' "">Debug</Configuration>
    <Platform Condition="" '$(Platform)' == '' "">AnyCPU</Platform>
    <ProjectGuid>{41D2BDD7-E52C-4470-AC81-5F4B496CC559}</ProjectGuid>
    <OutputType>Library</OutputType>
    <AppDesignerFolder>Properties</AppDesignerFolder>
    <RootNamespace>samplemodproject1</RootNamespace>
    <AssemblyName>samplemodproject1</AssemblyName>
    <TargetFrameworkVersion>v4.5</TargetFrameworkVersion>
    <FileAlignment>512</FileAlignment>
    <Deterministic>true</Deterministic>
  </PropertyGroup>
  <PropertyGroup Condition="" '$(Configuration)|$(Platform)' == 'Debug|AnyCPU' "">
    <DebugSymbols>true</DebugSymbols>
    <DebugType>full</DebugType>
    <Optimize>false</Optimize>
    <OutputPath>.\</OutputPath>
    <DefineConstants>DEBUG;TRACE</DefineConstants>
    <ErrorReport>prompt</ErrorReport>
    <WarningLevel>4</WarningLevel>
    <GenerateSerializationAssemblies>Off</GenerateSerializationAssemblies>
  </PropertyGroup>
  <PropertyGroup Condition="" '$(Configuration)|$(Platform)' == 'Release|AnyCPU' "">
    <DebugType>pdbonly</DebugType>
    <Optimize>true</Optimize>
    <OutputPath>bin\Release\</OutputPath>
    <DefineConstants>TRACE</DefineConstants>
    <ErrorReport>prompt</ErrorReport>
    <WarningLevel>4</WarningLevel>
  </PropertyGroup>
  <ItemGroup>
    <Compile Include=""samplemodproject1.cs"" />
  </ItemGroup>
  <ItemGroup>
    <Reference Include=""Assembly-CSharp"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\Assembly-CSharp.dll</HintPath>
    </Reference>
    <Reference Include=""Assembly-CSharp-firstpass"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\Assembly-CSharp-firstpass.dll</HintPath>
    </Reference>
    <Reference Include=""coremod"">
      <HintPath>_COREMODDLLFILE_</HintPath>
    </Reference>
    <Reference Include=""Mono.Posix"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\Mono.Posix.dll</HintPath>
    </Reference>
    <Reference Include=""Mono.Security"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\Mono.Security.dll</HintPath>
    </Reference>
    <Reference Include=""NavMeshComponents"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\NavMeshComponents.dll</HintPath>
    </Reference>
    <Reference Include=""Sirenix.OdinInspector.Attributes"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\Sirenix.OdinInspector.Attributes.dll</HintPath>
    </Reference>
    <Reference Include=""System"" />
    <Reference Include=""System.Configuration"" />
    <Reference Include=""System.Security"" />
    <Reference Include=""System.Xml"" />
    <Reference Include=""Unity.Analytics.DataPrivacy"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\Unity.Analytics.DataPrivacy.dll</HintPath>
    </Reference>
    <Reference Include=""Unity.Analytics.StandardEvents"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\Unity.Analytics.StandardEvents.dll</HintPath>
    </Reference>
    <Reference Include=""Unity.Analytics.Tracker"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\Unity.Analytics.Tracker.dll</HintPath>
    </Reference>
    <Reference Include=""Unity.TextMeshPro"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\Unity.TextMeshPro.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.AccessibilityModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.AccessibilityModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.AIModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.AIModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.AnimationModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.AnimationModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.ARModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.ARModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.AssetBundleModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.AssetBundleModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.AudioModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.AudioModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.BaselibModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.BaselibModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.ClothModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.ClothModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.ClusterInputModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.ClusterInputModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.ClusterRendererModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.ClusterRendererModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.CoreModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.CoreModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.CrashReportingModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.CrashReportingModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.DirectorModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.DirectorModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.FileSystemHttpModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.FileSystemHttpModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.GameCenterModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.GameCenterModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.GridModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.GridModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.HotReloadModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.HotReloadModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.ImageConversionModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.ImageConversionModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.IMGUIModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.IMGUIModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.InputModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.InputModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.JSONSerializeModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.JSONSerializeModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.LocalizationModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.LocalizationModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.Networking"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.Networking.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.ParticleSystemModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.ParticleSystemModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.PerformanceReportingModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.PerformanceReportingModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.Physics2DModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.Physics2DModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.PhysicsModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.PhysicsModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.ProfilerModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.ProfilerModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.ScreenCaptureModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.ScreenCaptureModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.SharedInternalsModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.SharedInternalsModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.SpatialTracking"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.SpatialTracking.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.SpriteMaskModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.SpriteMaskModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.SpriteShapeModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.SpriteShapeModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.StreamingModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.StreamingModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.StyleSheetsModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.StyleSheetsModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.SubstanceModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.SubstanceModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.TerrainModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.TerrainModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.TerrainPhysicsModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.TerrainPhysicsModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.TextCoreModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.TextCoreModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.TextRenderingModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.TextRenderingModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.TilemapModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.TilemapModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.Timeline"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.Timeline.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.TimelineModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.TimelineModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.TLSModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.TLSModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.UI"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.UI.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.UIElementsModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.UIElementsModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.UIModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.UIModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.UmbraModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.UmbraModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.UNETModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.UNETModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.UnityAnalyticsModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.UnityAnalyticsModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.UnityConnectModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.UnityConnectModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.UnityTestProtocolModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.UnityTestProtocolModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.UnityWebRequestAssetBundleModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.UnityWebRequestAssetBundleModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.UnityWebRequestAudioModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.UnityWebRequestAudioModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.UnityWebRequestModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.UnityWebRequestModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.UnityWebRequestTextureModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.UnityWebRequestTextureModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.UnityWebRequestWWWModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.UnityWebRequestWWWModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.VehiclesModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.VehiclesModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.VFXModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.VFXModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.VideoModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.VideoModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.VRModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.VRModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.WindModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.WindModule.dll</HintPath>
    </Reference>
    <Reference Include=""UnityEngine.XRModule"">
      <HintPath>_RAFTPATH_\Raft_Data\Managed\UnityEngine.XRModule.dll</HintPath>
    </Reference>
  </ItemGroup>
  <ItemGroup>
    <Folder Include=""Properties\"" />
  </ItemGroup>
  <Import Project=""$(MSBuildToolsPath)\Microsoft.CSharp.targets"" />
  <PropertyGroup>
    <PreBuildEvent>
===========WARNING============


PLEASE DO NOT COMPILE THE MOD.
THE MOD IS THE RAW .CS FILE.


============================
</PreBuildEvent>
  </PropertyGroup>
  <PropertyGroup>
    <PostBuildEvent>
===========WARNING============


PLEASE DO NOT COMPILE THE MOD.
THE MOD IS THE RAW .CS FILE.


============================
</PostBuildEvent>
  </PropertyGroup>
</Project>";
#endif
    }
}
