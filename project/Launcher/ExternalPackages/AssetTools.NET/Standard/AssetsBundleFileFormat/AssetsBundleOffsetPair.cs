﻿#if GAME_IS_RAFT
namespace AssetsTools.NET
{
    public struct AssetsBundleOffsetPair
    {
        public uint compressed;
        public uint uncompressed;
    }
}
#endif