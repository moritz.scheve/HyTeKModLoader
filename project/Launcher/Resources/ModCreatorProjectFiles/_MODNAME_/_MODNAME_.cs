﻿using UnityEngine;

public class _MODNAME_ : Mod
{
    public void Start()
    {
        Debug.Log("Mod _MODNAME_ has been loaded!");
    }

    public void OnModUnload()
    {
        Debug.Log("Mod _MODNAME_ has been unloaded!");
    }
}