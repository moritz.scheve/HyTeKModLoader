﻿using Gameloop.Vdf;
using Microsoft.Win32;
using SharpMonoInjector;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.Serialization.Json;
using static LauncherConfiguration;
using LauncherUpdater;
#if GAME_IS_RAFT
using AssetsTools.NET;
using AssetsTools.NET.Extra;
#endif

namespace Launcher
{
    public partial class Main : Form
    {
        static Main instance;
        public static Main Get()
        {
            return instance;
        }

        public static LauncherTab DownloadsTab = new DownloadsTab();
        public static LauncherTab ServicesTab = new ServicesTab();
        public static LauncherTab SettingsTab = new SettingsTab();

        public static string FOLDER_MAIN = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), MODLOADER_NAME_NOSPACES);
        public static string FOLDER_BINARIES = Path.Combine(FOLDER_MAIN, "binaries");
        public static string FOLDER_CACHE = Path.Combine(FOLDER_MAIN, "cache");
        public static string FOLDER_CACHE_MODS = Path.Combine(FOLDER_CACHE, "mods");
        public static string FOLDER_CACHE_TEXTURES = Path.Combine(FOLDER_CACHE, "textures");
        public static string FOLDER_CACHE_TEMPORARY = Path.Combine(FOLDER_CACHE, "temp");
        public static string CONFIGURATION_FILE = Path.Combine(FOLDER_MAIN, "launcher_config.json");
        static string LAUNCHER_EXECUTABLE = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), MODLOADER_NAME_NOSPACES + "/HMLCore.exe");
        public static LauncherConfiguration CurrentConfig = new LauncherConfiguration();
        public string arguments = "";
        ModLoaderVersion latestUpdateInfo;

        public Main()
        {
            instance = this;
            // Initializing The Error Handler
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += new UnhandledExceptionEventHandler(ErrorHandler);
            Application.ThreadException += new ThreadExceptionEventHandler(ErrorHandler);
            // Registering the URI Scheme to open the launcher from an url like RMLLauncher://
            RegisterUriScheme();
            arguments = string.Join(" ", Environment.GetCommandLineArgs().Skip(1));
            Opacity = 0;
            InitializeComponent();
            ThemeManager.InitializeTheme();
            ThemeManager.InitializeFonts();
            Size = new Size(800, 450);
            windowTitle.Text = "HyTeKModLoader Launcher " + Properties.Settings.Default["version"] + (new WindowsPrincipal(WindowsIdentity.GetCurrent()).IsInRole(WindowsBuiltInRole.Administrator) ? " (Running as Administrator)" : "");
            noDownloadsLabel.Show();
            assetsPanel.Hide();
            coremodPanel.Hide();
            downloadInfo.Hide();
            assetsBar.Size = new Size(0, 25);
            coremodBar.Size = new Size(0, 25);
            RunningChecker();
            DownloadsTab.Initialize();
            ServicesTab.Initialize();
            SettingsTab.Initialize();
        }
#if GAME_IS_RAFT
        public void ChangeDisableUnityAudio(string path, bool newValue)
        {
            try
            {
                AssetsManager am = new AssetsManager();
                AssetsFileInstance afi = am.LoadAssetsFile(path, false);
                using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("Launcher.Resources.cldb.dat"))
                {
                    am.LoadClassDatabase(stream);
                }
                AssetFileInfoEx audioInfo = afi.table.GetAssetInfo(4);
                AssetTypeInstance audioAti = am.GetATI(afi.file, audioInfo);
                AssetTypeValueField audioBaseField = audioAti.GetBaseField();
                audioBaseField.Get("m_DisableAudio").GetValue().Set(newValue);
                byte[] audioAsset;
                using (MemoryStream memStream = new MemoryStream())
                using (AssetsFileWriter writer = new AssetsFileWriter(memStream))
                {
                    writer.bigEndian = false;
                    audioBaseField.Write(writer);
                    audioAsset = memStream.ToArray();
                }
                List<AssetsReplacer> rep = new List<AssetsReplacer>() { new AssetsReplacerFromMemory(0, 4, 0x0B, 0xFFFF, audioAsset) };
                using (MemoryStream memStream = new MemoryStream())
                using (AssetsFileWriter writer = new AssetsFileWriter(memStream))
                {
                    afi.file.Write(writer, 0, rep, 0);
                    afi.stream.Close();
                    File.WriteAllBytes(path, memStream.ToArray());
                }
            }
            catch
            {
                MessageBox.Show("An error occured while enabling the Raft built in audio system!\n\nAs we now use a new method to enable custom audio, Please verify your game files using steam and it should work!\n\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
#endif

        public static void RegisterUriScheme()
        {
            try
            {
                using (var key = Registry.CurrentUser.CreateSubKey("SOFTWARE\\Classes\\" + MODLOADER_URISCHEME))
                {
                    string applicationLocation = Application.ExecutablePath;
                    key.SetValue("", "URL:" + MODLOADER_NAME);
                    key.SetValue("URL Protocol", "");
                    using (var defaultIcon = key.CreateSubKey("DefaultIcon"))
                    {
                        defaultIcon.SetValue("", LAUNCHER_EXECUTABLE + ",1");
                    }

                    using (var commandKey = key.CreateSubKey(@"shell\open\command"))
                    {
                        commandKey.SetValue("", "\"" + LAUNCHER_EXECUTABLE + "\" \"%1\"");
                    }
                }
            }
            catch { }
        }

        public async void RunningChecker()
        {
            do
            {
                if (!updateStatus.Text.StartsWith(MODLOADER_NAME_NOSPACES + " is up to date!", StringComparison.InvariantCulture) && !updateStatus.Text.StartsWith("An error occured while fetching for updates!", StringComparison.InvariantCulture))
                {
                    playBtn.Hide();
                }
                else
                {
                    playBtn.Show();
                    if (Process.GetProcessesByName(GAME_MAINFILENAME.Replace(".exe", "")).Length > 0 && Process.GetProcessesByName(DEDICATEDSERVER_MAINFILENAME.Replace(".exe", "")).Length == 0)
                    {
                        playBtn.Text = "Running";
                    }
                    else
                    {
                        if (updateStatus.Text == "An error occured while fetching for updates!")
                        {
                            playBtn.Text = "Play\nOffline";
                        }
                        else
                        {
                            playBtn.Text = "Play";
                        }
                    }
                }
                await Task.Delay(500);
            } while (true);
        }

        public void ErrorHandler(object sender, ThreadExceptionEventArgs args)
        {
            if (args.Exception.GetType() == typeof(OutOfMemoryException))
            {
                MessageBox.Show("Your system has run out of application memory.\n\nTo avoid problems with your computer, quit any applications you are not using.\n\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
            else if (args.Exception.GetType() == typeof(DirectoryNotFoundException))
            {
                MessageBox.Show("A directory is missing!\n\n" + ErrorForm.FlattenException(args.Exception) + "\n\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
            else if (args.Exception.GetType() == typeof(Win32Exception))
            {
                MessageBox.Show(args.Exception.Message + "\n" + ErrorForm.FlattenException(args.Exception) + "\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
            else if (args.Exception.GetType() == typeof(IOException))
            {
                MessageBox.Show("An unknown error occured!\n\n" + ErrorForm.FlattenException(args.Exception) + "\n\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
            else if (args.Exception.GetType() == typeof(UnauthorizedAccessException))
            {
                MessageBox.Show("An error occurred while trying to access a file/folder.\n\nSomething is preventing " + MODLOADER_NAME_NOSPACES + " from working properly.Please consider disabling your antivirus or whitelisting the app in its settings.Also make sure you have Administrator rights on your computer.\n\n" + args.Exception.Message + "\n\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
            else
            {
                ErrorOccured(args.Exception);
                Application.Exit();
            }
        }

        public void ErrorHandler(object sender, UnhandledExceptionEventArgs args)
        {
            if (args.ExceptionObject.GetType() == typeof(TargetInvocationException))
            {
                TargetInvocationException exception = (TargetInvocationException)args.ExceptionObject;
                if (exception.InnerException != null && exception.InnerException.GetType() == typeof(UnauthorizedAccessException))
                {
                    MessageBox.Show("An error occurred while trying to access a file/folder.\n\nSomething is preventing " + MODLOADER_NAME_NOSPACES + " from working properly.Please consider disabling your antivirus or whitelisting the app in its settings.Also make sure you have Administrator rights on your computer.\n\n" + exception.InnerException.Message + "\n\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Application.Exit();
                }
            }

            ErrorOccured(args.ExceptionObject);
            Application.Exit();
        }

        public static bool isServerAvailable = false;
        private async void Form1_Load(object sender, EventArgs e)
        {
            var client = new TcpClient();
            var connectTask = client.ConnectAsync(SERVICE_FASTDL.ip, SERVICE_FASTDL.port);
            var waitTask = Task.Delay(15000);
            Exception error = new Exception(SERVICE_FASTDL.ip + ":" + SERVICE_FASTDL.port + " Timed Out!");
            if (await Task.WhenAny(connectTask, waitTask) == connectTask)
            {
                if (connectTask.Exception != null)
                {
                    error = connectTask.Exception;
                }
                if (connectTask.IsFaulted || connectTask.IsCanceled || connectTask.Exception != null)
                {
                    isServerAvailable = false;
                }
                else
                {
                    isServerAvailable = true;
                }
            }
            if (!isServerAvailable)
            {
                if (error.InnerException != null)
                {
                    error = error.InnerException;
                }
                MessageBox.Show("A problem occured while connecting to the " + MODLOADER_DEVTEAM_NAME + " servers.\n\nPlease check your internet connection and try again\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                offlineMode.Show();
            }
            else
            {
                offlineMode.Hide();
            }
            await FadeIn(this, 5);
            CheckInstall();
        }
        bool isCoremodDownloaded = false;
        bool isAssetsDownloaded = false;

        async void DownloadModLoaderFiles()
        {
            isCoremodDownloaded = false;
            isAssetsDownloaded = false;
            if (latestUpdateInfo.coremod_url.EndsWith(".dll", StringComparison.InvariantCulture))
            {
                WebClient coremodDllClient = new WebClient();
                coremodDllClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(CoreModDllProgressChanged);
                coremodDllClient.DownloadFileCompleted += new AsyncCompletedEventHandler(CoreModDllDownloadCompleted);
                coremodDllClient.DownloadFileAsync(new Uri(latestUpdateInfo.coremod_url), Path.Combine(FOLDER_BINARIES, FILES_COREMOD));
            }

            if (latestUpdateInfo.assets_url.EndsWith(".assets", StringComparison.InvariantCulture))
            {
                WebClient assetsClient = new WebClient();
                assetsClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(AssetsProgressChanged);
                assetsClient.DownloadFileCompleted += new AsyncCompletedEventHandler(AssetsDownloadCompleted);
                assetsClient.DownloadFileAsync(new Uri(latestUpdateInfo.assets_url), Path.Combine(FOLDER_BINARIES, FILES_ASSETS));
            }

            while (isCoremodDownloaded == false || isAssetsDownloaded == false)
            {
                await Task.Delay(1);
            }

            if (CalculateSHA512(Path.Combine(FOLDER_BINARIES, FILES_COREMOD)).Equals(latestUpdateInfo.coremod_hash, StringComparison.InvariantCulture) && CalculateSHA512(Path.Combine(FOLDER_BINARIES, FILES_ASSETS)).Equals(latestUpdateInfo.assets_hash, StringComparison.InvariantCulture))
            {
                CurrentConfig.coreVersion = latestUpdateInfo.version;
                UpdateConfigFile();
                updateStatus.Text = MODLOADER_NAME_NOSPACES + " is up to date! (v" + CurrentConfig.coreVersion + ")";
                playBtn.Show();
            }
            else
            {
                CurrentConfig.coreVersion = "?";
                updateStatus.Text = "An error occured while downloading the update!";
                MessageBox.Show("An error occured while downloading the update! The downloaded file has a different hash than the requested file!\n\nLocal " + FILES_COREMOD + " Hash = " + CalculateSHA512(Path.Combine(FOLDER_BINARIES, FILES_COREMOD)) + "\nRequested " + FILES_COREMOD + " Hash = " + latestUpdateInfo.coremod_hash + "\n\nLocal " + FILES_ASSETS + " Hash = " + CalculateSHA512(Path.Combine(FOLDER_BINARIES, FILES_ASSETS)) + "\nRequested " + FILES_ASSETS + " Hash = " + latestUpdateInfo.assets_hash + "\n\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                updateStatus.ForeColor = Color.IndianRed;
                UpdateConfigFile();
                return;
            }
        }

        private async void CoreModDllProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            noDownloadsLabel.Hide();
            downloadInfo.Show();
            double bytesIn = double.Parse(e.BytesReceived.ToString());
            double totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
            double percentage = bytesIn / totalBytes * 100;
            coremodPanel.Show();
            coremodPercentage.Text = Math.Round(percentage).ToString() + "%";
            float progress = (250f / 100f) * (float)Math.Round(percentage);
            coremodBar.Size = new Size((int)Math.Floor(progress), 25);
            await Task.Delay(0);
        }

        private void CoreModDllDownloadCompleted(object sender, AsyncCompletedEventArgs e)
        {
            isCoremodDownloaded = true;
            noDownloadsLabel.Show();
            downloadInfo.Hide();
            coremodPanel.Hide();
        }

        private async void AssetsProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            noDownloadsLabel.Hide();
            downloadInfo.Show();
            double bytesIn = double.Parse(e.BytesReceived.ToString());
            double totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
            double percentage = bytesIn / totalBytes * 100;
            assetsPanel.Show();
            assetsPercentage.Text = Math.Round(percentage).ToString() + "%";
            float progress = (250f / 100f) * (float)Math.Round(percentage);
            assetsBar.Size = new Size((int)Math.Floor(progress), 25);
            await Task.Delay(0);
        }

        private void AssetsDownloadCompleted(object sender, AsyncCompletedEventArgs e)
        {
            isAssetsDownloaded = true;
            noDownloadsLabel.Show();
            downloadInfo.Hide();
            assetsPanel.Hide();
        }

        async void CheckInstall()
        {
            try
            {
                try
                {
                    Directory.CreateDirectory(FOLDER_MAIN);
                    Directory.CreateDirectory(FOLDER_BINARIES);
                    Directory.CreateDirectory(FOLDER_CACHE);
                    Directory.CreateDirectory(FOLDER_CACHE_MODS);
                    Directory.CreateDirectory(FOLDER_CACHE_TEXTURES);
                    Directory.CreateDirectory(FOLDER_CACHE_TEMPORARY);
                }
                catch (Exception e)
                {
                    MessageBox.Show("A problem occured during the " + MODLOADER_NAME_NOSPACES + " installation!\n\n" + ErrorForm.FlattenException(e) + "\n\nPlease check if you have admin rights and try again.\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Application.Exit();
                }
                if (!File.Exists(CONFIGURATION_FILE))
                {
                    LauncherConfiguration newconfig = new LauncherConfiguration();
                    CurrentConfig = newconfig;
                    SaveLauncherConfiguration();
                }
                else
                {
                    try
                    {
                        CurrentConfig = GetLauncherConfiguration();
                    }
                    catch
                    {
                        File.Delete(CONFIGURATION_FILE);
                        CheckInstall();
                        return;
                    }
                }
                if (CurrentConfig.agreeWithTOS == 0)
                {
                    Form tosform = new TOSForm(this);
                    DialogResult result = tosform.ShowDialog();
                    if (result != DialogResult.Yes)
                    {
                        Application.Exit();
                    }
                }
                if (!IsGamePathValid(CurrentConfig.gamePath))
                {
                    await FindGameFolder();
                }
                try
                {
                    if (IsGamePathValid(CurrentConfig.gamePath))
                    {
                        if (!Directory.Exists(Path.Combine(CurrentConfig.gamePath, "mods")))
                        {
                            Directory.CreateDirectory(Path.Combine(CurrentConfig.gamePath, "mods"));
                        }
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show("A problem occured during the " + MODLOADER_NAME_NOSPACES + " installation!\n\n" + ErrorForm.FlattenException(e) + "\n\nPlease check if you have admin rights and try again.\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Application.Exit();
                }

                UpdateConfigFile();
#if GAME_IS_RAFT
                ChangeDisableUnityAudio(Path.Combine(CurrentConfig.gamePath, @"Raft_Data\globalgamemanagers"), false);
#endif
                gamePathLabel.Text = CurrentConfig.gamePath;
#if GAME_IS_GREENHELL
                checkBoxSplashScreen.Checked = CurrentConfig.skipSplashScreen;
#endif
                disableProcessStart.Checked = CurrentConfig.disableAutomaticGameStart;
            }
            catch (Exception e)
            {
                MessageBox.Show("A problem occured during the " + MODLOADER_NAME_NOSPACES + " installation!\n\n" + ErrorForm.FlattenException(e) + "\n\nPlease delete the " + MODLOADER_NAME_NOSPACES + " folder located at \"%appdata%/" + MODLOADER_NAME_NOSPACES + "\" and try again.\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }

            await CheckForFilesUpdates();
        }

        private async void playBtn_MouseDown(object sender, MouseEventArgs e)
        {
            if (updateStatus.Text.StartsWith("An error occured while fetching for updates!"))
            {
                if (File.Exists(Path.Combine(FOLDER_BINARIES, FILES_COREMOD)) && File.Exists(Path.Combine(FOLDER_BINARIES, FILES_ASSETS)))
                {
                    MessageBox.Show("As an error occured while fetching for updates the ModLoader might not work correctly. \n\nIf it does not work, please check your internet connection and try again later.\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Your ModLoader installation is missing important files and we couldn't download them! please check your internet connection and try again later.\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Application.Exit();
                }
            }
            else if (!updateStatus.Text.StartsWith(MODLOADER_NAME_NOSPACES + " is up to date!"))
            {
                return;
            }
            Process[] processes = new Process[0];
            bool startedWithExe = false;
            if (!CurrentConfig.disableAutomaticGameStart)
            {
                if (Process.GetProcessesByName(GAME_MAINFILENAME.Replace(".exe", "")).Length > 0 && Process.GetProcessesByName(DEDICATEDSERVER_MAINFILENAME.Replace(".exe", "")).Length == 0) { return; }
                try
                {
                    Process.Start("steam://run/" + STEAM_APPID);
                }
                catch
                {
                    MessageBox.Show("Warning! We wasn't able to start the game from steam using steam://run/" + STEAM_APPID + ", The game will start from the executable file, this is not supported and the game might crash!\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", "Warning! Failed to start game using Steam!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    try
                    {
                        Process game = Process.Start(Path.Combine(CurrentConfig.gamePath, GAME_MAINFILENAME));
                        processes = new Process[1] { game };
                        startedWithExe = true;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            else
            {
                updateStatus.Text = "Please start the game from steam...";
            }

            if (!startedWithExe)
            {
                bool isReady = false;
                int secondsRemaining = 15;
                while (!isReady && secondsRemaining > 0)
                {
                    processes = Process.GetProcessesByName(GAME_MAINFILENAME.Replace(".exe", ""));
                    if (processes.Length >= 1)
                    {
                        try
                        {
                            const ProcessAccessRights flags = ProcessAccessRights.PROCESS_ALL_ACCESS;
                            IntPtr handle;

                            if ((handle = Native.OpenProcess(flags, false, processes[0].Id)) != IntPtr.Zero)
                            {
                                if (ProcessUtils.GetMonoModule(handle, out IntPtr mono))
                                {
                                    isReady = true;
                                    updateStatus.Text = "Found " + GAME_NAME + " process!";
                                }
                                Native.CloseHandle(handle);
                            }
                        }
                        catch { }
                    }
                    if (!CurrentConfig.disableAutomaticGameStart)
                    {
                        updateStatus.Text = "Searching " + GAME_NAME + " process (" + secondsRemaining + "s)...";
                    }
                    await Task.Delay(1000);
                    if (!CurrentConfig.disableAutomaticGameStart)
                        secondsRemaining--;
                }

                if (!CurrentConfig.disableAutomaticGameStart && secondsRemaining <= 0)
                {
                    updateStatus.Text = "Couldn't find " + GAME_NAME + " process. Attempt #2";
                    processes = Process.GetProcessesByName(GAME_MAINFILENAME.Replace(".exe", ""));
                    processes.ToList().ForEach(pr => pr.Kill());
                    await Task.Delay(1000);
                    Process game = Process.Start(Path.Combine(CurrentConfig.gamePath, GAME_MAINFILENAME));
                    processes = new Process[1] { game };
                    try
                    {
                        const ProcessAccessRights flags = ProcessAccessRights.PROCESS_QUERY_INFORMATION | ProcessAccessRights.PROCESS_VM_READ;
                        IntPtr handle;

                        if ((handle = Native.OpenProcess(flags, false, processes[0].Id)) != IntPtr.Zero)
                        {
                            if (ProcessUtils.GetMonoModule(handle, out IntPtr mono))
                            {
                                if (!processes[0].WaitForInputIdle(0))
                                {
                                    isReady = true;
                                    updateStatus.Text = "Found " + GAME_NAME + " process!";
                                }
                            }
                            Native.CloseHandle(handle);
                        }
                    }
                    catch { }
                }
            }
            Process p = processes[0];
            int remainingTries = 10;
            string step = "inject";
            switch (step)
            {
                case "inject":
                    if (!processes[0].HasExited)
                    {
                        updateStatus.Text = "Injecting...";
                        try
                        {
                            new Injector(processes[0].Id).Inject(Program.GetEmbeddedFile("Resources.AssemblyLoader.dll"), "AssemblyLoader", "Loader", "Load");
                            updateStatus.Text = "Successfully Injected!";
                        }
                        catch (Exception injectexception)
                        {
                            if (remainingTries > 0)
                            {
                                updateStatus.Text = "Injection Failed! Retrying... #" + remainingTries;
                                await Task.Delay(1000);
                                remainingTries--;
                                goto case "inject";
                            }
                            updateStatus.Text = "An error occured during injection!";
                            throw injectexception;
                        }
                    }
                    break;
            }
            await Task.Delay(2000);
            updateStatus.Text = MODLOADER_NAME_NOSPACES + " is up to date! (v" + CurrentConfig.coreVersion + ")";
            playBtn.Show();
        }

        public bool IsProcessReadyForInjection(int processId)
        {
            const ProcessAccessRights flags = ProcessAccessRights.PROCESS_QUERY_INFORMATION | ProcessAccessRights.PROCESS_VM_READ;
            IntPtr handle;

            if ((handle = Native.OpenProcess(flags, false, processId)) != IntPtr.Zero)
            {
                if (ProcessUtils.GetMonoModule(handle, out IntPtr mono))
                {
                    return true;
                }

                Native.CloseHandle(handle);
            }
            return false;
        }

        async Task FindGameFolder()
        {
            updateStatus.Text = "Searching for your " + GAME_NAME + " installation...";
            try
            {
                RegistryKey key = Registry.LocalMachine.OpenSubKey("Software\\Wow6432Node\\Valve\\Steam");
                string installPath = key.GetValue("InstallPath") as string;
                if (!File.Exists(Path.Combine(installPath, "config\\config.vdf")))
                {
                    key = Registry.LocalMachine.OpenSubKey("Software\\Valve\\Steam");
                    installPath = key.GetValue("InstallPath") as string;
                    if (!File.Exists(Path.Combine(installPath, "config\\config.vdf")))
                    {
                        installPath = @"C:\Program Files (x86)\Steam";
                    }
                }

                if (Directory.Exists(Path.Combine(installPath, "steamapps\\common\\" + GAME_MAINFOLDERNAME)))
                {
                    if (IsGamePathValid(Path.Combine(installPath, "steamapps\\common\\" + GAME_MAINFOLDERNAME)))
                    {
                        FoundGameFolder(Path.Combine(installPath, "steamapps\\common\\" + GAME_MAINFOLDERNAME));
                        return;
                    }
                }


                if (File.Exists(Path.Combine(installPath, "config\\config.vdf")))
                {
                    string potentialDirectory1 = "";
                    string potentialDirectory2 = "";
                    string potentialDirectory3 = "";
                    try
                    {
                        dynamic steamconfig = VdfConvert.Deserialize(File.ReadAllText(Path.Combine(installPath, "config\\config.vdf")));
                        if (steamconfig.Value["Software"]["Valve"]["Steam"].ContainsKey("BaseInstallFolder_1"))
                        {
                            potentialDirectory1 = Path.Combine(steamconfig.Value["Software"]["Valve"]["Steam"]["BaseInstallFolder_1"].ToString(), "steamapps\\common\\" + GAME_MAINFOLDERNAME);
                        }
                        if (steamconfig.Value["Software"]["Valve"]["Steam"].ContainsKey("BaseInstallFolder_2"))
                        {
                            potentialDirectory2 = Path.Combine(steamconfig.Value["Software"]["Valve"]["Steam"]["BaseInstallFolder_2"].ToString(), "steamapps\\common\\" + GAME_MAINFOLDERNAME);
                        }
                        if (steamconfig.Value["Software"]["Valve"]["Steam"].ContainsKey("BaseInstallFolder_3"))
                        {
                            potentialDirectory3 = Path.Combine(steamconfig.Value["Software"]["Valve"]["Steam"]["BaseInstallFolder_3"].ToString(), "steamapps\\common\\" + GAME_MAINFOLDERNAME);
                        }
                    }
                    catch { }

                    if (IsGamePathValid(potentialDirectory1))
                    {
                        FoundGameFolder(potentialDirectory1);
                        return;
                    }
                    if (IsGamePathValid(potentialDirectory2))
                    {
                        FoundGameFolder(potentialDirectory2);
                        return;
                    }
                    if (IsGamePathValid(potentialDirectory3))
                    {
                        FoundGameFolder(potentialDirectory3);
                        return;
                    }
                }
            }
            catch { }


            try
            {
                bool enableSteamMethod = false;

                DialogResult SteamDialogResult = MessageBox.Show("Do you want to use Steam to automatically find your " + GAME_NAME + " folder?", "Sorry, we weren't able to automatically find your " + GAME_NAME + " folder!", MessageBoxButtons.YesNo);
                if (SteamDialogResult == DialogResult.Yes)
                {
                    enableSteamMethod = true;
                }
                else
                {
                    enableSteamMethod = false;
                }

                if (enableSteamMethod)
                {
                    int remainingSeconds = 30;
                    updateStatus.Text = "Searching for your " + GAME_NAME + " installation (30s)...";
                    try
                    {
                        Process.Start("steam://run/" + STEAM_APPID);
                        Process[] gameProcesses = Process.GetProcessesByName(GAME_MAINFILENAME.Replace(".exe", ""));
                        do
                        {
                            gameProcesses = Process.GetProcessesByName(GAME_MAINFILENAME.Replace(".exe", ""));
                            await Task.Delay(999);
                            updateStatus.Text = "Searching for your " + GAME_NAME + " installation (" + remainingSeconds + "s)...";
                            remainingSeconds--;
                        } while (gameProcesses.Length == 0 && remainingSeconds > 0);

                        if (gameProcesses.Length > 0)
                        {
                            string temp = GetPathToApp(gameProcesses[0]);
                            foreach (Process p in gameProcesses)
                            {
                                p.Kill();
                            }
                            if (!string.IsNullOrWhiteSpace(temp) && !temp.ToLower().Contains("system32"))
                            {
                                if (IsGamePathValid(Path.GetDirectoryName(temp)))
                                {
                                    FoundGameFolder(Path.GetDirectoryName(temp));
                                    return;
                                }
                            }
                        }
                    }
                    catch { }
                }
            }
            catch { }

            updateStatus.Text = "Please manually select your " + GAME_NAME + " folder.";
            int step = 0;
            switch (step)
            {
                case 0:
                    DialogResult dialogResult = MessageBox.Show("To install " + MODLOADER_NAME_NOSPACES + " please manually select your " + GAME_NAME + " folder by pressing \"OK\" to continue, If you want to abort press \"Cancel\".", "Sorry, we weren't able to automatically find your " + GAME_NAME + " folder!", MessageBoxButtons.OKCancel);
                    if (dialogResult == DialogResult.OK)
                    {
                        step = 1;
                        goto case 1;
                    }
                    else
                    {
                        await CloseForm(this, 1);
                    }
                    break;
                case 1:

                    var dialog = new FolderSelectDialog
                    {
                        InitialDirectory = "",
                        Title = "Please Select Your " + GAME_NAME + " Folder"
                    };
                    if (dialog.Show(Handle))
                    {
                        string path = dialog.FileName;
                        if (IsGamePathValid(path))
                        {
                            FoundGameFolder(path);
                            return;
                        }
                        else
                        {
                            DialogResult dialogResult2 = MessageBox.Show(MODLOADER_NAME_NOSPACES + " is unable to locate " + GAME_MAINFILENAME + " in the selected folder. If you want to retry press OK, if you want to abort press cancel or close this window.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                            if (dialogResult2 == DialogResult.OK)
                            {
                                goto case 1;
                            }
                            else
                            {
                                await CloseForm(this, 1);
                            }
                        }
                    }
                    break;
            }

            await CloseForm(this, 1);
        }

        void ErrorOccured(object _e)
        {
            Icon = Properties.Resources.error;
            Form eform = new ErrorForm(_e);
            eform.Icon = Properties.Resources.error;
            eform.ShowDialog();
            Close();
            Application.Exit();
        }

        void FoundGameFolder(string path)
        {
            CurrentConfig.gamePath = path;
            UpdateConfigFile();
        }

        bool IsGamePathValid(string path)
        {
            if (File.Exists(Path.Combine(path, GAME_MAINFILENAME)) && Directory.Exists(Path.Combine(path, GAME_DATAFOLDERNAME + "\\Managed")))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void UpdateConfigFile()
        {
            try
            {
                SaveLauncherConfiguration();
            }
            catch (Exception e)
            {
                MessageBox.Show("A problem occured while updating the " + MODLOADER_NAME_NOSPACES + " configuration file.\n\n" + ErrorForm.FlattenException(e) + "\n\nPlease check if you have admin rights and try again.\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
        }

        [DllImport("Kernel32.dll")]
        static extern uint QueryFullProcessImageName(IntPtr hProcess, uint flags, StringBuilder text, out uint size);

        private string GetPathToApp(Process proc)
        {
            try
            {
                string pathToExe = string.Empty;

                if (null != proc)
                {
                    uint nChars = 256;
                    StringBuilder Buff = new StringBuilder((int)nChars);

                    uint success = QueryFullProcessImageName(proc.Handle, 0, Buff, out nChars);

                    if (0 != success)
                    {
                        pathToExe = Buff.ToString();
                    }
                    else
                    {
                        return "";
                    }
                }

                return pathToExe;
            }
            catch { }
            return "";
        }

        void ClearCache(bool modsCache)
        {
            DirectoryInfo di = new DirectoryInfo(modsCache ? FOLDER_CACHE_MODS : FOLDER_CACHE_TEXTURES);
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                dir.Delete(true);
            }
            Launcher.SettingsTab.UpdateCacheSize();
        }

        public async Task CheckForFilesUpdates()
        {
            try
            {
                updateStatus.Text = "Checking for updates...";
                Task<string> task = RequestData("https://" + SERVICE_FASTDL.ip + "/data/client.php");
                var waitTask = Task.Delay(15000);
                Exception error = new Exception("https://" + SERVICE_FASTDL.ip + "/data/client.php Timed Out!");
                if (await Task.WhenAny(task, waitTask) == task)
                {
                    if (task.Result.Length > 10 && !task.IsFaulted && task.Exception == null && !task.IsCanceled && task.IsCompleted)
                    {
                        latestUpdateInfo = ReadModLoaderVersion(task.Result);
                        if (CurrentConfig.coreVersion != latestUpdateInfo.version || !File.Exists(Path.Combine(FOLDER_BINARIES, FILES_COREMOD)) || !File.Exists(Path.Combine(FOLDER_BINARIES, FILES_ASSETS)))
                        {
                            UpdateAvailable form = new UpdateAvailable(latestUpdateInfo);
                            DialogResult result = form.ShowDialog();
                            if (result == DialogResult.Yes)
                            {
                                updateStatus.Text = "Downloading latest update...";
                                DownloadModLoaderFiles();
                            }
                            else
                            {
                                Application.Exit();
                            }
                        }
                        else
                        {
                            updateStatus.Text = MODLOADER_NAME_NOSPACES + " is up to date! (v" + CurrentConfig.coreVersion + ")";
                            playBtn.Show();
                        }
                    }
                    else
                    {
                        if (task.Exception != null)
                        {
                            MessageBox.Show("An error occured while fetching for updates\n\n" + ErrorForm.FlattenException(task.Exception) + "\n\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            MessageBox.Show("An error occured while fetching for updates\n\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("An error occured while fetching for updates\n\n" + ErrorForm.FlattenException(error) + "\n\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occured while fetching for updates\n\n" + ErrorForm.FlattenException(ex) + "\n\nIf the problem persists, please visit " + DISCORD_INVITEURL + " and ask for help in the #support channel.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                updateStatus.Text = "An error occured while fetching for updates!";
                playBtn.Show();
                playBtn.Text = "Play\nOffline";
            }
        }

        public static async Task<string> RequestData(string uri)
        {
            return await new WebClient().DownloadStringTaskAsync(uri);
        }

        private async Task FadeIn(Form o, int interval = 80)
        {
            await Task.Delay(50);
            while (o.Opacity < 1.0)
            {
                await Task.Delay(interval);
                o.Opacity += 0.07;
            }
            o.Opacity = 1;
        }

        public async void ReopenForm(int interval = 80)
        {
            var o = this;
            while (o.Opacity < 1.0)
            {
                await Task.Delay(interval);
                o.Opacity += 0.07;
            }
            o.Opacity = 1;
        }

        public async void OpenAnotherForm(Form o, int interval = 80)
        {
            while (o.Opacity > 0.0)
            {
                await Task.Delay(interval);
                o.Opacity -= 0.07;
            }
            o.Opacity = 0;
        }

        private async Task CloseForm(Form o, int interval = 80)
        {
            while (o.Opacity > 0.0)
            {
                await Task.Delay(interval);
                o.Opacity -= 0.07;
            }
            o.Opacity = 0;
            Application.Exit();
        }

        private void btnClick_ToggleDownloads(object sender, MouseEventArgs e)
        {
            DownloadsTab.IsOpened = !DownloadsTab.IsOpened;
            if (DownloadsTab.IsOpened)
            {
                DownloadsTab.OnTabOpen();
            }
            else
            {
                DownloadsTab.OnTabClose();
            }
        }

        private void btnClick_ToggleServicesStatus(object sender, MouseEventArgs e)
        {
            ServicesTab.IsOpened = !ServicesTab.IsOpened;
            if (ServicesTab.IsOpened)
            {
                ServicesTab.OnTabOpen();
            }
            else
            {
                ServicesTab.OnTabClose();
            }
        }

        private void btnClick_ToggleSettingsStatus(object sender, MouseEventArgs e)
        {
            SettingsTab.IsOpened = !SettingsTab.IsOpened;
            if (SettingsTab.IsOpened)
            {
                SettingsTab.OnTabOpen();
            }
            else
            {
                SettingsTab.OnTabClose();
            }
        }

        private void btnClick_Website(object sender, MouseEventArgs e)
        {
            Process.Start("https://www." + MODLOADER_SITE);
        }

        private void btnClick_Discord(object sender, MouseEventArgs e)
        {
            Process.Start("https://" + DISCORD_INVITEURL);
        }

        private async void closeBtn_Click(object sender, EventArgs e)
        {
            await CloseForm(this, 1);
        }

        private void playBtn_MouseLeave(object sender, EventArgs e)
        {
#if GAME_IS_GREENHELL
            playBtn.BackgroundImage = Properties.Resources.greenhell_playBtn;
#elif GAME_IS_RAFT
            playBtn.BackgroundImage = Properties.Resources.raft_playBtn;
#endif
        }

        private void playBtn_MouseEnter(object sender, EventArgs e)
        {
#if GAME_IS_GREENHELL
            playBtn.BackgroundImage = Properties.Resources.greenhell_playBtn_hover;
#elif GAME_IS_RAFT
            playBtn.BackgroundImage = Properties.Resources.raft_playBtn_hover;
#endif
        }

        private void downloadPanelCloseBtn_MouseEnter(object sender, EventArgs e)
        {
            downloadPanelCloseBtn.Size = new Size(25, 25);
            downloadPanelCloseBtn.Location = new Point(294, 7);
        }

        private void downloadPanelCloseBtn_MouseLeave(object sender, EventArgs e)
        {
            downloadPanelCloseBtn.Size = new Size(20, 20);
            downloadPanelCloseBtn.Location = new Point(296, 10);
        }

        private void downloadPanelCloseBtn_MouseClick(object sender, MouseEventArgs e)
        {
            DownloadsTab.OnTabClose();
        }

        private void settingsPanelCloseBtn_MouseClick(object sender, MouseEventArgs e)
        {
            SettingsTab.OnTabClose();
        }

        private void statusPanelCloseBtn_MouseEnter(object sender, EventArgs e)
        {
            statusPanelCloseBtn.Size = new Size(25, 25);
            statusPanelCloseBtn.Location = new Point(7, 7);
        }

        private void statusPanelCloseBtn_MouseLeave(object sender, EventArgs e)
        {
            statusPanelCloseBtn.Size = new Size(20, 20);
            statusPanelCloseBtn.Location = new Point(10, 10);
        }

        private void statusPanelCloseBtn_MouseClick(object sender, MouseEventArgs e)
        {
            ServicesTab.OnTabClose();
        }

        bool mouseDown;
        int mouseX = 0, mouseY = 0;
        int mouseinX = 0, mouseinY = 0;

        private void DragMouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
            mouseinX = MousePosition.X - Bounds.X;
            mouseinY = MousePosition.Y - Bounds.Y;
        }

        private void DragMouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        private void downloadsBtn_MouseEnter(object sender, EventArgs e)
        {
#if GAME_IS_GREENHELL
            downloadsBtn.BackgroundImage = Properties.Resources.greenhell_download;
#elif GAME_IS_RAFT
            downloadsBtn.BackgroundImage = Properties.Resources.raft_download;
#endif
            downloadsLabel.ForeColor = THEME_HOVEREDLABEL_COLOR;
        }

        private void downloadsBtn_MouseLeave(object sender, EventArgs e)
        {
            downloadsBtn.BackgroundImage = Properties.Resources.download;
            downloadsLabel.ForeColor = Color.White;
        }

        private void websiteBtn_MouseEnter(object sender, EventArgs e)
        {
#if GAME_IS_GREENHELL
            websiteBtn.BackgroundImage = Properties.Resources.greenhell_website;
#elif GAME_IS_RAFT
            websiteBtn.BackgroundImage = Properties.Resources.raft_website;
#endif
            websiteLabel.ForeColor = THEME_HOVEREDLABEL_COLOR;
        }

        private void websiteBtn_MouseLeave(object sender, EventArgs e)
        {
            websiteBtn.BackgroundImage = Properties.Resources.website;
            websiteLabel.ForeColor = Color.White;
        }

        private void discordBtn_MouseEnter(object sender, EventArgs e)
        {
#if GAME_IS_GREENHELL
            discordBtn.BackgroundImage = Properties.Resources.greenhell_discord;
#elif GAME_IS_RAFT
            discordBtn.BackgroundImage = Properties.Resources.raft_discord;
#endif
            discordLabel.ForeColor = THEME_HOVEREDLABEL_COLOR;
        }

        private void discordBtn_MouseLeave(object sender, EventArgs e)
        {
            discordBtn.BackgroundImage = Properties.Resources.discord;
            discordLabel.ForeColor = Color.White;
        }

        private void statusBtn_MouseEnter(object sender, EventArgs e)
        {
#if GAME_IS_GREENHELL
            statusBtn.BackgroundImage = Properties.Resources.greenhell_ping;
#elif GAME_IS_RAFT
            statusBtn.BackgroundImage = Properties.Resources.raft_ping;
#endif
            statusLabel.ForeColor = THEME_HOVEREDLABEL_COLOR;
        }

        private void statusBtn_MouseLeave(object sender, EventArgs e)
        {
            statusBtn.BackgroundImage = Properties.Resources.ping;
            statusLabel.ForeColor = Color.White;
        }

        private void settingsBtn_MouseEnter(object sender, EventArgs e)
        {
#if GAME_IS_GREENHELL
            settingsBtn.BackgroundImage = Properties.Resources.greenhell_settings;
#elif GAME_IS_RAFT
            settingsBtn.BackgroundImage = Properties.Resources.raft_settings;
#endif
            settingsLabel.ForeColor = THEME_HOVEREDLABEL_COLOR;
        }

        private void settingsBtn_MouseLeave(object sender, EventArgs e)
        {
            settingsBtn.BackgroundImage = Properties.Resources.settings;
            settingsLabel.ForeColor = Color.White;
        }

        private void settingsPanelCloseBtn_MouseEnter(object sender, EventArgs e)
        {
            settingsPanelCloseBtn.Size = new Size(25, 25);
            settingsPanelCloseBtn.Location = new Point(7, 7);
        }

        private void settingsPanelCloseBtn_MouseLeave(object sender, EventArgs e)
        {
            settingsPanelCloseBtn.Size = new Size(20, 20);
            settingsPanelCloseBtn.Location = new Point(10, 10);
        }

        private void DragMouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                mouseX = MousePosition.X - mouseinX;
                mouseY = MousePosition.Y - mouseinY;

                SetDesktopLocation(mouseX, mouseY);
            }
        }

        private void ModsCacheClearBtn_MouseDown(object sender, MouseEventArgs e)
        {
            ClearCache(true);
        }

        private void TexturesCacheClearBtn_MouseDown(object sender, MouseEventArgs e)
        {
            ClearCache(false);
        }

        private void gamePathBtn_MouseDown(object sender, MouseEventArgs e)
        {

            var dialog = new FolderSelectDialog
            {
                InitialDirectory = "",
                Title = "Please Select Your " + GAME_NAME + " Folder"
            };
            if (dialog.Show(Handle))
            {
                string path = dialog.FileName;
                if (File.Exists(Path.Combine(path, GAME_MAINFILENAME)))
                {
                    CurrentConfig.gamePath = path;
                    UpdateConfigFile();
                    return;
                }
                else
                {
                    MessageBox.Show(MODLOADER_NAME_NOSPACES + " is unable to locate " + GAME_MAINFILENAME + " in the selected folder. If you want to retry press OK, if you want to abort press cancel or close this window.", MODLOADER_NAME_NOSPACES + " - An error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        public static int Clamp(int value, int min, int max)
        {
            return (value < min) ? min : (value > max) ? max : value;
        }

        private void pictureBox2_MouseEnter(object sender, EventArgs e)
        {
#if GAME_IS_GREENHELL
            modCreatorIcon.BackgroundImage = Properties.Resources.greenhell_modcreator;
#elif GAME_IS_RAFT
            modCreatorIcon.BackgroundImage = Properties.Resources.raft_modcreator;
#endif
            modCreatorLabel.ForeColor = THEME_HOVEREDLABEL_COLOR;
        }

        private void pictureBox2_MouseLeave(object sender, EventArgs e)
        {
            modCreatorIcon.BackgroundImage = Properties.Resources.mod;
            modCreatorLabel.ForeColor = Color.White;
        }

        private void pictureBox1_MouseEnter(object sender, EventArgs e)
        {
#if GAME_IS_GREENHELL
            modsFolderIcon.BackgroundImage = Properties.Resources.greenhell_folder;
#elif GAME_IS_RAFT
            modsFolderIcon.BackgroundImage = Properties.Resources.raft_folder;
#endif
            modsFolderLabel.ForeColor = THEME_HOVEREDLABEL_COLOR;
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            modsFolderIcon.BackgroundImage = Properties.Resources.folder;
            modsFolderLabel.ForeColor = Color.White;
        }

        private void OpenModsFolder(object sender, MouseEventArgs e)
        {
            if (Directory.Exists(CurrentConfig.gamePath))
            {
                Directory.CreateDirectory(Path.Combine(CurrentConfig.gamePath, "mods"));
                Directory.CreateDirectory(Path.Combine(CurrentConfig.gamePath, "mods/ModData"));

                if (updateStatus.Text.StartsWith(MODLOADER_NAME_NOSPACES + " is up to date!"))
                {
                    Process.Start(Path.Combine(CurrentConfig.gamePath, "mods"));
                }
            }
        }

        private void OpenModCreator(object sender, MouseEventArgs e)
        {
            if (updateStatus.Text.StartsWith(MODLOADER_NAME_NOSPACES + " is up to date!"))
            {
                OpenAnotherForm(this, 1);
                Form modcreator = new ModCreator(this);
                modcreator.ShowDialog();
            }
        }

        private void minimizeBtn_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void minimizeBtn_MouseEnter(object sender, EventArgs e)
        {
            Color color = THEME_TOPBAR_BUTTONS_HOVER_COLOR;
            minimizeBtnHitbox.BackColor = color;
            minimizeBtn.BackColor = color;
        }

        private void minimizeBtn_MouseLeave(object sender, EventArgs e)
        {
            Color color = THEME_TOPBAR_COLOR;
            minimizeBtnHitbox.BackColor = color;
            minimizeBtn.BackColor = color;
        }

        private void closeBtn_MouseEnter(object sender, EventArgs e)
        {
            Color color = THEME_TOPBAR_BUTTONS_HOVER_COLOR;
            closeBtn_Hitbox.BackColor = color;
            closeBtn.BackColor = color;
        }

        private void closeBtn_MouseLeave(object sender, EventArgs e)
        {
            Color color = THEME_TOPBAR_COLOR;
            closeBtn_Hitbox.BackColor = color;
            closeBtn.BackColor = color;
        }

        private void uninstall_Click(object sender, EventArgs e)
        {
            string remainingFiles = "";

            GC.Collect();
            GC.WaitForPendingFinalizers();

            bool _modsFolder = false;
            if (Directory.Exists(Path.Combine(CurrentConfig.gamePath, "mods")))
            {
                try
                {
                    Empty(new DirectoryInfo(Path.Combine(CurrentConfig.gamePath, "mods")));
                    _modsFolder = true;
                }
                catch { }
            }
            if (!_modsFolder)
            {
                remainingFiles += "Mods folder (" + Path.Combine(CurrentConfig.gamePath, "mods") + ")\n";
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();

            bool _dataFolder = false;
            if (Directory.Exists(FOLDER_MAIN))
            {
                try
                {
                    bool success = Empty(new DirectoryInfo(FOLDER_MAIN));
                    _dataFolder = true;
                }
                catch { }
            }
            if (!_dataFolder)
            {
                remainingFiles += MODLOADER_NAME_NOSPACES + " data folder (" + FOLDER_MAIN + ")\n";
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();

            bool _registry = false;
            try
            {
                RegistryKey key = Registry.CurrentUser.OpenSubKey(@"HyTeKGames\" + MODLOADER_NAME_NOSPACES);
                if (key != null)
                {
                    Registry.CurrentUser.DeleteSubKeyTree(@"HyTeKGames\" + MODLOADER_NAME_NOSPACES);
                }
                _registry = true;
            }
            catch { }
            if (!_registry)
            {
                remainingFiles += MODLOADER_NAME_NOSPACES + " registry values (HKEY_CURRENT_USER/HyTeKGames/" + MODLOADER_NAME_NOSPACES + ")\n";
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();

#if GAME_IS_RAFT
            Process.Start("steam://validate/" + STEAM_APPID);
#endif

            if (remainingFiles == "")
            {
                MessageBox.Show(MODLOADER_NAME_NOSPACES + " has been successfully uninstalled!", MODLOADER_NAME_NOSPACES + " has been successfully uninstalled!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(MODLOADER_NAME_NOSPACES + " has been uninstalled, however there is still some remaining files that you need to manually delete!\nRemaining files and their location : \n\n" + remainingFiles + "\n\nAs " + MODLOADER_NAME_NOSPACES + " override some game files, steam needs to redownload 1 file. If the steam game verification doesn't automatically start, please manually verify your game files.", MODLOADER_NAME_NOSPACES + " has been successfully uninstalled!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            Application.Exit();
        }

        static string CalculateSHA512(string filename)
        {
            if (!File.Exists(filename))
            {
                return "FILE DOESN'T EXIST!";
            }
            using (var sha512 = SHA512.Create())
            {
                using (var stream = File.OpenRead(filename))
                {
                    var hash = sha512.ComputeHash(stream);
                    return BitConverter.ToString(hash).Replace("-", "").ToUpperInvariant();
                }
            }
        }

        public static bool Empty(DirectoryInfo directory)
        {
            bool success = true;
            foreach (FileInfo file in directory.GetFiles())
            {
                try
                {
                    file.Delete();
                }
                catch { success = false; }
            }
            foreach (DirectoryInfo subDirectory in directory.GetDirectories())
            {
                try
                {
                    subDirectory.Delete(true);
                }
                catch { success = false; }
            }
            return success;
        }

        private void disableProcessStart_CheckedChanged(object sender, EventArgs e)
        {
            CurrentConfig.disableAutomaticGameStart = disableProcessStart.Checked;
            UpdateConfigFile();
        }

        private void checkBoxSplashScreen_CheckedChanged(object sender, EventArgs e)
        {
#if GAME_IS_GREENHELL
            CurrentConfig.skipSplashScreen = checkBoxSplashScreen.Checked;
            UpdateConfigFile();
#endif
        }

        public static ModLoaderVersion ReadModLoaderVersion(string json)
        {
            ModLoaderVersion deserializedModLoaderVersion = new ModLoaderVersion();
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json));
            DataContractJsonSerializer ser = new DataContractJsonSerializer(deserializedModLoaderVersion.GetType());
            deserializedModLoaderVersion = ser.ReadObject(ms) as ModLoaderVersion;
            ms.Close();
            return deserializedModLoaderVersion;
        }

        public static LauncherConfiguration GetLauncherConfiguration()
        {
            LauncherConfiguration deserializedLauncherConfiguration = new LauncherConfiguration();
            MemoryStream ms = new MemoryStream(File.ReadAllBytes(CONFIGURATION_FILE));
            DataContractJsonSerializer ser = new DataContractJsonSerializer(deserializedLauncherConfiguration.GetType());
            deserializedLauncherConfiguration = ser.ReadObject(ms) as LauncherConfiguration;
            ms.Close();
            return deserializedLauncherConfiguration;
        }

        public static void SaveLauncherConfiguration()
        {
            var stream1 = new MemoryStream();
            var ser = new DataContractJsonSerializer(typeof(LauncherConfiguration));
            ser.WriteObject(stream1, CurrentConfig);
            stream1.Position = 0;
            var sr = new StreamReader(stream1);
            File.WriteAllText(CONFIGURATION_FILE, sr.ReadToEnd());

        }
    }
}

public class ModLoaderVersion
{
    public string version = "";
    public string coremod_url = "";
    public string assets_url = "";
    public string coremod_hash = "";
    public string assets_hash = "";
    public string rawChangelog = "";
    public string fullChangelogUrl = "";
}

public class ServiceInfo
{
    public string ip;
    public int port;
    public bool tcpServer;

    public ServiceInfo(string _ip, int _port, bool _tcpServer)
    {
        ip = _ip;
        port = _port;
        tcpServer = _tcpServer;
    }
}

public class LauncherTab
{
    public bool IsOpened;
    public FlowLayoutPanel panel;
    public virtual void Initialize() { }
    public virtual void OnTabOpen() { IsOpened = true; }
    public virtual void OnTabClose() { IsOpened = false; }
}