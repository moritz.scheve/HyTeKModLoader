﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using UnityEngine;

namespace AssemblyLoader
{
    public class Loader
    {
        static GameObject gameObject;
#if GAME_IS_GREENHELL
        public static int assembliesCount = 85;
        public static string folderName = "GreenHellModLoader";
        public static string mainClass = "GHML_Main";
#elif GAME_IS_RAFT
        public static int assembliesCount = 79;
        public static string folderName = "RaftModLoader";
        public static string mainClass = "RML_Main";
#endif

        public async static void Load()
        {
            while (AppDomain.CurrentDomain.GetAssemblies().Length < assembliesCount)
            {
                await Task.Delay(1500);
            }
            await Task.Delay(1500);
            gameObject = new GameObject();
            gameObject.AddComponent<HyTeKInjector>();
            GameObject.DontDestroyOnLoad(gameObject);
        }
    }

    public class HyTeKInjector : MonoBehaviour
    {
        [Serializable]
        public class LauncherConfiguration
        {
            public string gamePath = "";
            public string coreVersion = "?";
            public int agreeWithTOS = 0;
            public bool skipSplashScreen = false;
            public bool disableAutomaticGameStart = false;
        }

        /*[Serializable]
        public class ServerConfiguration
        {
            public string serverPath = "";
            public int tcpServerPort = 8052;
        }*/

        public static string modloaderFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), Loader.folderName);
        public static string modloader_configfile = Path.Combine(modloaderFolder, "launcher_config.json");
        //public static string dedicatedserver_configfile = Path.Combine(modloaderFolder, "server_config.json");
        //public static ServerConfiguration dedicatedserver_config = null;
        public static LauncherConfiguration modloader_config = null;

        void Awake()
        {
            Directory.CreateDirectory(modloaderFolder);
            Directory.CreateDirectory(Path.Combine(modloaderFolder, "logs"));

            /*if (File.Exists(dedicatedserver_configfile))
            {
                dedicatedserver_config = JsonUtility.FromJson<ServerConfiguration>(File.ReadAllText(dedicatedserver_configfile));
                StartDedicatedServer();
            }*/

            if (File.Exists(modloader_configfile))
            {
                modloader_config = JsonUtility.FromJson<LauncherConfiguration>(File.ReadAllText(modloader_configfile));
                StartModLoader();
            }
        }

        /*public static void StartDedicatedServer()
        {
            try
            {
                string logfilePath = Path.Combine(modloaderFolder, "logs\\dedicatedserver.log");
                File.WriteAllText(logfilePath, "");
                string dllFile = Path.Combine(dedicatedserver_config.serverPath, "servercore.dll");
                if (!GameObject.Find("HyTeKDedicatedServer") && !GameObject.Find("HyTeKModLoader"))
                {
                    if (!File.Exists(dllFile))
                    {
                        DedicatedServerLog("Missing file servercore.dll, tried to look at " + dllFile + " without success.");
                        return;
                    }

                    Assembly assembly = Assembly.Load(File.ReadAllBytes(dllFile));
                    DedicatedServerLog("Loaded assembly : " + assembly.ToString());
                    GameObject gameObject = new GameObject("HyTeKDedicatedServer");
                    GameObject.DontDestroyOnLoad(gameObject);
                    gameObject.AddComponent(assembly.GetType("GHDS_Main"));
                    DedicatedServerLog("HyTeKDedicatedServer has been successfully loaded!");
                    File.WriteAllText(Path.Combine(modloaderFolder, "server_config.json"), JsonUtility.ToJson(dedicatedserver_config, true));
                }
            }
            catch (Exception e)
            {
                DedicatedServerLog("/!\\ StartDedicatedServer() : A fatal error occured! /!\\\n" + e.ToString());
            }
        }*/

        public static void StartModLoader()
        {
            try
            {
                string logfilePath = Path.Combine(modloaderFolder, "logs\\modloader.log");
                File.WriteAllText(logfilePath, "");
                string dllFile = Path.Combine(modloaderFolder, "binaries/coremod.dll");
                if (!GameObject.Find("HyTeKModLoader") && !GameObject.Find("HyTeKDedicatedServer"))
                {
                    Assembly assembly = Assembly.Load(File.ReadAllBytes(dllFile));
                    ModLoaderLog("Loaded assembly : " + assembly.ToString());
                    GameObject gameObject = new GameObject("HyTeKModLoader");
                    GameObject.DontDestroyOnLoad(gameObject);
                    gameObject.AddComponent(assembly.GetType(Loader.mainClass));
                    ModLoaderLog("HyTeKModLoader has been successfully loaded!");
                    File.WriteAllText(Path.Combine(modloaderFolder, "launcher_config.json"), JsonUtility.ToJson(modloader_config, true));
                }
            }
            catch (Exception e)
            {
                ModLoaderLog("/!\\ StartModLoader() : A fatal error occured! /!\\\n" + e.ToString());
            }
        }

        public static void ModLoaderLog(string text)
        {
            string logfilePath = Path.Combine(modloaderFolder, "logs\\modloader.log");
            File.AppendAllText(logfilePath, text + "\n");
        }

        /*public static void DedicatedServerLog(string text)
        {
            string logfilePath = Path.Combine(modloaderFolder, "logs\\dedicatedserver.log");
            File.AppendAllText(logfilePath, text + "\n");
        }*/
    }
}