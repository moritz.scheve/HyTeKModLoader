﻿using Harmony;
using System;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace RaftModLoader
{
    public class CustomLoadingScreen : MonoBehaviour
    {
        public static CustomLoadingScreen instance;
        public static LoadSceneManager loadscenemanager;
        public static FadePanel fadePanel;
        public static GameObject loadingscreen_background;
        public static GameObject loadingscreen_banner;
        public static Image progressBar;
        public static TextMeshProUGUI progressText;
        public static bool fromMenuToGame;
        public static float loadingProgress;
        public static float lastProgress;
        public static float lerp = 0f;
        public static Texture defaultBackground;
        public static Texture originalDefaultBackground;
        public static bool isWorldDownloaded = false;
        public static GameObject patronPrefab;

        void Awake()
        {
            instance = this;
            GameObject prefab1 = RML_Main.rmlAssets.LoadAsset<GameObject>("LoadingScreenBackground");
            GameObject prefab2 = RML_Main.rmlAssets.LoadAsset<GameObject>("LoadingScreenBanner");
            patronPrefab = RML_Main.rmlAssets.LoadAsset<GameObject>("PatronEntry");
            loadscenemanager = FindObjectOfType<LoadSceneManager>();
            fadePanel = (Traverse.Create(loadscenemanager).Field("fadePanel").GetValue() as FadePanel);
            loadingscreen_background = Instantiate(prefab1, fadePanel.transform);
            defaultBackground = loadingscreen_background.GetComponent<RawImage>().texture;
            originalDefaultBackground = defaultBackground;
            loadingscreen_background.transform.SetAsLastSibling();
            loadingscreen_banner = Instantiate(prefab2, fadePanel.transform);
            loadingscreen_banner.transform.SetAsLastSibling();
            progressBar = loadingscreen_banner.transform.Find("LoadingBarMask").Find("loadingbar").Find("value").GetComponent<Image>();
            progressText = loadingscreen_banner.transform.Find("Name").GetComponent<TextMeshProUGUI>();
        }

        public static async void InitLoadingScreen(JSONObject patronsdata)
        {
            while (loadingscreen_banner == null)
            {
                await Task.Delay(1);
            }
            try
            {
                foreach (Transform t in loadingscreen_banner.transform.Find("PatronsBar").Find("PatronsRow"))
                {
                    Destroy(t.gameObject);
                }
                for (int i = 0; i < patronsdata.Count; i++)
                {
                    string name = patronsdata[i]["name"].str;
                    string amount = patronsdata[i]["amount"].str;
                    string imageUrl = patronsdata[i]["imageUrl"].str;
                    bool isNitroBooster = patronsdata[i]["isNitroBooster"].b;
                    bool avatarAnimated = patronsdata[i]["avatarAnimated"].b;
                    bool usernameAnimated = patronsdata[i]["usernameAnimated"].b;
                    GameObject item = Instantiate(patronPrefab, Vector3.zero, Quaternion.identity, loadingscreen_banner.transform.Find("PatronsBar").Find("PatronsRow"));
                    item.transform.Find("username").GetComponent<TextMeshProUGUI>().text = name;
                    if (amount != "")
                    {
                        item.transform.Find("username").GetComponent<TextMeshProUGUI>().text += " : <color=#4287f5>" + amount + "</color>";
                    }
                    item.transform.Find("IsNitroUser").gameObject.SetActive(isNitroBooster);
                    item.transform.Find("AvatarMask").GetComponent<Animator>().enabled = avatarAnimated;
                    item.transform.Find("username").GetComponent<Animator>().enabled = usernameAnimated;

                    if (imageUrl != "" && imageUrl.Length > 5)
                    {
                        RawImage image = item.transform.Find("AvatarMask").Find("avatar").GetComponent<RawImage>();
                        Utils.DownloadCachedTexture(imageUrl).ContinueWith((t) =>
                        {
                            image.texture = t.Result;
                        });
                    }
                }
            }
            catch (Exception e)
            {
                RConsole.LogError(e.ToString());
            }
        }

        void FixedUpdate()
        {
            if (fadePanel.gameObject.activeSelf)
            {
                lastProgress = Mathf.Lerp(lastProgress, loadingProgress, Time.fixedDeltaTime * 5f);
                progressBar.fillAmount = lastProgress / 100f;
            }
        }
    }

    [HarmonyPatch(typeof(FadePanel))]
    [HarmonyPatch("FadeToAlpha")]
    public static class Patch_LoadingFadeSpeed
    {
        public static void Prefix(float targetAlpha, ref float speed)
        {
            speed = 4f;
        }
    }

    [HarmonyPatch(typeof(Helper))]
    [HarmonyPatch("LogBuild")]
    public static class Patch_LoadingStatus3
    {
        private static async void Prefix(string text)
        {
            if (CustomLoadingScreen.fromMenuToGame && CustomLoadingScreen.fadePanel.gameObject.activeSelf)
            {
                string sent = text.ToLower();
                if (sent == "waiting for bundle...")
                {
                    CustomLoadingScreen.isWorldDownloaded = Semih_Network.IsHost;
                    RChat.instance.ClearChat();
                    CustomLoadingScreen.progressText.text = "Loading assets...";
                    CustomLoadingScreen.loadingProgress = 16.66f;
                }
                else if (sent == "bundle loaded ")
                {
                    CustomLoadingScreen.isWorldDownloaded = Semih_Network.IsHost;
                    CustomLoadingScreen.progressText.text = "Loading prefabs...";
                    CustomLoadingScreen.loadingProgress += 16.66f;
                }
                else if (sent == "loading targetscene mainscene is done!")
                {
                    CustomLoadingScreen.isWorldDownloaded = Semih_Network.IsHost;
                    CustomLoadingScreen.progressText.text = "Switching scene...";
                    CustomLoadingScreen.loadingProgress += 16.66f;
                }
                else if (sent == "starting to load landmarks")
                {
                    CustomLoadingScreen.isWorldDownloaded = Semih_Network.IsHost;
                    CustomLoadingScreen.progressText.text = "Loading scenes...";
                    CustomLoadingScreen.loadingProgress += 16.66f;
                }
                else if (sent == "all scenes loaded")
                {
                    CustomLoadingScreen.isWorldDownloaded = Semih_Network.IsHost;
                    CustomLoadingScreen.progressText.text = "Loaded!";
                    CustomLoadingScreen.loadingProgress = 100f;
                    await Task.Delay(5000);
                    CustomLoadingScreen.loadingscreen_background.GetComponent<RawImage>().texture = CustomLoadingScreen.defaultBackground;
                }
            }
        }
    }

    [HarmonyPatch(typeof(LoadSceneManager))]
    [HarmonyPatch("LoadScene")]
    public static class Patch_LoadingStatus4
    {
        private static void Prefix(string sceneName, bool loadAdditively)
        {
            CustomLoadingScreen.loadingProgress = 0f;
            CustomLoadingScreen.lastProgress = 0f;
            CustomLoadingScreen.progressBar.fillAmount = 0;
            CustomLoadingScreen.fromMenuToGame = (sceneName == Semih_Network.GameSceneName);
            if (!CustomLoadingScreen.fromMenuToGame)
            {
                CustomLoadingScreen.loadingscreen_background.GetComponent<RawImage>().texture = CustomLoadingScreen.defaultBackground;
                CustomLoadingScreen.progressText.text = "Exiting world...";
                CustomLoadingScreen.loadingProgress = 100f;
            }
            else
            {
                CustomLoadingScreen.progressText.text = "Entering world...";
            }
        }
    }
}