﻿using Harmony;
using Microsoft.Win32;
using ShellLink;
using SocketIO;
using Steamworks;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;
namespace RaftModLoader
{
    public class MainMenu : MonoBehaviour
    {
        public static MainMenu instance;
        public static bool IsOpen = true;
        public string CurrentPage;
        public static GameObject pages;
        public static Dictionary<string, MenuPage> menuPages = new Dictionary<string, MenuPage>();
        public static GameObject LeftPagesButtons;
        public static GameObject VersionText;

        GameObject introductionCanvas;
        void Awake()
        {
            if (!PlayerPrefs.HasKey("rmlSettings_ShownIntro") || PlayerPrefs.GetInt("rmlSettings_ShownIntro") != 1)
            {
                introductionCanvas = Instantiate(RML_Main.rmlAssets.LoadAsset<GameObject>("Introduction"));
                introductionCanvas.transform.Find("Background").Find("Text").GetComponent<TextMeshProUGUI>().text = introductionCanvas.transform.Find("Background").Find("Text").GetComponent<TextMeshProUGUI>().text.Replace("TeKGameR", SteamFriends.GetPersonaName());
                introductionCanvas.transform.Find("Buttons").Find("discord").GetComponent<Button>().onClick.AddListener(() =>
                {
                    Application.OpenURL("https://discordapp.com/invite/Uw8tkKE");
                });
                introductionCanvas.transform.Find("Buttons").Find("patreon").GetComponent<Button>().onClick.AddListener(() =>
                {
                    Application.OpenURL("https://www.patreon.com/hytekgames");
                });
                introductionCanvas.transform.Find("Buttons").Find("continue").GetComponent<Button>().onClick.AddListener(() =>
                {
                    PlayerPrefs.SetInt("rmlSettings_ShownIntro", 1);
                    introductionCanvas.GetComponent<Animation>().Play();
                    Destroy(introductionCanvas, 5f);
                });
            }
        }

        void Start()
        {
            instance = this;
            ComponentManager<RSocket>.Value.onlineUsers = GameObject.Find("RMLOnlineUsersCounter").GetComponent<TextMeshProUGUI>();
            pages = GameObject.Find("RMLMainMenu_PageZone");
            foreach (Transform p in pages.transform)
            {
                p.gameObject.SetActive(true);
            }

            LeftPagesButtons = GameObject.Find("RMLMainMenu_Slots");
            VersionText = Instantiate(RML_Main.rmlAssets.LoadAsset<GameObject>("RMLVersionText"), Vector3.zero, Quaternion.identity);
            VersionText.GetComponentInChildren<TextMeshProUGUI>().text = @"RaftModLoader <color=#36d1a8>v" + AssemblyLoader.HyTeKInjector.modloader_config.coreVersion + @"</color>
Press <color=#36d1a8>F9</color> to open the main menu
Press <color=#36d1a8>F10</color> to open the console";
            DontDestroyOnLoad(VersionText);

            GameObject.Find("RMLMainMenuLButton_Home").GetComponent<Button>().onClick.AddListener(() => { ChangeMenu("Home"); });
            GameObject.Find("RMLMainMenuLButton_ModManager").GetComponent<Button>().onClick.AddListener(() => { ChangeMenu("ModManager"); });
            GameObject.Find("RMLMainMenuLButton_Servers").GetComponent<Button>().onClick.AddListener(() => { ChangeMenu("Servers"); });
            GameObject.Find("RMLMainMenuLButton_Settings").GetComponent<Button>().onClick.AddListener(() => { ChangeMenu("Settings"); });
            GameObject.Find("RMLMainMenuLButton_Credits").GetComponent<Button>().onClick.AddListener(() => { ChangeMenu("Credits"); });
            GameObject.Find("RMLMainMenuCloseBtn").GetComponent<Button>().onClick.AddListener(CloseMenu);

            menuPages.Add("Home", pages.transform.Find("Home").gameObject.AddComponent<HomePage>());
            menuPages.Add("ModManager", pages.transform.Find("ModManager").gameObject.AddComponent<ModManagerPage>());
            menuPages.Add("Servers", pages.transform.Find("Servers").gameObject.AddComponent<ServersPage>());
            menuPages.Add("Settings", pages.transform.Find("Settings").gameObject.AddComponent<SettingsPage>());
            menuPages.Add("Credits", pages.transform.Find("Credits").gameObject.AddComponent<CreditsPage>());
        }

        Dropdown load_authSettingDropdown;
        Dropdown new_authSettingDropdown;
        void Update()
        {
            if (Camera.current != null && Camera.current.GetComponent<AudioListener>() == null)
            {
                Camera.current.gameObject.AddComponent<AudioListener>();
            }
            if (SceneManager.GetActiveScene().name != Semih_Network.LobbySceneName) { return; }
            if (!load_authSettingDropdown)
            {
                load_authSettingDropdown = Traverse.Create(FindObjectOfType<LoadGameBox>()).Field("authSettingDropdown").GetValue() as Dropdown;
            }
            else
            {
                if (load_authSettingDropdown.options.Count < 3)
                {
                    load_authSettingDropdown.AddOptions(new List<string> { "Everyone Can Join" });
                }
            }

            if (!new_authSettingDropdown)
            {
                new_authSettingDropdown = Traverse.Create(FindObjectOfType<NewGameBox>()).Field("authSettingDropdown").GetValue() as Dropdown;
            }
            else
            {
                if (new_authSettingDropdown.options.Count < 3)
                {
                    new_authSettingDropdown.AddOptions(new List<string> { "Everyone Can Join" });
                }
            }
        }

        public static void CloseMenu()
        {
            ModManagerPage.RefreshModsStates();
            IsOpen = false;
            RAPI.TogglePriorityCursor(false);
        }

        public void OpenMenu()
        {
            ChangeMenu(CurrentPage);
            IsOpen = true;
            RAPI.TogglePriorityCursor(true);
            ModManagerPage.RefreshModsStates();
        }

        void LateUpdate()
        {
            if (Semih_Network.InLobbyScene)
            {
                VersionText.GetComponent<Canvas>().enabled = true;
            }
            else
            {
                VersionText.GetComponent<Canvas>().enabled = false;
            }

            if (!SettingsPage.EnableMenuInGame && !Semih_Network.InLobbyScene)
            {
                GetComponent<Canvas>().enabled = false;
                IsOpen = false;
                return;
            }

            if (IsOpen)
            {
                if (!GetComponent<Canvas>().enabled)
                {
                    GetComponent<Canvas>().enabled = true;
                    GetComponent<Animation>().Play("MenuOpen");
                }
            }
            else
            {
                if (GetComponent<Canvas>().enabled)
                {
                    StartCoroutine(CloseMenuLate());
                }
            }

            if (Input.GetKeyDown(RML_Main.MenuKey))
            {
                if ((Traverse.Create(FindObjectOfType<LoadSceneManager>()).Field("fadePanel").GetValue() as FadePanel).gameObject.activeSelf)
                {
                    return;
                }
                IsOpen = !IsOpen;
                if (IsOpen)
                {
                    ChangeMenu(CurrentPage);
                }
                if (!Semih_Network.InLobbyScene)
                {
                    RAPI.TogglePriorityCursor(IsOpen);
                }

            }
            else if (Input.GetKeyDown(KeyCode.Escape) && IsOpen)
            {
                if ((Traverse.Create(FindObjectOfType<LoadSceneManager>()).Field("fadePanel").GetValue() as FadePanel).gameObject.activeSelf)
                {
                    return;
                }
                IsOpen = false;
                if (!Semih_Network.InLobbyScene)
                {
                    RAPI.TogglePriorityCursor(false);
                }
            }
        }

        public IEnumerator CloseMenuLate()
        {
            GetComponent<Animation>().Play("MenuClose");
            yield return new WaitForSeconds(0.10f);
            GetComponent<Canvas>().enabled = false;
        }

        public static void ChangeMenu(string menuname)
        {
            foreach (Transform p in pages.transform)
            {
                if (p.gameObject.activeSelf)
                {
                    if (menuPages.ContainsKey(p.name))
                    {
                        menuPages[p.name].OnPageOpen();
                    }
                }
                p.gameObject.SetActive(false);
            }
            if (menuPages.ContainsKey(menuname))
            {
                menuPages[menuname].OnPageOpen();
            }
            instance.StartCoroutine(instance.ChangePage(menuname));
        }

        public IEnumerator ChangePage(string menuname)
        {
            yield return new WaitForEndOfFrame();
            foreach (Transform b in LeftPagesButtons.transform)
            {
                b.GetComponent<Button>().interactable = true;
            }
            pages.GetComponent<CanvasGroup>().alpha = 0;
            GameObject page = pages.transform.Find(menuname).gameObject;
            Button button = LeftPagesButtons.transform.Find("RMLMainMenuLButton_" + menuname).gameObject.GetComponent<Button>();

            if (page != null)
            {
                page.SetActive(true);
                pages.GetComponent<Animation>().Play();
                button.interactable = false;
                CurrentPage = menuname;
            }
        }
    }

    public class TooltipHandler : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public GameObject tooltip;

        public void Start()
        {
            tooltip.SetActive(false);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            tooltip.SetActive(true);
        }
        public void OnPointerExit(PointerEventData eventData)
        {
            if (gameObject.GetComponent<Button>() != null)
            {
                gameObject.GetComponent<Button>().OnDeselect(null);
            }
            tooltip.SetActive(false);
        }
    }
}