﻿using UnityEngine;

namespace RaftModLoader
{
    public class MenuPage : MonoBehaviour
    {
        public virtual void OnPageOpen() { }

        public virtual void OnPageClose() { }
    }
}