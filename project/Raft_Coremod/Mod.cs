﻿using RaftModLoader;
using UnityEngine;

public class Mod : MonoBehaviour
{
    public virtual void UnloadMod()
    {
        foreach (ModData md in ModManagerPage.modList)
        {
            if (md.modinfo.assembly != null)
            {
                if (md.modinfo.assembly == this.GetType().Assembly)
                {
                    md.modinfo.modHandler.UnloadMod(md);
                }
            }
        }
    }

    public virtual void WorldEvent_WorldLoaded()
    {
    }

    public virtual void WorldEvent_WorldSaved()
    {
    }

    public virtual void LocalPlayerEvent_Hurt(float damage, Vector3 hitPoint, Vector3 hitNormal, EntityType damageInflictorEntityType)
    {
    }

    public virtual void LocalPlayerEvent_Death(Vector3 deathPosition)
    {
    }

    public virtual void LocalPlayerEvent_Respawn()
    {
    }

    public virtual void LocalPlayerEvent_ItemCrafted(Item_Base item)
    {
    }

    public virtual void LocalPlayerEvent_PickupItem(PickupItem item)
    {
    }

    public virtual void LocalPlayerEvent_DropItem(ItemInstance item, Vector3 position, Vector3 direction, bool parentedToRaft)
    {
    }
}