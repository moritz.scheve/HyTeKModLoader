﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RNotify : MonoBehaviour
{
    GameObject content;
    GameObject _errorNotificationPrefab;
    GameObject _spinningNotificationPrefab;
    GameObject _scalingNotificationPrefab;
    GameObject _normalNotificationPrefab;

    public static Sprite ErrorSprite;
    public static Sprite LoadingSprite;
    public static Sprite CheckSprite;

    public static List<RNotification> notifications = new List<RNotification>();

    public static RNotification errornotification;

    public enum NotificationType
    {
        error,
        spinning,
        scaling,
        normal
    }

    void Start()
    {
        content = transform.Find("Content").gameObject;
        _errorNotificationPrefab = RML_Main.rmlAssets.LoadAsset<GameObject>("ErrorNotification");
        _spinningNotificationPrefab = RML_Main.rmlAssets.LoadAsset<GameObject>("SpinningNotification");
        _scalingNotificationPrefab = RML_Main.rmlAssets.LoadAsset<GameObject>("ScalingNotification");
        _normalNotificationPrefab = RML_Main.rmlAssets.LoadAsset<GameObject>("NormalNotification");

        ErrorSprite = RML_Main.rmlAssets.LoadAsset<Sprite>("IconError");
        LoadingSprite = RML_Main.rmlAssets.LoadAsset<Sprite>("IconRefresh");
        CheckSprite = RML_Main.rmlAssets.LoadAsset<Sprite>("Check");

        errornotification = AddNotification(NotificationType.error, "");
    }

    public RNotification AddNotification(NotificationType type, int closeDelay = 0, Sprite icon = null)
    {
        return AddNotification(type, "", closeDelay, icon);
    }

    public RNotification AddNotification(NotificationType type, string text = "", int closeDelay = 0, Sprite icon = null)
    {
        GameObject prefab = null;
        if (type == NotificationType.error)
        {
            if (errornotification != null) { return errornotification; }
            prefab = _errorNotificationPrefab;
        }
        else if (type == NotificationType.normal)
        {
            prefab = _normalNotificationPrefab;
        }
        else if (type == NotificationType.scaling)
        {
            prefab = _scalingNotificationPrefab;
        }
        else if (type == NotificationType.spinning)
        {
            prefab = _spinningNotificationPrefab;
        }

        GameObject nobj = Instantiate(prefab, content.transform);
        RNotification n = nobj.AddComponent<RNotification>();

        n.closeDelay = closeDelay;
        if (closeDelay > 0)
        {
            n.StartCoroutine(n.AutoClose());
        }

        n.text = n.transform.Find("Message").GetComponent<TextMeshProUGUI>();
        n.icon = n.transform.Find("Icon").GetComponent<Image>();
        n.Background = n.GetComponent<RawImage>();

        if (text != "")
            n.text.text = text;
        if (icon != null)
            n.icon.sprite = icon;

        if (type == NotificationType.error)
        {
            n.StartCoroutine(n.ErrorChecker());
            n.SetText("").Hide();
        }
        else
        {
            notifications.Add(n);
        }
        return n;
    }

    public static void ClearNotifications()
    {
        foreach (RNotification rn in notifications)
        {
            if (rn != null)
                rn.Close();
        }
        notifications.Clear();
    }
}

public class RNotification : MonoBehaviour
{
    public int closeDelay = 0;

    public bool isError;
    public int errorAmount;
    public DateTime lastError = DateTime.Now;
    public TextMeshProUGUI text;
    public Image icon;
    public RawImage Background;
    
    public async void AddNewError()
    {
        lastError = DateTime.Now;
        errorAmount++;
        SetText("An error occured! (x"+errorAmount+")");
        Show();
        await Task.Delay(0);
    }

    public IEnumerator ErrorChecker()
    {
        yield return new WaitForSeconds(2);
        if (lastError.AddSeconds(2) < DateTime.Now)
        {
            if (errorAmount > 0)
            {
                errorAmount = 0;
                Hide();
            }
        }
        StartCoroutine(ErrorChecker());
    }

    public void Hide()
    {
        GetComponent<Animation>().Play("NotifyClose");
    }

    public void Show()
    {
        if (errorAmount == 1)
        {
            GetComponent<Animation>().Play("NotifyOpen");
        }
    }

    public IEnumerator AutoClose()
    {
        if(RNotify.errornotification == this) { yield break; }
        yield return new WaitForSeconds(closeDelay);
        GetComponent<Animation>().Play("NotifyClose");
        yield return new WaitForSeconds(0.20f);
        Destroy(gameObject);
        RNotify.notifications.Remove(this);
        yield break;
    }

    public void Close()
    {
        if (RNotify.errornotification == this) { return; }
        closeDelay = 0;
        StartCoroutine(AutoClose());
    }

    public RNotification SetColor(Color color)
    {
        Background.color = color;
        return this;
    }

    public RNotification SetText(string val)
    {
        text.text = val;
        return this;
    }

    public RNotification SetIcon(Sprite val)
    {
        icon.sprite = val;
        return this;
    }

    public RNotification SetNormal()
    {
        if (icon.GetComponent<Animation>())
        {
            Destroy(icon.GetComponent<Animation>());
            icon.GetComponent<RectTransform>().rotation = Quaternion.identity;
            icon.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        }
        return this;
    }

    public RNotification SetFontSize(int size = 10)
    {
        if (size != 10)
            text.fontSize = size;
        return this;
    }

    public RNotification SetTextColor(Color color = default)
    {
        if (color != default)
            text.color = color;
        return this;
    }

    public RNotification SetCloseDelay(int delay)
    {
        if (RNotify.errornotification == this) { return this; }
        StopAllCoroutines();
        if (delay > 0)
        {
            closeDelay = delay;
            StartCoroutine(AutoClose());
        }
        return this;
    }
}
