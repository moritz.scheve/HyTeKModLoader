﻿using Harmony;
using RaftModLoader;
using SocketIO;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class RML_Main : MonoBehaviour
{
    public static AssetBundle rmlAssets;
    private bool debug = false;

    public static List<string> logs = new List<string>();

    public static string path_dataFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "RaftModLoader");
    public static string path_binariesFolder = Path.Combine(path_dataFolder, "binaries");
    public static string path_logsFolder = Path.Combine(path_dataFolder, "logs");
    public static string path_cacheFolder = Path.Combine(path_dataFolder, "cache");
    public static string path_cacheFolder_mods = Path.Combine(path_cacheFolder, "mods");
    public static string path_cacheFolder_textures = Path.Combine(path_cacheFolder, "textures");
    public static string path_cacheFolder_temp = Path.Combine(path_cacheFolder, "temp");
    public static string path_configFile = Path.Combine(path_dataFolder, "launcher_config.json");
    public static string path_modsFolder = Path.Combine(Application.dataPath, "..\\mods");
    public static string gameLogFile = Path.Combine(path_logsFolder, "coremod.log");
    public static Texture2D missingTexture;
    public static KeyCode MenuKey = KeyCode.F9;
    public static KeyCode ConsoleKey = KeyCode.F10;

    void OnEnable()
    {
        Application.logMessageReceived += HandleUnityLog;
    }
    void HandleUnityLog(string logString, string stackTrace, LogType type)
    {
        if (debug)
        {
            logs.Add(logString + "\n" + stackTrace);
        }
    }

    void OnGUI()
    {
        if (debug)
        {
            GUIStyle style = new GUIStyle();
            style.normal.textColor = Color.red;
            string text = "";
            foreach (string s in logs)
            {
                text += "\n" + s;
            }
            GUI.Label(new Rect(0, 0, Screen.width, Screen.height), text, style);
        }
    }

    void OnApplicationQuit()
    {
        Process.GetCurrentProcess().Kill();
    }

    private void Awake()
    {
        Application.backgroundLoadingPriority = UnityEngine.ThreadPriority.High;
        Directory.CreateDirectory(path_dataFolder);
        Directory.CreateDirectory(path_cacheFolder);
        Directory.CreateDirectory(path_cacheFolder_mods);
        Directory.CreateDirectory(path_cacheFolder_textures);
        Directory.CreateDirectory(path_cacheFolder_temp);
        Directory.SetCurrentDirectory(Path.Combine(Application.dataPath, "..\\"));
        StartCoroutine(LoadRML());
    }

    public void Update()
    {
        TemporaryPacketSplitingReadingClass.TemporaryPacketSplitingReading();
    }

    [Serializable]
    private class ServerVersionInfo
    {
        public string clientVersion = "";
    }

    IEnumerator LoadRML()
    {
        ComponentManager<RSocket>.Value = gameObject.AddComponent<RSocket>();
        foreach (string s in Directory.GetFiles(path_cacheFolder_temp))
        {
            if (s.ToLower().EndsWith(".dll"))
            {
                File.Delete(s);
            }
        }
        ComponentManager<RML_Main>.Value = this;

        AssetBundleCreateRequest bundleLoadRequest = AssetBundle.LoadFromFileAsync(Path.Combine(path_binariesFolder, "rml.assets"));
        yield return bundleLoadRequest;

        rmlAssets = bundleLoadRequest.assetBundle;
        if (rmlAssets == null) { yield break; }

        missingTexture = rmlAssets.LoadAsset<Sprite>("missing").texture;

        var ConsoleAssetLoadRequest = rmlAssets.LoadAssetAsync<GameObject>("RMLConsoleCanvas");
        yield return ConsoleAssetLoadRequest;

        GameObject tempconsoleobj = ConsoleAssetLoadRequest.asset as GameObject;
        GameObject consoleobj = Instantiate(tempconsoleobj, gameObject.transform);
        ComponentManager<RConsole>.Value = consoleobj.AddComponent<RConsole>();
        consoleobj.name = "RMLConsoleCanvas";

        try
        {
            HarmonyInstance harmony = HarmonyInstance.Create("hytekgames.raftmodloader");
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
        catch (Exception e)
        {
            Debug.LogError(e.ToString());
        }

        ComponentManager<RawSharp>.Value = gameObject.AddComponent<RawSharp>();
        ComponentManager<SocketIOComponent>.Value = gameObject.AddComponent<SocketIOComponent>();
        ComponentManager<UnityMainThreadDispatcher>.Value = gameObject.AddComponent<UnityMainThreadDispatcher>();

        gameObject.AddComponent<CustomLoadingScreen>();

        var MenuAssetLoadRequest = rmlAssets.LoadAssetAsync<GameObject>("RMLMainMenu");
        yield return MenuAssetLoadRequest;

        GameObject tempmenuobj = MenuAssetLoadRequest.asset as GameObject;
        GameObject mainmenu = Instantiate(tempmenuobj, gameObject.transform);
        ComponentManager<MainMenu>.Value = mainmenu.AddComponent<MainMenu>();

        var RNotifyLoadRequest = rmlAssets.LoadAssetAsync<GameObject>("RMLNotificationSystem");
        yield return RNotifyLoadRequest;

        GameObject rnotifytempobj = RNotifyLoadRequest.asset as GameObject;
        GameObject rnotify = Instantiate(rnotifytempobj, gameObject.transform);
        ComponentManager<RNotify>.Value = rnotify.AddComponent<RNotify>();

        gameObject.AddComponent<RChat>();
    }
}