﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace RaftModLoader
{
    public class DllModHandler : BaseModHandler
    {
        public override async Task<ModData> GetModData(FileInfo file, Dictionary<string, Assembly> assemblyList = null)
        {
            ModData moddata = new ModData();
            moddata.modinfo.modHandler = this;
            moddata.modinfo.modFile = file;
            moddata.modinfo.fileHash = Utils.GetFileSHA512Hash(file.FullName);
            if (!assemblyList.ContainsKey(moddata.modinfo.modFile.Name))
            {
                if (moddata.modinfo.assembly == null)
                    moddata.modinfo.assembly = await GetModAssembly(moddata);
            }
            else
            {
                moddata.modinfo.assembly = assemblyList[moddata.modinfo.modFile.Name];
            }
            if (moddata.modinfo.assembly == null) { return null; }
            IEnumerable<Type> types = moddata.modinfo.assembly.GetTypes().Where(t => t.IsSubclassOf(typeof(Mod)));
            if (types.Count() != 1)
            {
                Debug.LogError("[ModManager] " + file.Name + "> The mod codebase doesn't specify a mod class or specifies more than one!");
                return null;
            }
            else
            {
                moddata.jsonmodinfo = GetModInfoFromOldMod(types.FirstOrDefault());
                if (moddata.jsonmodinfo == null) { return null; }
                return moddata;
            }
        }

        public override void LoadMod(ModData md)
        {
            if (md == null) { return; }
            if (md.modinfo.modState == ModInfo.ModStateEnum.running) { return; }

            if (!ModManagerPage.ModsGameObjectParent.transform.Find(md.modinfo.modFile.Name.ToLower()))
            {
                try
                {
                    if (md.modinfo.assembly == null) { md.modinfo.modState = ModInfo.ModStateEnum.errored; }
                    IEnumerable<Type> types = md.modinfo.assembly.GetTypes().Where(t => t.IsSubclassOf(typeof(Mod)));
                    if (types.Count() != 1)
                    {
                        RConsole.LogError("[ModManager] " + md.jsonmodinfo.ModName + "> The mod codebase doesn't specify a mod class or specifies more than one!");
                        md.modinfo.modState = ModInfo.ModStateEnum.errored;
                    }
                    else
                    {
                        GameObject ModObj = new GameObject();
                        ModObj.name = md.modinfo.modFile.Name.ToLower();
                        ModObj.transform.parent = ModManagerPage.ModsGameObjectParent.transform;
                        ModObj.AddComponent(types.FirstOrDefault());
                        md.modinfo.modState = ModInfo.ModStateEnum.running;
                    }
                }
                catch (Exception e)
                {
                    md.modinfo.modState = ModInfo.ModStateEnum.errored;
                    RConsole.LogError("[ModManager] " + md.jsonmodinfo.ModName + " > A fatal error occured while loading the mod!\nStacktrace : " + e.ToString());
                }
            }
            ModManagerPage.RefreshModState(md);
        }

        public override void UnloadMod(ModData md)
        {
            if (md.jsonmodinfo.ModIsPermanent) { return; }
            try
            {
                Transform mod = ModManagerPage.ModsGameObjectParent.transform.Find(md.modinfo.modFile.Name.ToLower());
                if (mod == null || md.modinfo.assembly == null)
                {
                    md.modinfo.modState = ModInfo.ModStateEnum.idle;
                    ModManagerPage.RefreshModState(md);
                    return;
                }
                IEnumerable<Type> types = md.modinfo.assembly.GetTypes().Where(t => t.IsSubclassOf(typeof(Mod)));
                if (types.First() != null)
                {
                    if (!mod.GetComponent(types.First()))
                    {
                        Destroy(mod.gameObject);
                        md.modinfo.modState = ModInfo.ModStateEnum.idle;
                        ModManagerPage.RefreshModState(md);
                        return;
                    }

                    List<Command> modCommands = RConsole.commands.Where(c => c.mod != null && c.mod.Equals(types.First())).ToList();
                    modCommands.ForEach(c => RConsole.unregisterCommand(c.command));
                }
                else
                {
                    // I guess
                    md.modinfo.modState = ModInfo.ModStateEnum.idle;
                    return;
                }

                var methodInfo = types.First().GetMethod("OnModUnload");
                if (methodInfo != null)
                {
                    try
                    {
                        methodInfo.Invoke(mod.GetComponent(types.First()), null);
                    }
                    catch
                    {
                        if (mod != null)
                        {
                            Destroy(mod.gameObject);
                        }
                        RConsole.Log("[ModManager] " + md.jsonmodinfo.ModName + " > Mod Successfully Unloaded!");
                    }
                }
                else
                {
                    RConsole.LogWarning("[ModManager] " + md.jsonmodinfo.ModName + " > The mod doesn't support unload! (Trying to manually unload it, This could lead into some issues)");
                    Destroy(mod.gameObject);
                    RConsole.Log("[ModManager] " + md.jsonmodinfo.ModName + " > Mod Successfully Unloaded!");
                }

                md.modinfo.modState = ModInfo.ModStateEnum.idle;
                ModManagerPage.RefreshModState(md);
            }
            catch (Exception e)
            {
                RConsole.LogError("[ModManager] " + md.jsonmodinfo.ModName + " > An error occured while unloading the mod!\nStacktrace : " + e.ToString());
            }
        }

        public override async Task<Assembly> GetModAssembly(ModData moddata)
        {
            if (moddata.modinfo.assembly != null) { return moddata.modinfo.assembly; }

            try
            {
                Assembly assembly = Assembly.Load(File.ReadAllBytes(moddata.modinfo.modFile.FullName));
                return assembly;
            }
            catch (Exception e)
            {
                Debug.LogError("[ModManager] " + moddata.modinfo.modFile.Name + " > A fatal error occured while loading the mod!\nStacktrace : " + e.ToString());
                return null;
            }
        }
        public JsonModInfo GetModInfoFromOldMod(Type t)
        {
            JsonModInfo jsonmodinfo = new JsonModInfo();
            jsonmodinfo.ModName = RAPI.GetModTitle(t);
            jsonmodinfo.ModPackage = "old.deprecated." + DateTime.Now.Ticks;
            jsonmodinfo.ModDescription = RAPI.GetModDescription(t);
            jsonmodinfo.ModAuthor = RAPI.GetModAuthor(t);
            jsonmodinfo.ModVersion = RAPI.GetModVersion(t);
            jsonmodinfo.RaftVersion = RAPI.GetModRaftVersion(t);
            jsonmodinfo.ModIconUrl = RAPI.GetModIconUrl(t);
            jsonmodinfo.ModBannerUrl = RAPI.GetModWallpaperUrl(t);
            jsonmodinfo.ModVersionCheckUrl = RAPI.GetModVersionCheckUrl(t);
            jsonmodinfo.ModIsPermanent = RAPI.GetModIsPermanent(t);
            return jsonmodinfo;
        }
    }
}