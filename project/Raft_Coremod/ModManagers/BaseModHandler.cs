﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace RaftModLoader
{
    public class BaseModHandler : MonoBehaviour
    {
        public virtual async Task<ModData> GetModData(FileInfo file, Dictionary<string, Assembly> assemblyList = null) { throw new NotImplementedException(); }

        public virtual async Task<Assembly> GetModAssembly(ModData moddata) { throw new NotImplementedException(); }

        public virtual async void LoadMod(ModData moddata) { throw new NotImplementedException(); }

        public virtual async void UnloadMod(ModData moddata) { throw new NotImplementedException(); }

        /*
        public void UnloadMod(ModData md)
        {
            if (md.jsonmodinfo.ModIsPermanent) { return; }
            try
            {
                Transform mod = ModsGameObjectParent.transform.Find(md.modinfo.modFile.Name.ToLower());
                if (!mod || md.modinfo.assembly == null)
                {
                    md.modinfo.modState = ModInfo.ModStateEnum.idle;
                    RefreshModsStates();
                    return;
                }
                IEnumerable<Type> types = md.modinfo.assembly.GetTypes().Where(t => t.IsSubclassOf(typeof(Mod)));
                if (types.First() != null)
                {
                    if (!mod.GetComponent(types.First()))
                    {
                        Destroy(mod.gameObject);
                        md.modinfo.modState = ModInfo.ModStateEnum.idle;
                        RefreshModsStates();
                        return;
                    }

                    List<string> a = new List<string>();
                    foreach (Command c in RConsole.commands)
                    {
                        if (c.mod != null && c.mod.Equals(types.First()))
                        {
                            a.Add(c.command);
                        }
                    }

                    foreach (string b in a)
                    {
                        RConsole.unregisterCommand(b);
                    }
                }

                var methodInfo = types.First().GetMethod("OnModUnload");
                if (methodInfo != null)
                {
                    try
                    {
                        methodInfo.Invoke(mod.GetComponent(types.First()), null);
                    }
                    catch (Exception e)
                    {
                        RConsole.LogError(e.ToString());
                        if (mod != null)
                        {
                            Destroy(mod.gameObject);
                            RConsole.Log("[ModManager] " + md.jsonmodinfo.ModName + " > Mod Successfully Unloaded!");
                        }
                    }
                }
                else
                {
                    RConsole.LogWarning("[ModManager] " + md.jsonmodinfo.ModName + " > The mod doesn't support unload! (Trying to manually unload it, This could lead into some issues)");
                    Destroy(mod.gameObject);
                    RConsole.Log("[ModManager] " + md.jsonmodinfo.ModName + " > Mod Successfully Unloaded!");
                }

                md.modinfo.modState = ModInfo.ModStateEnum.idle;
                string lastHash = md.modinfo.fileHash;
                string currentHash = Utils.GetFileMD5Hash(md.modinfo.modFile.FullName);
                bool modHasUpdated = (lastHash != currentHash);
                if (modHasUpdated || md.modinfo.assembly == null)
                {
                    RefreshMods();
                }
                else
                {
                    RefreshModsStates();
                }
            }
            catch (Exception e)
            {
                Debug.LogError("[ModManager] " + md.jsonmodinfo.ModName + " > An error occured while unloading the mod!\nStacktrace : " + e.ToString());
            }
        }*/
    }
}