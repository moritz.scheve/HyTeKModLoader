﻿using ICSharpCode.SharpZipLib.Zip;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace RaftModLoader
{
    public class ZipModHandler : BaseModHandler
    {
        public override async Task<ModData> GetModData(FileInfo file, Dictionary<string, Assembly> assemblyList = null)
        {
            ModData moddata = await GetModDataFromZipStream(file);
            return moddata;
        }

        public override void LoadMod(ModData moddata) { throw new NotImplementedException(); }

        public override void UnloadMod(ModData moddata) { throw new NotImplementedException(); }

        public async Task<ModData> GetModDataFromZipStream(FileInfo file)
        {
            ModData moddata = new ModData();
            moddata.jsonmodinfo = null;
            moddata.modinfo.modHandler = this;
            moddata.modinfo.modFile = file;
            moddata.modinfo.fileHash = Utils.GetFileSHA512Hash(file.FullName);
            using (Stream stream = File.OpenRead(file.FullName))
            {
                using (var zipInputStream = new ZipInputStream(stream))
                {
                    while (zipInputStream.GetNextEntry() is ZipEntry v)
                    {
                        var zipentry = v.Name;
                        StreamReader reader = new StreamReader(zipInputStream);

                        if (!moddata.modinfo.modFiles.ContainsKey(zipentry))
                        {
                            try
                            {
                                var bytes = default(byte[]);
                                using (var memstream = new MemoryStream())
                                {
                                    reader.BaseStream.CopyTo(memstream);
                                    bytes = memstream.ToArray();
                                }
                                moddata.modinfo.modFiles.Add(zipentry, bytes);

                                if (zipentry.ToLower() == "modinfo.json")
                                {
                                    try
                                    {
                                        moddata.jsonmodinfo = JsonConvert.DeserializeObject<JsonModInfo>(System.Text.Encoding.UTF8.GetString(bytes));
                                        moddata.jsonmodinfo.ModPackage = moddata.jsonmodinfo.ModPackage.ToLower();
                                        for (int i = 0; i < moddata.jsonmodinfo.ModDependencies.Count; i++)
                                        {
                                            moddata.jsonmodinfo.ModDependencies[i] = moddata.jsonmodinfo.ModDependencies[i].ToLower();
                                            if (moddata.jsonmodinfo.ModPackage == moddata.jsonmodinfo.ModDependencies[i])
                                            {
                                                Debug.LogError("[ModManager] " + file.Name + " > The mod can't have itself as a dependency!");
                                                return null;
                                            }
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        Debug.LogError("[ModManager] " + file.Name + " > An error occured while deserializing the modinfo.json file!\nStacktrace : " + e.Message);
                                        return null;
                                    }
                                }

                            }
                            catch (Exception e)
                            {
                                Debug.LogError("[ModManager] " + file.Name + " > An error occured while loading the file " + zipentry + " !\nStacktrace : " + e.ToString());
                                return null;
                            }
                        }
                    }

                    if (moddata.jsonmodinfo == null)
                    {
                        Debug.LogError("[ModManager] " + file.Name + " > The mod is missing a modinfo.json file!");
                        return null;
                    }

                    bool rawcs = false;
                    bool dll = false;
                    bool lnk = false;
                    foreach (KeyValuePair<string, byte[]> f in moddata.modinfo.modFiles)
                    {
                        if (f.Key.ToLower().EndsWith(".cs"))
                        {
                            moddata.modinfo.modType = ModInfo.ModTypeEnum.cs;
                            rawcs = true;
                        }
                        if (f.Key.ToLower().EndsWith(".dll"))
                        {
                            moddata.modinfo.modType = ModInfo.ModTypeEnum.dll;
                            rawcs = true;
                        }
                        if (f.Key.ToLower().EndsWith(".lnk"))
                        {
                            moddata.modinfo.modType = ModInfo.ModTypeEnum.lnk;
                            rawcs = true;
                        }
                    }
                    int filetypeamount = 0;
                    if (rawcs) { filetypeamount++; };
                    if (dll) { filetypeamount++; };
                    if (lnk) { filetypeamount++; };

                    if (filetypeamount == 1)
                    {
                        return moddata;
                    }
                    else if (filetypeamount == 0)
                    {
                        Debug.LogError("[ModManager] " + file.Name + " > The mod is missing a codebase file (Can be a <b>.cs</b>, <b>.dll</b> or a <b>.cs shortcut</b>)!");
                        return null;
                    }
                    else
                    {
                        Debug.LogError("[ModManager] " + file.Name + " > The mod has multiple codebase extensions! (You can only use one type of codebase file. For example you can't use a <b>.dll</b> and <b>.cs</b> file in the same mod. If your mod is too big you can also use dependencies!)");
                        return null;
                    }
                }
            }
        }

    }
}