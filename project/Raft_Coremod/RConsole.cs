﻿using RaftModLoader;
using Steamworks;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TMPro;
using UltimateWater;
using UnityEngine;
using UnityEngine.AzureSky;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RConsole : MonoBehaviour
{
    public static List<GameObject> logs = new List<GameObject>();
    public static bool isConsoleOpen;
    private static GameObject ConsoleContent;
    private static GameObject ConsoleScrollRect;
    private static GameObject ConsoleInputField;
    private static RConsole ConsoleInstance;
    public static List<Command> commands = new List<Command>();
    public static List<string> lastCommands = new List<String>();
    public static bool isInitialized;
    public static string[] lcargs;
    private GameObject AutoComplete;
    bool isingame;
    int currentlc;
    bool lastwasup;
    CustomTMProInputField cinputfield;
    public static TMP_FontAsset font;

    private void Awake()
    {
        ConsoleInstance = this;
        font = RML_Main.rmlAssets.LoadAsset<TMP_FontAsset>("Consolas");
        ConsoleContent = GameObject.Find("RMLConsoleContentObject");
        ConsoleScrollRect = GameObject.Find("RMLConsoleScrollRect");
        ConsoleInputField = GameObject.Find("RMLConsoleInputField");
        ConsoleInputField.AddComponent<CustomTMProInputField>();
        cinputfield = ConsoleInputField.GetComponent<CustomTMProInputField>();
        cinputfield.enabled = false;
        cinputfield.enabled = true;
        AutoComplete = ConsoleInputField.transform.Find("RMLConsoleAutoComplete").gameObject;
        AutoComplete.transform.Find("RMLConsoleAutoCompleteText").gameObject.AddComponent<TextMeshProUGUI>().font = font;
        AutoComplete.transform.Find("RMLConsoleAutoCompleteText").gameObject.GetComponent<TextMeshProUGUI>().fontSize = 10;
        AutoComplete.transform.Find("RMLConsoleAutoCompleteText").gameObject.GetComponent<TextMeshProUGUI>().alignment = TextAlignmentOptions.Left;
        AutoComplete.transform.Find("RMLConsoleAutoCompleteText").gameObject.GetComponent<RectTransform>().anchorMin = new Vector2(0, 0);
        AutoComplete.transform.Find("RMLConsoleAutoCompleteText").gameObject.GetComponent<RectTransform>().anchorMax = new Vector2(1, 1);
        AutoComplete.transform.Find("RMLConsoleAutoCompleteText").gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(-10, 0);
        GameObject cinputarea = ConsoleInputField.transform.Find("Text Area").gameObject;
        cinputarea.AddComponent<RectMask2D>();
        ConsoleInputField.transform.Find("Text Area").Find("Text").gameObject.AddComponent<TextMeshProUGUI>().font = font;
        ConsoleInputField.transform.Find("Text Area").Find("Placeholder").gameObject.AddComponent<TextMeshProUGUI>().font = font;
        cinputfield.textComponent = ConsoleInputField.transform.Find("Text Area").Find("Text").gameObject.GetComponent<TextMeshProUGUI>();
        cinputfield.textComponent.richText = false;
        cinputfield.textComponent.fontSize = 10;
        cinputfield.textComponent.font = font;
        cinputfield.pointSize = 10;
        cinputfield.placeholder = ConsoleInputField.transform.Find("Text Area").Find("Placeholder").gameObject.GetComponent<TextMeshProUGUI>();
        cinputfield.textViewport = cinputfield.GetComponentInChildren<RectTransform>();
        cinputfield.textComponent.alignment = TextAlignmentOptions.Left;
        cinputfield.textComponent.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 10);

        GameObject go = new GameObject(transform.name + " Input Caret");
        go.AddComponent<TMP_SelectionCaret>();
        go.hideFlags = HideFlags.DontSave;
        go.transform.SetParent(ConsoleInputField.transform.Find("Text Area").transform);
        go.transform.SetAsFirstSibling();
        go.layer = gameObject.layer;
        go.AddComponent<LayoutElement>().ignoreLayout = true;
        ConsoleInputField.GetComponent<CustomTMProInputField>().onEndEdit.AddListener(val =>
        {
            if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
                sendCommand();
        });
        ConsoleInputField.GetComponent<CustomTMProInputField>().onValueChanged.AddListener(val =>
        {
            OnCommandInputChange();
        });

        ConsoleInputField.GetComponent<CustomTMProInputField>().onFocusSelectAll = false;
        isInitialized = true;
        LoadBoundCommands();
    }

    List<string> blockedWords = new List<string>();

    void OnEnable()
    {
        blockedWords.Clear();
        blockedWords.Add("opt-out");
        blockedWords.Add("jobtempalloc");
        blockedWords.Add("tla_debug_stack_leak");
        blockedWords.Add("sendwillrendercanvases");
        blockedWords.Add("missing default terrain shader.");
        blockedWords.Add("your current multi-scene setup has inconsistent lighting");
        blockedWords.Add("\"setdestination\" can only be called on an active agent that has been placed on a navmesh.");
        Application.logMessageReceived += HandleUnityLog;
    }

    void OnDisable()
    {
        Application.logMessageReceived -= HandleUnityLog;
    }

    void HandleUnityLog(string logString, string stackTrace, LogType type)
    {
        foreach (string s in blockedWords)
        {
            if (logString.ToLower().Contains(s) || stackTrace.ToLower().Contains(s))
                return;
        }
        if (stackTrace.StartsWith("0x"))
        {
            Log(type, logString);
        }
        else
        {
            Log(type, logString + "\n" + stackTrace);
        }
    }

    async void Start()
    {
        registerCommand(typeof(RML_Main), "Shows you all the available commands.", "help", HelpCommand);
        registerCommand(typeof(RML_Main), "Shows you the current unity Version.", "unityVersion", () => RConsole.Log("Current Unity Version: " + Application.unityVersion));
        registerCommand(typeof(RML_Main), "Shows you the current Raft Version.", "raftVersion", () => RConsole.Log("Current Raft Version: " + Settings.VersionNumberText));
        registerCommand(typeof(RML_Main), "Shows you the current .NET version.", "dotnetversion", () => RConsole.Log("Current .NET Version: " + Environment.Version.ToString()));
        registerCommand(typeof(RML_Main), "Clear the current console output.", "clear", ClearCommand);
        registerCommand(typeof(RML_Main), "Shows you all your friends currently playing Raft.", "listRafters", listRafters);
        registerCommand(typeof(RML_Main), "Join a server, usage: <u><i>connect SteamID password</i></u>.", "connect", TryJoinServer);
        registerCommand(typeof(RML_Main), "Change the current fullscreen mode, usage: <u><i>fullscreenMode windowed/fullscreen/borderless</i></u>", "fullscreenMode", changeFullscreen);
        registerCommand(typeof(RML_Main), "Get the current roslyn compiler path.", "getcompiler", () => RawSharp.GetCSCPath());
        registerCommand(typeof(RML_Main), "Syntax: 'settimescale <value>' Change the game speed (Default is 1).", "settimescale", settimescale);
        registerCommand(typeof(RML_Main), "Kill yourself.", "kill", kill);
        registerCommand(typeof(RML_Main), "Toggle the noclip.", "noclip", noclip);
        registerCommand(typeof(RML_Main), "Teleports you to your raft.", "gotoraft", gotoraft);
        registerCommand(typeof(RML_Main), "Exit the game.", "exit", () => Application.Quit());
        registerCommand(typeof(RML_Main), "Syntax: 'bind <key> <command>' Bind a console command to a key.", "bind", BindCommand);
        registerCommand(typeof(RML_Main), "Syntax: 'unbind <key>' Unbind a console comand from a key.", "unbind", UnbindCommand);
        registerCommand(typeof(RML_Main), "Unbind all commands from all keys.", "unbindall", Unbindall);
        registerCommand(typeof(RML_Main), "Syntax: 'timeset <value>' Change the game time.", "timeset", timeset);
        registerCommand(typeof(RML_Main), "Allows you to run csharp code at runtime.", "csrun", RawSharp.CSRUN);
        registerCommand(typeof(RML_Main), "Makes the game looks like minecraft but it helps to play the game on low-end devices such as old laptops.", "poopmode", PoopMode);
        registerCommand(typeof(RML_Main), "Toggle the RaftModLoader chat. (Temporarily fix for Russian & Chinese characters)", "togglecustomchat", RChat.ToggleCustomChat);
        //registerCommand(typeof(RML_Main), "Syntax: 'kick <username/steamid>' Eject a player from the game.", "kick", kick);

        ClearCommand();
        Log("Welcome to RaftModLoader!");

        await Task.Delay(1000);
        ConsoleInstance.gameObject.GetComponent<CanvasGroup>().alpha = 1;
    }

    void PoopMode()
    {
        QualitySettings.pixelLightCount = 0;
        QualitySettings.masterTextureLimit = 5;
        QualitySettings.anisotropicFiltering = AnisotropicFiltering.Disable;
        QualitySettings.antiAliasing = 0;
        QualitySettings.softParticles = false;
        QualitySettings.realtimeReflectionProbes = false;
        QualitySettings.billboardsFaceCameraPosition = false;
        QualitySettings.shadowCascades = 0;
        QualitySettings.shadows = ShadowQuality.Disable;
        QualitySettings.softVegetation = false;
        QualitySettings.blendWeights = BlendWeights.OneBone;
        QualitySettings.vSyncCount = 0;
        QualitySettings.lodBias = 0;
        QualitySettings.maximumLODLevel = 0;
        WaterQualitySettings.Instance.SetQualityLevel(0);
        QualitySettings.shadowDistance = 0;
        QualitySettings.maxQueuedFrames = 1000;
        Settings settings = FindObjectOfType<Settings>();
        settings.graphicsBox.postEffects.ambientOcclusion.enabled = false;
        settings.graphicsBox.postEffects.antialiasing.enabled = false;
        settings.graphicsBox.postEffects.bloom.enabled = false;
        settings.graphicsBox.postEffects.chromaticAberration.enabled = false;
        settings.graphicsBox.postEffects.colorGrading.enabled = false;
        settings.graphicsBox.postEffects.debugViews.enabled = false;
        settings.graphicsBox.postEffects.depthOfField.enabled = false;
        settings.graphicsBox.postEffects.dithering.enabled = false;
        settings.graphicsBox.postEffects.eyeAdaptation.enabled = false;
        settings.graphicsBox.postEffects.fog.enabled = false;
        settings.graphicsBox.postEffects.grain.enabled = false;
        settings.graphicsBox.postEffects.motionBlur.enabled = false;
        settings.graphicsBox.postEffects.screenSpaceReflection.enabled = false;
        settings.graphicsBox.postEffects.userLut.enabled = false;
        settings.graphicsBox.postEffects.vignette.enabled = false;

        FindObjectOfType<VolumetricLightRenderer>().Resolution = VolumetricLightRenderer.VolumtericResolution.Quarter;
        FindObjectOfType<VolumetricLightRenderer>().enabled = false;

        Application.backgroundLoadingPriority = ThreadPriority.Low;
        Application.runInBackground = true;
    }

    /*void kick()
    {
        lcargs = lcargs.Skip(1).ToArray();
        if (lcargs.Length == 0)
        {
            RConsole.LogWarning("You must specify an username or a steamid.");
            return;
        }
        if (!Semih_Network.IsHost)
        {
            RConsole.LogWarning("You must be the host of the current game to kick someone.");
            return;
        }

        string input = string.Join(" ", lcargs);

        Dictionary<CSteamID,Network_Player> players = ComponentManager<Semih_Network>.Value.remoteUsers;

        Network_Player choosenPlayer = null;
        Semih_Network network = ComponentManager<Semih_Network>.Value;

        foreach (KeyValuePair<CSteamID, Network_Player> p in players)
        {
            if(p.Key == network.HostID) { continue; }
            if (p.Value.characterSettings.Name.ToLower() == input.ToLower())
            {
                choosenPlayer = p.Value;
                break;
            }

            if (p.Key.ToString() == input.ToLower())
            {
                choosenPlayer = p.Value;
                break;
            }
        }

        if (choosenPlayer == null)
        {
            RConsole.LogWarning("No player has been found!");
            return;
        }

        ChatManager chatManager = ComponentManager<ChatManager>.Value;
        CSteamID csteamid = network.HostID;
        Message_IngameChat nmessage = new Message_IngameChat(Messages.Ingame_Chat_Message, chatManager, csteamid, "<color=#c94040>You have been kicked from the server!</color>");
        network.SendP2P(choosenPlayer.steamID, nmessage, EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
        SteamNetworking.CloseP2PSessionWithUser(choosenPlayer.steamID);
    }*/

    void settimescale()
    {
        lcargs = lcargs.Skip(1).ToArray();
        if (lcargs.Length == 0)
        {
            RConsole.LogWarning("You must specify a value to change the game speed.");
            return;
        }

        lcargs[0] = lcargs[0].Replace(".", ",");
        float newValue = 1;
        float.TryParse(lcargs[0], out newValue);

        if (newValue < 0 || newValue > 100)
        {
            RConsole.LogWarning("The value that you specified is invalid, It should be between 0 and 100!");
            return;
        }
        else
        {
            Time.timeScale = newValue;
            RConsole.Log("The game timescale has been changed to " + newValue);
        }
    }

    void timeset()
    {
        lcargs = lcargs.Skip(1).ToArray();
        if (lcargs.Length == 0)
        {
            RConsole.LogWarning("You must specify a value to change the game time.");
            return;
        }

        lcargs[0] = lcargs[0].Replace(".", ",");
        float newValue = 1;
        float.TryParse(lcargs[0], out newValue);

        if (newValue < 0 || newValue > 24)
        {
            RConsole.LogWarning("The value that you specified is invalid, It should be between 0 and 24!");
            return;
        }
        else
        {
            FindObjectOfType<AzureSkyController>().timeOfDay.GotoTime(newValue);
            RConsole.Log("The game time has been changed to " + newValue);
        }
    }

    void kill()
    {
        Network_Player ply = RAPI.GetLocalPlayer();
        if (ply != null && !ply.IsKilled && SceneManager.GetActiveScene().name == Semih_Network.GameSceneName)
        {
            ply.PlayerScript.Kill();
            RConsole.Log("The suicide note was written in blue ink.");
            return;
        }
        RConsole.LogWarning("You can't suicide right now.");
    }

    void noclip()
    {
        Network_Player ply = RAPI.GetLocalPlayer();
        if (ply != null && !ply.IsKilled && SceneManager.GetActiveScene().name == Semih_Network.GameSceneName)
        {
            ply.flightCamera.Toggle(true);
            return;
        }
        RConsole.LogWarning("You can't use noclip right now.");
    }

    void gotoraft()
    {
        Network_Player ply = RAPI.GetLocalPlayer();
        if (ply != null && !ply.IsKilled && SceneManager.GetActiveScene().name == Semih_Network.GameSceneName)
        {
            ply.PersonController.CameraSubmersionChanged(UltimateWater.SubmersionState.None, true);
            ply.PersonController.SwitchControllerType(ControllerType.Ground);
            ply.SetToWalkableBlockPosition();
            ply.PlayerScript.MakeUnStuck();
            return;
        }
        RConsole.LogWarning("You can't teleport to your raft right now.");
    }


    public Dictionary<KeyCode, string> boundCommands = new Dictionary<KeyCode, string>();
    void BindCommand()
    {
        lcargs = lcargs.Skip(1).ToArray();
        if (lcargs.Length < 2)
        {
            Debug.LogWarning("You must specify a key and a command as arguments to 'bind'.");
            return;
        }

        KeyCode key = KeyCodeFromString(lcargs[0]);
        if (key == KeyCode.None) { return; }
        string command = string.Join(" ", lcargs.Skip(1).ToArray());
        boundCommands[key] = command;
        UpdateBoundCommandsFile();
    }

    void UnbindCommand()
    {
        lcargs = lcargs.Skip(1).ToArray();
        if (lcargs.Length != 1)
        {
            Debug.LogWarning("Command 'unbind' only takes 1 argument.");
            return;
        }

        KeyCode key = KeyCodeFromString(lcargs[0]);
        if (key == KeyCode.None) { return; }
        boundCommands.Remove(key);
        UpdateBoundCommandsFile();
    }

    void Unbindall()
    {
        boundCommands.Clear();
        UpdateBoundCommandsFile();
    }

    public static KeyCode KeyCodeFromString(string keyString)
    {
        if (keyString.Length == 1)
        {
            keyString = keyString.ToUpper();
        }

        KeyCode key = KeyCode.None;
        try
        {
            key = (KeyCode)Enum.Parse(typeof(KeyCode), keyString);
        }
        catch (ArgumentException)
        {
            Debug.LogError("Key '" + keyString + "' does not specify a key code.");
        }
        return key;
    }

    public void LoadBoundCommands()
    {
        if (PlayerPrefs.HasKey("rml4.boundcommands"))
        {
            JSONObject j = new JSONObject(PlayerPrefs.GetString("rml4.boundcommands"));
            if (j.IsArray && j.list.Count >= 1)
            {
                foreach (JSONObject obj in j.list)
                {
                    try
                    {
                        string[] args = obj.str.Split(new[] { "•◆•◆•" }, StringSplitOptions.None);
                        if (args.Length < 2)
                        {
                            Debug.LogWarning("You must specify a key and a command as arguments to 'bind'.");
                            return;
                        }

                        KeyCode key = KeyCodeFromString(args[0]);
                        if (key == KeyCode.None) { return; }
                        string command = string.Join(" ", args.Skip(1).ToArray());
                        boundCommands[key] = command;
                        UpdateBoundCommandsFile();
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e);
                    }
                }
                Debug.Log("Successfully loaded bound commands.");
            }
        }
    }

    public void UpdateBoundCommandsFile()
    {
        string folder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "RaftModLoader");
        JSONObject j = new JSONObject(JSONObject.Type.ARRAY);
        foreach (KeyValuePair<KeyCode, string> v in boundCommands)
        {
            j.Add(v.Key.ToString() + "•◆•◆•" + v.Value);
        }
        PlayerPrefs.SetString("rml4.boundcommands", j.Print());
    }

    void changeFullscreen()
    {
        if (lcargs.Length != 2)
        {
            LogWarning("Syntax error! Usage: <u><i>fullscreenMode windowed/fullscreen/borderless</i></u>");
            return;
        }
        string mode = lcargs[1].ToLower();
        switch (mode)
        {
            case "windowed":
                Screen.SetResolution(Screen.width, Screen.height, FullScreenMode.Windowed);
                break;
            case "fullscreen":
                Screen.SetResolution(Screen.width, Screen.height, FullScreenMode.ExclusiveFullScreen);
                break;
            case "borderless":
                Screen.SetResolution(Screen.width, Screen.height, FullScreenMode.FullScreenWindow);
                break;
            default:
                LogWarning("Invalid fullscreen mode, Valid modes are <i>Windowed</i>, <i>Fullscreen</i> or <i>Borderless</i>");
                break;
        }
    }

    void listRafters()
    {
        int friendCount = SteamFriends.GetFriendCount(EFriendFlags.k_EFriendFlagImmediate);
        List<string> rafters = new List<string>();
        for (int i = 0; i < friendCount; ++i)
        {
            CSteamID friendSteamId = SteamFriends.GetFriendByIndex(i, EFriendFlags.k_EFriendFlagImmediate);
            string friendName = SteamFriends.GetFriendPersonaName(friendSteamId);
            FriendGameInfo_t gameInfo;
            SteamFriends.GetFriendGamePlayed(friendSteamId, out gameInfo);
            if (gameInfo.m_gameID.ToString() == "648800")
            {
                rafters.Add(friendName + " is playing Raft! its SteamID is " + friendSteamId.GetAccountID().ToString());
            }
        }
        if (rafters.Count > 0)
        {
            Log("You have " + rafters.Count + " friend(s) currently playing Raft!");
            foreach (string s in rafters)
            {
                Log(s);
            }
        }
        else
        {
            Log("None of your friends are currently playing Raft!");
        }
    }

    public void TryJoinServer()
    {
        if (lcargs.Length < 2 || lastCommands.LastOrDefault().Length >= 50)
        {
            LogWarning("Syntax error! Usage: <u><i>connect SteamID password</i></u>");
            return;
        }
        string steamid = lcargs[1];
        string password = "";
        if (lcargs.Length > 2)
        {
            lcargs[0] = null;
            lcargs[1] = null;
            password = String.Join(" ", lcargs).TrimStart(' ');
        }

        try
        {
            if (steamid.Length > 5 && steamid.Length < 12)
            {
                uint accountid = uint.Parse(steamid);
                CSteamID csteamid = new CSteamID(new AccountID_t(accountid), EUniverse.k_EUniversePublic, EAccountType.k_EAccountTypeIndividual);
                if (!csteamid.IsValid())
                {
                    LogWarning("The provided SteamID is invalid, Valid SteamID's are SteamID,SteamID64,SteamID3 and AccountID");
                    return;
                }
                ServersPage.ConnectToServer(csteamid, password, false);
                return;
            }
        }
        catch { }
        RSocket.convertSteamid(steamid, password);
        return;
    }

    public void HelpCommand()
    {
        Log("Available commands:");
        foreach (Command c in commands)
        {
            Log("   " + c.command + " - " + c.desc);
        }
    }

    public void ClearCommand()
    {
        logs.Clear();
        foreach (Transform child in ConsoleContent.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    public void sendCommand(string command = "")
    {
        if (command == "")
        {
            if (string.IsNullOrEmpty(ConsoleInputField.GetComponent<CustomTMProInputField>().text))
            {
                if (isConsoleOpen)
                {
                    ConsoleInputField.GetComponent<CustomTMProInputField>().Select();
                    ConsoleInputField.GetComponent<CustomTMProInputField>().ActivateInputField();
                }
                return;
            }
        }

        if (command == "")
        {
            command = ConsoleInputField.GetComponent<CustomTMProInputField>().text;
        }
        ConsoleInputField.GetComponent<CustomTMProInputField>().text = String.Empty;
        Log(LogType.Log, "> " + command, false);
        lastCommands.Add(command);
        currentlc = 0;
        foreach (Command c in commands)
        {
            string[] args = command.Split(' ');
            if (args[0].ToLower().Equals(c.command.ToLower()))
            {
                lcargs = args;
                c.action.Invoke();
                if (command == "")
                {
                    if (isConsoleOpen)
                    {
                        ConsoleInputField.GetComponent<CustomTMProInputField>().Select();
                        ConsoleInputField.GetComponent<CustomTMProInputField>().ActivateInputField();
                    }
                }
                return;
            }
        }
        LogWarning("Unknown command type help for help.");
        if (isConsoleOpen)
        {
            ConsoleInputField.GetComponent<CustomTMProInputField>().Select();
            ConsoleInputField.GetComponent<CustomTMProInputField>().ActivateInputField();
        }
    }

    private void OnCommandInputChange()
    {
        if (!isConsoleOpen)
        {
            ConsoleInputField.GetComponent<CustomTMProInputField>().text = "";
            ConsoleInputField.GetComponent<CustomTMProInputField>().DeactivateInputField();
        }
        if (AutoComplete != null)
        {
            string command = ConsoleInputField.GetComponent<CustomTMProInputField>().text.ToLower();


            if (command != "")
            {
                bool found = false;
                foreach (Command c in commands)
                {
                    if (c.command.ToLower().StartsWith(command))
                    {
                        if (c.command.ToLower() == command)
                        {
                            AutoComplete.GetComponentInChildren<TextMeshProUGUI>().text = "";
                            AutoComplete.SetActive(false);
                            return;
                        }
                        found = true;
                        AutoComplete.SetActive(true);
                        AutoComplete.GetComponentInChildren<TextMeshProUGUI>().text = c.command;
                        return;
                    }
                }
                if (!found)
                {
                    AutoComplete.GetComponentInChildren<TextMeshProUGUI>().text = "";
                    AutoComplete.SetActive(false);
                }
            }
            else
            {
                AutoComplete.GetComponentInChildren<TextMeshProUGUI>().text = "";
                AutoComplete.SetActive(false);
            }

        }
    }

    void Update()
    {
        if ((Cursor.lockState == CursorLockMode.Locked || !Cursor.visible) && isConsoleOpen)
        {
            RAPI.TogglePriorityCursor(isConsoleOpen);
        }

        if (CanvasHelper.ActiveMenu == 0 && !isConsoleOpen && !MainMenu.IsOpen)
        {
            foreach (var boundCommand in boundCommands)
            {
                if (Input.GetKeyDown(boundCommand.Key))
                {
                    sendCommand(boundCommand.Value);
                }
            }
        }
    }

    private void LateUpdate()
    {
        if (isConsoleOpen && Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (!lastwasup)
            {
                currentlc--;
            }
            lastwasup = true;
            if (currentlc <= 0 || lastCommands.Count - currentlc < 0)
            {
                currentlc = 1;
            }

            if (lastCommands.Count > 0)
            {

                ConsoleInputField.GetComponent<CustomTMProInputField>().text = lastCommands[lastCommands.Count - currentlc] ?? "";
                currentlc++;
                StartCoroutine(AutocompleteSetCaret());
            }
        }

        if (isConsoleOpen && Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (lastwasup)
            {
                currentlc = lastCommands.Count - currentlc + 2;
            }
            lastwasup = false;
            if (currentlc > lastCommands.Count - 1)
            {
                currentlc = 0;
            }
            else if (currentlc < 0)
            {
                currentlc = lastCommands.Count - 1;
            }
            if (lastCommands.Count > 0)
            {
                ConsoleInputField.GetComponent<CustomTMProInputField>().text = lastCommands[currentlc] ?? "";
                currentlc++;
                StartCoroutine(AutocompleteSetCaret());
            }
        }

        isingame = SceneManager.GetActiveScene().name == Semih_Network.GameSceneName;
        if (Input.GetKeyDown(RML_Main.ConsoleKey))
        {
            isConsoleOpen = !isConsoleOpen;
            RAPI.TogglePriorityCursor(isConsoleOpen);
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && isConsoleOpen)
        {
            isConsoleOpen = false;
            RAPI.TogglePriorityCursor(isConsoleOpen);
        }

        if (isConsoleOpen)
        {
            if (!GetComponent<Canvas>().enabled)
            {
                GetComponent<Canvas>().enabled = true;
                GetComponent<Animation>().Play("ConsoleOpen");
                CustomTMProInputField cinputfield = ConsoleInputField.GetComponent<CustomTMProInputField>();
            }

            ConsoleInputField.GetComponent<CustomTMProInputField>().Select();
            ConsoleInputField.GetComponent<CustomTMProInputField>().ActivateInputField();

            if (AutoComplete != null)
            {
                if (string.IsNullOrEmpty(AutoComplete.GetComponentInChildren<TextMeshProUGUI>().text))
                {
                    AutoComplete.GetComponentInChildren<TextMeshProUGUI>().text = "";
                    AutoComplete.SetActive(false);
                }
                else
                {
                    if (Input.GetKeyDown(KeyCode.Tab))
                    {
                        ConsoleInputField.GetComponent<CustomTMProInputField>().text = AutoComplete.GetComponentInChildren<TextMeshProUGUI>().text;
                        StartCoroutine(AutocompleteSetCaret());
                    }
                }
            }

        }
        else
        {
            if (GetComponent<Canvas>().enabled)
            {
                StartCoroutine(CloseConsoleLate());
            }
        }
    }

    public IEnumerator CloseConsoleLate()
    {
        GetComponent<Animation>().Play("ConsoleClose");
        yield return new WaitForSeconds(0.10f);
        GetComponent<Canvas>().enabled = false;
    }

    public IEnumerator AutocompleteSetCaret()
    {
        yield return new WaitForEndOfFrame();
        ConsoleInputField.GetComponent<CustomTMProInputField>().MoveToEndOfLine(false, false);
    }

    public static void Log(string log)
    {
        Log(LogType.Log, log);
    }

    public static void Log(string log, LogType LogType)
    {
        Log(LogType, log);
    }

    public static void LogAssert(string log)
    {
        Log(LogType.Assert, log);
    }

    public static void LogError(string log)
    {
        Log(LogType.Error, log);
    }

    public static void LogException(string log)
    {
        Log(LogType.Exception, log);
    }

    public static void LogWarning(string log)
    {
        Log(LogType.Warning, log);
    }

    public static void Log(LogType LogType, string log, bool enableRichText = true)
    {
        if (isInitialized)
        {
            GameObject textobj = new GameObject();
            textobj.transform.parent = ConsoleContent.transform;
            textobj.transform.localScale = new Vector3(1, 1, 1);
            textobj.name = "ConsoleLine";
            TextMeshProUGUI t = textobj.AddComponent<TextMeshProUGUI>();
            t.richText = enableRichText;
            t.font = font;
            t.GetComponent<TextMeshProUGUI>().fontSize = 9;
            string result = "";
            switch (LogType)
            {
                case LogType.Assert:
                    result = log;
                    if (!isConsoleOpen && RNotify.errornotification != null)
                        RNotify.errornotification.AddNewError();
                    t.color = new Color(0.7372f, 0.1451f, 0.1451f);
                    break;
                case LogType.Error:
                    result = log;
                    if (!isConsoleOpen && RNotify.errornotification != null)
                        RNotify.errornotification.AddNewError();
                    t.color = new Color(0.7372f, 0.1451f, 0.1451f);
                    break;
                case LogType.Exception:
                    result = log;
                    if (!isConsoleOpen && RNotify.errornotification != null)
                        RNotify.errornotification.AddNewError();
                    t.color = new Color(0.7372f, 0.1451f, 0.1451f);
                    break;
                case LogType.Log:
                    result = log.ToString();
                    t.color = new Color(1, 1, 1);
                    break;
                case LogType.Warning:
                    result = log;
                    t.color = new Color(0.9098f, 0.6235f, 0.2509f);
                    break;
            }
            t.text = result;
            logs.Add(textobj);
            ConsoleInstance.StartCoroutine(ScrollToBottom());
        }
    }

    public static IEnumerator ScrollToBottom()
    {
        if (logs.Count >= 150)
        {
            if (logs.First() != null)
            {
                Destroy(logs.First());
                logs.Remove(logs.First());
            }
        }
        yield return new WaitForEndOfFrame();
        ConsoleScrollRect.SetActive(true);
        ConsoleScrollRect.GetComponent<ScrollRect>().verticalNormalizedPosition = 0f;
    }

    [Obsolete("RConsole.registerCommand(name,desc,command,action) is deprecated, please use RConsole.registerCommand(modType,desc,command,action) instead!")]
    public static void registerCommand(string name, string desc, string command, Action action)
    {
        Log("<color=red>[RML Deprecated Warning] Command \"" + name + "\" is registered using a deprecated method, it has been registered as \"" + command + "\"</color>");
        registerCommand(typeof(RML_Main), desc, command, action);
    }

    public static void registerCommand(Type modType, string desc, string command, Action action)
    {
        Regex r = new Regex("^[a-zA-Z0-9]*$");
        if (command.Contains(" "))
        {
            LogError("Command " + command + " contains spaces!");
            return;
        }
        if (!r.IsMatch(command))
        {
            LogError("Command " + command + " contains invalids characters!");
            return;
        }

        foreach (Command c in commands)
        {
            if (c.command.ToLower().Equals(command.ToLower()))
            {
                LogError("Command " + command + " is already registered!");
                return;
            }
        }
        Command cmd = new Command();
        cmd.desc = desc;
        cmd.command = command;
        cmd.action = action;
        cmd.mod = modType;
        commands.Add(cmd);
        Log("Successfully registered command <i>" + cmd.command + "</i>.");
    }

    public static void unregisterCommand(string command)
    {
        foreach (Command c in commands)
        {
            if (c.command.ToLower().Equals(command.ToLower()))
            {
                commands.Remove(c);
                Log("Successfully unregistered command <i>" + c.command + "</i>.");
                return;
            }
        }
    }

}

[Serializable]
public class Command
{
    public string desc;
    public string command;
    public Action action;
    public Type mod;
}
