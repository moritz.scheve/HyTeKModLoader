﻿using FMODUnity;
using Harmony;
using RaftModLoader;
using Steamworks;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace RaftModLoader
{
    [HarmonyPatch(typeof(Semih_Network), "SendP2P", new Type[] { typeof(CSteamID), typeof(Packet_Single), typeof(NetworkChannel) })]
    class PatchSendP2P
    {
        static bool Prefix(Semih_Network __instance, CSteamID steamID, Packet_Single packetSingle, NetworkChannel channel)
        {
            if (!steamID.IsValid())
            {
                return false;
            }
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            MemoryStream memoryStream = new MemoryStream();
            binaryFormatter.Serialize(memoryStream, packetSingle);
            if (packetSingle.SendType == EP2PSend.k_EP2PSendUnreliable && memoryStream.Length > 1196L)
            {
                Debug.LogError(string.Concat(new object[]
                {
                "Sending a packet single of type unreliable with to much bytes: ",
                memoryStream.Length,
                "/",
                1196
                }));
                return false;
            }

            long totalSize = memoryStream.Length;
            if (packetSingle.SendType == EP2PSend.k_EP2PSendReliable && totalSize > 999990L)
            {
                Debug.Log("Reliable Packet is bigger than the max send size! Fragmenting...");

                byte[] binaryData = memoryStream.ToArray();
                int maxFragmentSize = 999000;
                string packetID = Guid.NewGuid().ToString();
                int packetCount = 0;

                for (int i = 0; i < binaryData.Length; i += maxFragmentSize)
                {
                    int maxRange = Math.Min(maxFragmentSize, binaryData.Length - i);

                    byte[] packetFragment = new byte[maxRange];
                    Buffer.BlockCopy(binaryData, i, packetFragment, 0, maxRange);

                    memoryStream = new MemoryStream();
                    var fragment = new Packet_Single(EP2PSend.k_EP2PSendReliable, new FragmentedPacket() { packetID = packetID, lastPacket = maxRange != maxFragmentSize, packetData = packetFragment, totalDataSize = totalSize, packetIndex = packetCount, channel = channel });

                    binaryFormatter.Serialize(memoryStream, fragment);
                    if (!SteamNetworking.SendP2PPacket(steamID, memoryStream.ToArray(), (uint)memoryStream.Length, packetSingle.SendType, (int)2))
                    {
                        Debug.LogError("Failed to send fragmented message: " + packetSingle.message);
                    }
                    packetCount++;
                }

                Debug.Log("Fragmenting large reliable packet completed, sent " + packetCount + " fragments.");
                return false;
            }
            if (!SteamNetworking.SendP2PPacket(steamID, memoryStream.ToArray(), (uint)memoryStream.Length, packetSingle.SendType, (int)channel) && Application.isEditor)
            {
                Debug.LogError("Failed to send message: " + packetSingle.message);
            }
            return false;
        }
    }

    public static class TemporaryPacketSplitingReadingClass
    {
        public static List<FragmentedPacket> packets = new List<FragmentedPacket>();

        public static void TemporaryPacketSplitingReading()
        {
            uint num;
            while (SteamNetworking.IsP2PPacketAvailable(out num, 2))
            {
                byte[] array = new byte[num];
                uint num2;
                CSteamID csteamID;
                if (SteamNetworking.ReadP2PPacket(array, num, out num2, out csteamID, 2))
                {
                    MemoryStream serializationStream = new MemoryStream(array);
                    BinaryFormatter bf = new BinaryFormatter();
                    bf.Binder = new PreMergeToMergedDeserializationBinder();
                    Packet packet = bf.Deserialize(serializationStream) as Packet;
                    Packet_Single singlePacket = (Packet_Single)packet;

                    if (singlePacket.message.Type == (Messages)4000)
                    {
                        FragmentedPacket frag = singlePacket.message as FragmentedPacket;
                        packets.Add(frag);

                        if (frag.lastPacket)
                        {
                            List<FragmentedPacket> allFrags = packets.Where(x => x.packetID == frag.packetID).OrderBy(x => x.packetID).ToList();
                            long binaryDataLength = allFrags[0].totalDataSize;

                            Debug.Log("Finished Receiving fragmented data, Size: " + allFrags.Count);

                            List<byte> receivedData = new List<byte>();
                            byte[] reformedData = new byte[binaryDataLength];
                            int offset = 0;
                            NetworkChannel channel = allFrags[0].channel;

                            for (int i = 0; i < allFrags.Count; i++)
                            {
                                offset += allFrags[i].packetData.Length;
                                receivedData.AddRange(allFrags[i].packetData.ToList());
                                packets.Remove(allFrags[i]);
                            }

                            reformedData = receivedData.ToArray();

                            #region RAFT DEFAULT P2P NETWORKING CODE
                            serializationStream = new MemoryStream(reformedData);
                            packet = new BinaryFormatter().Deserialize(serializationStream) as Packet;
                            Packet_Multiple packet_Multiple;
                            if (packet.PacketType == PacketType.Single)
                            {
                                Packet_Single packet_Single = packet as Packet_Single;
                                packet_Multiple = new Packet_Multiple(packet_Single.SendType);
                                packet_Multiple.messages = new Message[]
                                {
                                packet_Single.message
                                };
                            }
                            else
                            {
                                packet_Multiple = (packet as Packet_Multiple);
                            }
                            NetworkUpdateManager.Deserialize(packet_Multiple, csteamID);
                            #endregion
                        }
                    }
                }
            }
        }
    }

    [Serializable]
    public class FragmentedPacket : Message
    {
        public string packetID;
        public int packetIndex;
        public bool lastPacket;
        public byte[] packetData;
        public long totalDataSize;
        public NetworkChannel channel;

        public FragmentedPacket()
        {
            this.Type = (Messages)4000;
        }
    }
}

sealed class PreMergeToMergedDeserializationBinder : SerializationBinder
{
    public override Type BindToType(string assemblyName, string typeName)
    {
        Type typeToDeserialize = null;
        String exeAssembly = Assembly.GetExecutingAssembly().FullName;
        if(typeName == "RaftDedicatedServer.FragmentedPacket")
        {
            return typeof(FragmentedPacket);
        }
        typeToDeserialize = Type.GetType(String.Format("{0}, {1}", typeName, exeAssembly));
        return typeToDeserialize;
    }
}