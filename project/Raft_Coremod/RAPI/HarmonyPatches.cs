﻿using FMODUnity;
using Harmony;
using Steamworks;
using System;
using System.Collections.Generic;

namespace RaftModLoader
{
    public enum RNetworkMessages { PacketFragment = 4000 }
    public enum RNetworkChannel { Fragmentation = 2 }

    [HarmonyPatch(typeof(UseItemController))]
    [HarmonyPatch("StartUsing")]
    static class UseItemController_StartUsing_ModdedItemFix
    {
        static bool Prefix(UseItemController __instance, Item_Base item, Dictionary<string, ItemConnection> ___connectionDictionary, ref Item_Base ___usableItem, ref PlayerAnimator ___playerAnimator, ref Network_Player ___playerNetwork, ref StudioEventEmitter ___SoundEmitter_Equip, ref ItemConnection ___activeObject)
        {
            string itemNameFromUsable = __instance.GetItemNameFromUsable(item);
            if (itemNameFromUsable != string.Empty && !___connectionDictionary.ContainsKey(itemNameFromUsable))
            {
                RConsole.LogWarning("[RAPI] The item " + item.UniqueName + " is not correctly configured. Please use RAPI.SetItemObject() to fix it.");
                ___usableItem = item;
                if (item.settings_usable.AnimationOnSelect != PlayerAnimation.None)
                {
                    ___playerAnimator.SetAnimation(item.settings_usable.AnimationOnSelect, item.settings_usable.ForceAnimationIndex, item.settings_usable.SetTriggering);
                }
                if (___playerNetwork.IsLocalPlayer && item.settings_recipe.CraftingCategory == CraftingCategory.Tools)
                {
                    ___SoundEmitter_Equip.Play();
                }
                ___activeObject = null;
                __instance.OnSelectItem?.Invoke(item);
                return false;
            }

            return true;
        }
    }

    [HarmonyPatch(typeof(Semih_Network))]
    [HarmonyPatch("AddPlayer")]
    [HarmonyPatch(new Type[] { typeof(CSteamID), typeof(RGD_Settings_Character) })]
    class Patch_AddPlayer1
    {
        static void Postfix(Semih_Network __instance, CSteamID steamID)
        {
            Network_Player player = __instance.GetPlayerFromID(steamID);
            if (player != null)
            {
                InternalItemAPI.SetItemObject(player);
            }
        }
    }

    [HarmonyPatch(typeof(Semih_Network))]
    [HarmonyPatch("AddPlayer")]
    [HarmonyPatch(new Type[] { typeof(Message_Player_Create) })]
    class Patch_AddPlayer2
    {
        static void Postfix(Semih_Network __instance, Message_Player_Create msg)
        {
            Network_Player player = __instance.GetPlayerFromID(msg.SteamID);
            if (player != null)
            {
                InternalItemAPI.SetItemObject(player);
            }
        }
    }

    [HarmonyPatch(typeof(MyInput))]
    [HarmonyPatch("CanGetInput", MethodType.Getter)]
    class Patch_RequestWorld
    {
        static bool Prefix(ref bool __result)
        {
            if (RConsole.isConsoleOpen || MainMenu.IsOpen)
            {
                __result = false;
                return false;
            }
            return true;
        }
    }

    /*
    [HarmonyPatch(typeof(Helper))]
    [HarmonyPatch("LogBuild")]
    class Patch_RequestWorld
    {
        static void Postfix(string text)
        {
            if (text == "Semih_Network: GameScene loaded as client... Requesting world") {
                Debug.Log("<size=15>REQUESTING WORLD....</size>");
            }
        }
    }

    [HarmonyPatch(typeof(NetworkUpdateManager))]
    [HarmonyPatch("Deserialize")]
    class Patch_NetworkUpdateManager_Deserialize
    {
        static void Postfix(Packet_Multiple packet, CSteamID remoteID)
        {
            foreach (Message m in packet.messages)
            {
                switch (m.type)
                {
                    case Messages.CreatePlayer:
                        Debug.Log("<size=15>CREATING PLAYER "+((Message_Player_Create)m).characterSettings.Name+"</size>");
                        break;
                    case Messages.ObjectSpawner_Create:
                        Debug.Log("<size=15>CREATING OBJECTSPAWNER</size>");
                        break;
                    case Messages.BlockCreator_Create:
                        Debug.Log("<size=15>CREATING BLOCKCREATOR!</size>");
                        break;
                    case Messages.WorldReceived:
                        Debug.Log("<size=15>WORLD RECEIVED!</size>");
                        break;
                }
            }
        }
    }*/
}