﻿using Harmony;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RaftModLoader
{
    [HarmonyPatch(typeof(Semih_Network))]
    [HarmonyPatch("OnSceneLoaded")]
    class Patch_WorldEvent_WorldLoaded
    {
        static async void Prefix(Semih_Network __instance, Scene scene)
        {
            if (scene.name == __instance.gameSceneName)
            {
                while (!AssetBundleManager.IsAllScenesLoaded)
                {
                    await Task.Delay(100);
                }
                await Task.Delay(1000);
                foreach (Mod mod in UnityEngine.Object.FindObjectsOfType<Mod>())
                {
                    mod.WorldEvent_WorldLoaded();
                }
            }
        }
    }

    [HarmonyPatch(typeof(SaveAndLoad))]
    [HarmonyPatch("SaveWorld")]
    class Patch_WorldEvent_WorldSaved
    {
        static void Postfix(SaveAndLoad __instance)
        {
            foreach (Mod mod in UnityEngine.Object.FindObjectsOfType<Mod>())
            {
                mod.WorldEvent_WorldSaved();
            }
        }
    }

    [HarmonyPatch(typeof(PlayerStats))]
    [HarmonyPatch("Damage")]
    class Patch_LocalPlayerEvent_Hurt
    {
        static void Prefix(PlayerStats __instance, float damage, Vector3 hitPoint, Vector3 hitNormal, EntityType damageInflictorEntityType, SO_Buff buffAsset = null)
        {
            if (__instance.transform == __instance.transform)
            {
                foreach (Mod mod in UnityEngine.Object.FindObjectsOfType<Mod>())
                {
                    mod.LocalPlayerEvent_Hurt(damage, hitPoint, hitNormal, damageInflictorEntityType);
                }
            }
        }
    }

    [HarmonyPatch(typeof(Player))]
    [HarmonyPatch("Kill")]
    class Patch_LocalPlayerEvent_Death
    {
        static void Postfix(Player __instance, ref Network_Player ___playerNetwork)
        {
            if (___playerNetwork.IsLocalPlayer)
            {
                foreach (Mod mod in UnityEngine.Object.FindObjectsOfType<Mod>())
                {
                    mod.LocalPlayerEvent_Death(__instance.transform.position);
                }
            }
        }
    }

    [HarmonyPatch(typeof(Player))]
    [HarmonyPatch("OnRespawnComplete")]
    class Patch_LocalPlayerEvent_Respawn
    {
        static void Postfix(Player __instance, ref Network_Player ___playerNetwork)
        {
            if (___playerNetwork.IsLocalPlayer)
            {
                foreach (Mod mod in UnityEngine.Object.FindObjectsOfType<Mod>())
                {
                    mod.LocalPlayerEvent_Respawn();
                }
            }
        }
    }

    [HarmonyPatch(typeof(CraftingMenu))]
    [HarmonyPatch("CraftItem")]
    class Patch_LocalPlayerEvent_ItemCrafted
    {
        static void Postfix(CraftingMenu __instance, ref Network_Player ___localPlayer, ref SelectedRecipeBox ___selectedRecipeBox)
        {
            if (___localPlayer.IsLocalPlayer)
            {
                foreach (Mod mod in UnityEngine.Object.FindObjectsOfType<Mod>())
                {
                    mod.LocalPlayerEvent_ItemCrafted(___selectedRecipeBox.selectedRecipeItem);
                }
            }
        }
    }

    [HarmonyPatch(typeof(Pickup))]
    [HarmonyPatch("AddItemToInventory")]
    class Patch_LocalPlayerEvent_PickupItem
    {
        static void Postfix(CraftingMenu __instance, ref Network_Player ___playerNetwork, PickupItem item)
        {
            if (___playerNetwork.IsLocalPlayer)
            {
                foreach (Mod mod in UnityEngine.Object.FindObjectsOfType<Mod>())
                {
                    mod.LocalPlayerEvent_PickupItem(item);
                }
            }
        }
    }

    [HarmonyPatch(typeof(Helper))]
    [HarmonyPatch("DropItem")]
    class Patch_LocalPlayerEvent_DropItem
    {
        static void Postfix(Helper __instance, ItemInstance item, Vector3 position, Vector3 direction, bool parentedToRaft)
        {
            foreach (Mod mod in UnityEngine.Object.FindObjectsOfType<Mod>())
            {
                mod.LocalPlayerEvent_DropItem(item, position, direction, parentedToRaft);
            }
        }
    }
}