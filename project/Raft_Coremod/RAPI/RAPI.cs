﻿using Harmony;
using RaftModLoader;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

public class RAPI
{
    public static string GetModTitle(Type modClass)
    {
        try
        {
            return (modClass.GetCustomAttributes(typeof(ModTitle), true).FirstOrDefault() as ModTitle).ModTitleAttribute;
        }
        catch { return ""; }
    }

    public static string GetModDescription(Type modClass)
    {
        try
        {
            return (modClass.GetCustomAttributes(typeof(ModDescription), true).FirstOrDefault() as ModDescription).ModDescriptionAttribute;
        }
        catch { return ""; }
    }

    public static string GetModAuthor(Type modClass)
    {
        try
        {
            return (modClass.GetCustomAttributes(typeof(ModAuthor), true).FirstOrDefault() as ModAuthor).ModAuthorAttribute;
        }
        catch { return ""; }
    }

    public static string GetModVersion(Type modClass)
    {
        try
        {
            return (modClass.GetCustomAttributes(typeof(ModVersion), true).FirstOrDefault() as ModVersion).ModVersionAttribute;
        }
        catch { return ""; }
    }

    public static string GetModRaftVersion(Type modClass)
    {
        try
        {
            return (modClass.GetCustomAttributes(typeof(RaftVersion), true).FirstOrDefault() as RaftVersion).RaftVersionAttribute;
        }
        catch { return ""; }
    }

    public static string GetModIconUrl(Type modClass)
    {
        try
        {
            return (modClass.GetCustomAttributes(typeof(ModIconUrl), true).FirstOrDefault() as ModIconUrl).ModIconUrlAttribute;
        }
        catch { return ""; }
    }

    public static string GetModWallpaperUrl(Type modClass)
    {
        try
        {
            return (modClass.GetCustomAttributes(typeof(ModWallpaperUrl), true).FirstOrDefault() as ModWallpaperUrl).ModWallpaperUrlAttribute;
        }
        catch { return ""; }
    }

    public static string GetModVersionCheckUrl(Type modClass)
    {
        try
        {
            return (modClass.GetCustomAttributes(typeof(ModVersionCheckUrl), true).FirstOrDefault() as ModVersionCheckUrl).ModVersionCheckUrlAttribute;
        }
        catch { return ""; }
    }

    public static bool GetModIsPermanent(Type modClass)
    {
        try
        {
            return (modClass.GetCustomAttributes(typeof(ModIsPermanent), true).FirstOrDefault() as ModIsPermanent).ModIsPermanentAttribute;
        }
        catch { return false; }
    }

    public static string GetUsernameFromSteamID(CSteamID steamid)
    {
        string username = SteamFriends.GetFriendPersonaName(steamid);
        if (username.ToLower() != "[unknown]")
        {
            return username;
        }
        Network_Player player = ComponentManager<Semih_Network>.Value.GetPlayerFromID(steamid);
        if (player != null)
        {
            return player.playerNameTextMesh.text;
        }
        return username;
    }

    public static void TogglePriorityCursor(bool var)
    {
        if (Semih_Network.InLobbyScene)
            return;
        try
        {
            if (var)
            {
                if (CanvasHelper.ActiveMenu == MenuType.None)
                {
                    Helper.SetCursorVisibleAndLockState(true, CursorLockMode.None);
                    CanvasHelper.ActiveMenu = (MenuType)666;
                    var ch = ComponentManager<CanvasHelper>.Value;
                    ch.SetUIState(true);
                }
                if (CanvasHelper.ActiveMenu == (MenuType)666 && !Cursor.visible)
                {
                    Helper.SetCursorVisibleAndLockState(true, CursorLockMode.None);
                    CanvasHelper.ActiveMenu = (MenuType)666;
                    var ch = ComponentManager<CanvasHelper>.Value;
                    ch.SetUIState(true);
                }
            }
            else
            {
                if (RConsole.isConsoleOpen || MainMenu.IsOpen) { return; }
                if (CanvasHelper.ActiveMenu == (MenuType)666)
                {
                    Helper.SetCursorVisibleAndLockState(false, CursorLockMode.Locked);
                    CanvasHelper.ActiveMenu = MenuType.None;
                    var ch = ComponentManager<CanvasHelper>.Value;
                    ch.SetUIState(true);
                }
            }
        }
        catch { }
    }

    public static void ToggleCursor(bool var)
    {
        if (Semih_Network.InLobbyScene)
            return;
        try
        {
            if (var)
            {
                Helper.SetCursorVisibleAndLockState(true, CursorLockMode.None);
                CanvasHelper.ActiveMenu = MenuType.PauseMenu;
                var ch = ComponentManager<CanvasHelper>.Value;
                ch.SetUIState(true);
            }
            else
            {
                Helper.SetCursorVisibleAndLockState(false, CursorLockMode.Locked);
                CanvasHelper.ActiveMenu = MenuType.None;
                var ch = ComponentManager<CanvasHelper>.Value;
                ch.SetUIState(true);
            }
        }
        catch { }
    }

    public static Network_Player GetLocalPlayer()
    {
        return ComponentManager<Semih_Network>.Value.GetLocalPlayer();
    }

    public static void BroadcastChatMessage(string message)
    {
        ChatManager chatManager = ComponentManager<ChatManager>.Value;
        Semih_Network network = ComponentManager<Semih_Network>.Value;
        CSteamID csteamid = new CSteamID();
        Message_IngameChat nmessage = new Message_IngameChat(Messages.Ingame_Chat_Message, chatManager, csteamid, message);
        network.RPC(nmessage, Target.All, EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
    }

    [Obsolete("RAPI.getLocalPlayer() is deprecated, please use RAPI.GetLocalPlayer() instead!")]
    public static Network_Player getLocalPlayer()
    {
        return ComponentManager<Semih_Network>.Value.GetLocalPlayer();
    }

    public static void GiveItem(Item_Base item, int amount)
    {
        ComponentManager<Semih_Network>.Value.GetLocalPlayer().Inventory.AddItem(item.UniqueName, amount);
    }

    public static void AddItemToBlockQuadType(Item_Base item, RBlockQuadType quadtype)
    {
        string quadtypestring = "blockquadtype/" + quadtype.ToString();
        List<Item_Base> customquadtype = Traverse.Create(Resources.Load<ScriptableObject>(quadtypestring)).Field("acceptableBlockTypes").GetValue<List<Item_Base>>();
        customquadtype.Add(item);
        Traverse.Create(Resources.Load<ScriptableObject>(quadtypestring)).Field("acceptableBlockTypes").SetValue(customquadtype);
    }

    public static void RemoveItemFromBlockQuadType(string itemUniqueName, RBlockQuadType quadtype)
    {
        string quadtypestring = "blockquadtype/" + quadtype.ToString();
        List<Item_Base> customquadtype = Traverse.Create(Resources.Load<ScriptableObject>(quadtypestring)).Field("acceptableBlockTypes").GetValue<List<Item_Base>>();
        foreach (Item_Base i in customquadtype)
        {
            if (i.UniqueName == itemUniqueName)
            {
                customquadtype.Remove(i);
            }
        }
        Traverse.Create(Resources.Load<ScriptableObject>(quadtypestring)).Field("acceptableBlockTypes").SetValue(customquadtype);
    }

    public static void RegisterItem(Item_Base item)
    {
        if (item != null)
        {
            if (item.UniqueIndex >= short.MaxValue)
            {
                Debug.LogError("[RAPI.RegisterNewItem()] Failed! > The item \"" + item.UniqueName + "\" has an invalid UniqueIndex (Needs to be less than " + short.MaxValue + ")!");
                return;
            }
            else
            {
                if (item.MaxUses < 1)
                {
                    Debug.LogError("[RAPI.RegisterNewItem()] Failed! The MaxUses value for item \"" + item.UniqueName + "\" is lower than 1!");
                    return;
                }

                if (!ItemManager.GetItemByIndex(item.UniqueIndex))
                {
                    List<Item_Base> list = Traverse.Create(typeof(ItemManager)).Field("allAvailableItems").GetValue<List<Item_Base>>();
                    list.Add(item);
                    Traverse.Create(typeof(ItemManager)).Field("allAvailableItems").SetValue(list);
                }
                else
                {
                    Debug.LogError("[RAPI.RegisterNewItem()] Failed! > The item \"" + item.UniqueName + "\" can't be registered because the item \"" + ItemManager.GetItemByIndex(item.UniqueIndex).UniqueName + "\" already use that UniqueIndex!");
                    return;
                }
            }
        }
        else
        {
            Debug.LogError("[RAPI.RegisterNewItem()] Failed! > The method has been invoked with a null argument!");
            return;
        }
    }

    public static void SetItemObject(Item_Base item, GameObject prefab, RItemHand parent = RItemHand.rightHand)
    {
        if (item != null)
        {
            if (prefab != null)
            {
                if (!InternalItemAPI.itemObjects.ContainsKey(item))
                {
                    InternalItemAPI.itemObjects.Add(item, new InternalItemAPI.ItemObject(prefab, parent));
                }
                else
                {
                    Debug.LogError("[RAPI.SetItemObject()] Failed! > This item already has an object!");
                    return;
                }
            }
            else
            {
                Debug.LogError("[RAPI.SetItemObject()] Failed! > The method has been invoked with a null argument!");
                return;
            }
        }
        else
        {
            Debug.LogError("[RAPI.SetItemObject()] Failed! > The method has been invoked with a null argument!");
            return;
        }
    }

    public static void SendNetworkMessage(Message message, int channel = 0, EP2PSend ep2psend = EP2PSend.k_EP2PSendReliable, Target target = Target.Other)
    {
        if (Semih_Network.IsHost)
        {
            GetLocalPlayer().Network.RPC(message, target, ep2psend, (NetworkChannel)channel);
        }
        else
        {
            GetLocalPlayer().SendP2P(message, ep2psend, (NetworkChannel)channel);
        }
    }
}

public static class RPlayerExtentions
{
    public static void SendChatMessage(this Network_Player player, string message)
    {
        ChatManager chatManager = ComponentManager<ChatManager>.Value;
        Semih_Network network = ComponentManager<Semih_Network>.Value;
        CSteamID csteamid = new CSteamID();
        Message_IngameChat nmessage = new Message_IngameChat(Messages.Ingame_Chat_Message, chatManager, csteamid, message);
        network.SendP2P(player.steamID, nmessage, EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
    }
}

public enum RBlockQuadType
{
    quad_corner_all_empty,
    quad_corner_inv_empty,
    quad_corner_normal_empty,
    quad_floor,
    quad_floor_empty,
    quad_foundation,
    quad_foundation_empty,
    quad_foundationarmor,
    quad_itemnet,
    quad_itemnet_empty,
    quad_pillar_empty,
    quad_roof_straight_45_inv,
    quad_table, quad_wall,
    quad_walltop_empty
};

public enum RItemHand
{
    leftHand,
    rightHand
};