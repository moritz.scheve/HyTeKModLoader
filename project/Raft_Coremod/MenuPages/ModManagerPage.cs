﻿using ICSharpCode.SharpZipLib.Zip;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
namespace RaftModLoader
{
    public class ModManagerPage : MenuPage
    {
        public static ModManagerPage instance;
        public static List<ModData> modList = new List<ModData>();
        public static GameObject ModListContent;
        public static GameObject ModEntryPrefab;
        public static Button LoadModsBtn;
        public static Button UnloadModsBtn;
        public static Button ModListRefreshBtn;
        public static GameObject ModsGameObjectParent;
        public static bool bypassValueChangeCheckAll;
        public static GameObject modInfoObj;
        public static GameObject noModsText;
        public static Color greenColor = new Color(67.0f / 255, 181.0f / 255, 129.0f / 255);
        public static Color orangeColor = new Color(181.0f / 255, 135.0f / 255, 67.0f / 255);
        public static Color redColor = new Color(181.0f / 255, 67.0f / 255, 67.0f / 255);
        public static Color blueColor = new Color(67.0f / 255, 129.0f / 255, 181.0f / 255);
        public static Color yellowColor = new Color(232.0f / 255, 226.0f / 255, 58.0f / 255);
        public static bool canRefreshModlist = true;

        public static bool firstRefresh = true;
        public static PermanentModsList UserPermanentMods;
        public static PermanentModsList OnStartUserPermanentMods;

        public async void Awake()
        {
            instance = this;
            ModsGameObjectParent = new GameObject();
            ModsGameObjectParent.name = "RMLModsPrefabs";
            DontDestroyOnLoad(ModsGameObjectParent);
            modInfoObj = transform.Find("ModScrollView").Find("Viewport").Find("ModInfo").gameObject;
            modInfoObj.SetActive(false);
            noModsText = transform.Find("ModScrollView").Find("Viewport").Find("RML_FindMods").gameObject;
            noModsText.GetComponent<Button>().onClick.AddListener(() => Application.OpenURL("https://www.raftmodding.com/mods"));
            noModsText.SetActive(false);
            ModEntryPrefab = RML_Main.rmlAssets.LoadAsset<GameObject>("ModEntry");
            ModListContent = GameObject.Find("RMLModManager_ModListContent");
            GameObject.Find("RML_CheckAllToggle").GetComponent<Toggle>().onValueChanged.AddListener(var => SelectAllMods(var));
            LoadModsBtn = GameObject.Find("RML_LoadModsBtn").GetComponent<Button>();
            LoadModsBtn.onClick.AddListener(LoadSelectedMods);
            UnloadModsBtn = GameObject.Find("RML_UnloadModsBtn").GetComponent<Button>();
            UnloadModsBtn.onClick.AddListener(UnloadSelectedMods);
            ModListRefreshBtn = GameObject.Find("RML_RefreshModsBtn").GetComponent<Button>();
            ModListRefreshBtn.onClick.AddListener(() => RefreshMods());
            GameObject.Find("RML_OpenModsFolderBtn").GetComponent<Button>().onClick.AddListener(OpenModsFolder);
            await Task.Delay(1000);
            RefreshMods(true);
        }

        public static void OpenModsFolder()
        {
            System.Diagnostics.Process.Start("explorer.exe", Path.GetFullPath(RML_Main.path_modsFolder));
        }

        public static void RefreshModsStates()
        {
            modList.ForEach(m => RefreshModState(m));
        }

        public static void RefreshModState(ModData md)
        {
            if (md.modinfo.ModlistEntry == null) { return; }
            if (md.modinfo.modState == ModInfo.ModStateEnum.running)
            {
                if (!md.jsonmodinfo.ModIsPermanent)
                {
                    md.modinfo.permanentModWarning.SetActive(false);
                    md.modinfo.unloadBtn.SetActive(true);
                    md.modinfo.loadBtn.SetActive(false);
                    if (OnStartUserPermanentMods.list.Contains(md.modinfo.modFile.Name.ToLower()))
                    {
                        md.modinfo.permanentModWarning.SetActive(true);
                        ColorBlock block = md.modinfo.permanentModWarning.transform.GetComponent<Button>().colors;
                        block.disabledColor = new Color(84.0f / 255.0f, 166.0f / 255.0f, 87.0f / 255.0f, 200.0f / 255.0f);
                        md.modinfo.permanentModWarning.transform.GetComponent<Button>().colors = block;
                        md.modinfo.permanentModWarning.transform.Find("Tooltip").Find("TextMeshPro Text").GetComponent<TextMeshProUGUI>().text = "Loaded at startup";
                    }
                }
                else
                {
                    md.modinfo.unloadBtn.SetActive(false);
                    md.modinfo.loadBtn.SetActive(false);
                    md.modinfo.permanentModWarning.SetActive(true);
                }
            }
            else
            {
                if (!md.jsonmodinfo.ModIsPermanent)
                {
                    md.modinfo.permanentModWarning.SetActive(false);
                    md.modinfo.unloadBtn.SetActive(false);
                    if (md.modinfo.modState != ModInfo.ModStateEnum.errored)
                        md.modinfo.loadBtn.SetActive(true);
                    if (OnStartUserPermanentMods.list.Contains(md.modinfo.modFile.Name.ToLower()))
                    {
                        md.modinfo.permanentModWarning.SetActive(true);
                        ColorBlock block = md.modinfo.permanentModWarning.transform.GetComponent<Button>().colors;
                        block.disabledColor = new Color(84.0f / 255.0f, 166.0f / 255.0f, 87.0f / 255.0f, 200.0f / 255.0f);
                        md.modinfo.permanentModWarning.transform.GetComponent<Button>().colors = block;
                        md.modinfo.permanentModWarning.transform.Find("Tooltip").Find("TextMeshPro Text").GetComponent<TextMeshProUGUI>().text = "Loaded at startup";
                    }
                }
                else
                {
                    md.modinfo.unloadBtn.SetActive(false);
                    md.modinfo.loadBtn.SetActive(false);
                    md.modinfo.permanentModWarning.SetActive(true);
                }
            }

            TextMeshProUGUI Statustext = md.modinfo.ModlistEntry.transform.Find("ModStatusText").GetComponent<TextMeshProUGUI>();
            switch (md.modinfo.modState)
            {
                case ModInfo.ModStateEnum.running:
                    Statustext.text = "Running...";
                    Statustext.color = greenColor;
                    break;
                case ModInfo.ModStateEnum.errored:
                    Statustext.text = "Errored";
                    Statustext.color = redColor;
                    break;
                case ModInfo.ModStateEnum.idle:
                    Statustext.text = "Not loaded";
                    Statustext.color = blueColor;
                    break;
                case ModInfo.ModStateEnum.compiling:
                    Statustext.text = "Compiling...";
                    Statustext.color = yellowColor;
                    break;
                default:
                    Statustext.text = "Unknown";
                    Statustext.color = orangeColor;
                    break;
            }
        }

        public static void LoadSelectedMods()
        {
            foreach (ModData md in modList.ToArray())
            {
                if (md.jsonmodinfo.ModIsPermanent) { continue; }
                if (md.modinfo.ModlistEntry.GetComponentInChildren<Toggle>().isOn)
                {
                    md.modinfo.modHandler.LoadMod(md);
                }
            }
        }

        public static void UnloadSelectedMods()
        {
            foreach (ModData md in modList.ToArray())
            {
                if (md.jsonmodinfo.ModIsPermanent) { continue; }
                if (md.modinfo.ModlistEntry.GetComponentInChildren<Toggle>().isOn)
                {
                    md.modinfo.modHandler.UnloadMod(md);
                }
            }
        }

        public static void SelectAllMods(bool var)
        {
            if (!bypassValueChangeCheckAll)
            {
                foreach (ModData md in modList.ToArray())
                {
                    md.modinfo.ModlistEntry.GetComponentInChildren<Toggle>().isOn = var;
                }
            }
            else
            {
                bypassValueChangeCheckAll = false;
            }
        }

        public static async void RefreshMods(bool ShowNotifications = true)
        {
            if (!canRefreshModlist) { return; }
            canRefreshModlist = false;
            foreach (Transform t in ModsGameObjectParent.transform)
            {
                bool found = false;
                foreach (ModData moddata in modList.ToArray())
                {
                    if (moddata.modinfo.modFile.Name.ToLower() == t.gameObject.name)
                    {
                        found = true;
                    }
                }
                if (!found)
                {
                    Destroy(t.gameObject);
                }
            }
            Dictionary<string, Assembly> assemblyList = new Dictionary<string, Assembly>();
            foreach (ModData m in modList)
            {
                if (m.modinfo.modState != ModInfo.ModStateEnum.running || m.modinfo.assembly == null) { continue; }
                assemblyList.Add(m.modinfo.modFile.Name, m.modinfo.assembly);
            }
            modList.Clear();
            LoadModsBtn.interactable = false;
            UnloadModsBtn.interactable = false;
            noModsText.SetActive(false);
            ModListRefreshBtn.interactable = false;
            foreach (Transform t in ModListContent.transform)
            {
                Destroy(t.gameObject);
            }
            int processedFiles = 0;
            DirectoryInfo d = new DirectoryInfo(Path.Combine(Application.dataPath, "..\\mods"));
            FileInfo[] mods = d.GetFiles("*", SearchOption.TopDirectoryOnly);
            foreach (FileInfo file in mods)
            {
                UnityMainThreadDispatcher.Instance().Enqueue(async () =>
                {
                    try
                    {
                        RefreshMod(file, ShowNotifications, assemblyList);
                    }
                    catch (Exception e)
                    {
                        Debug.LogError("[ModManager] " + file.Name + " > A fatal error occured while loading the mod!\nStacktrace : " + e.ToString());
                    }
                    finally
                    {
                        processedFiles++;
                    }
                });
                await Task.Delay(1);
            }
            while (processedFiles != mods.Count())
            {
                await Task.Delay(1);
            }

            /*List<ModData> sortedMods = new List<ModData>();
            sortedMods = Utils.DependencySort(modList, x => GetModDependencies(x.jsonmodinfo.ModDependencies)).ToList();
            foreach (ModData md in sortedMods)
            {
                if (!ModsGameObjectParent.transform.Find(md.modinfo.modFile.Name.ToLower()) && md.modinfo.modState != ModInfo.ModStateEnum.running)
                {
                    Debug.Log("Loading mod " + md.jsonmodinfo.ModName);
                    await md.modinfo.modHandler.LoadMod(md);
                }
            }*/

            await Task.Delay(1);
            noModsText.SetActive(mods.Count() == 0);
            LoadModsBtn.interactable = true;
            UnloadModsBtn.interactable = true;
            ModListRefreshBtn.interactable = true;
            RefreshModsStates();
            canRefreshModlist = true;
            firstRefresh = false;
        }

        public static async void RefreshMod(FileInfo file, bool ShowNotifications = true, Dictionary<string, Assembly> assemblyList = null)
        {
            if(assemblyList == null)
            {
                assemblyList = new Dictionary<string, Assembly>();
            }
            RNotification notif = null;
            try
            {
                ModData _md = modList.Where(m => m.modinfo.modFile == file).FirstOrDefault();
                if (_md != null && _md.modinfo.ModlistEntry != null)
                {
                    Destroy(_md.modinfo.ModlistEntry);
                    modList.Remove(_md);
                }
                if (ShowNotifications)
                {
                    notif = ComponentManager<RNotify>.Value.AddNotification(RNotify.NotificationType.spinning, "Initializing " + file.Name + "...");
                }
                ModData md = null;
                string filename = file.Name.ToLower();
                if (filename.EndsWith(".rmod.zip"))
                {
                    //md = await new ZipModHandler().GetModData(file, assemblyList);
                }
                else if (filename.EndsWith(".cs") || filename.EndsWith(".lnk"))
                {
                    md = await new RawModHandler().GetModData(file, assemblyList);
                }
                else if (filename.EndsWith(".dll"))
                {
                    md = await new DllModHandler().GetModData(file, assemblyList);
                }
                if (md == null)
                {
                    if (notif != null)
                    {
                        notif.Close();
                    }
                    return;
                }

                md.modinfo.ModlistEntry = Instantiate(ModEntryPrefab, ModListContent.transform.position, ModListContent.transform.rotation, ModListContent.transform);

                if (ModsGameObjectParent.transform.Find(md.modinfo.modFile.Name.ToLower()))
                {
                    md.modinfo.modState = ModInfo.ModStateEnum.running;
                }
                else
                {
                    if (md.modinfo.modState != ModInfo.ModStateEnum.errored)
                    {
                        md.modinfo.modState = ModInfo.ModStateEnum.idle;
                    }
                }

                md.modinfo.ModlistEntry.transform.Find("ModName").GetComponent<TextMeshProUGUI>().text = Utils.StripRichText(md.jsonmodinfo.ModName);
                md.modinfo.ModlistEntry.transform.Find("ModName").GetComponent<TextMeshProUGUI>().color = filename.EndsWith(".lnk") || md.modinfo.modHandler.GetType() == typeof(ZipModHandler) && md.modinfo.modType == ModInfo.ModTypeEnum.lnk ? new Color(0.258f, 0.956f, 0.584f) : Color.white;
                md.modinfo.ModlistEntry.transform.Find("ModAuthor").GetComponent<TextMeshProUGUI>().text = Utils.StripRichText(md.jsonmodinfo.ModAuthor);
                md.modinfo.ModlistEntry.transform.Find("ModVersionText").GetComponent<TextMeshProUGUI>().text = Utils.StripRichText(md.jsonmodinfo.ModVersion);
                md.modinfo.buttons = md.modinfo.ModlistEntry.transform.Find("Buttons").gameObject;
                md.modinfo.infoBtn = md.modinfo.buttons.transform.Find("ModInfoBtn").gameObject;
                md.modinfo.infoBtnTooltip = md.modinfo.buttons.transform.Find("ModInfoBtn").Find("Tooltip").gameObject;
                md.modinfo.infoBtn.AddComponent<TooltipHandler>().tooltip = md.modinfo.infoBtnTooltip;
                md.modinfo.permanentModWarning = md.modinfo.buttons.transform.Find("PermanentModWarning").gameObject;
                md.modinfo.permanentTooltip = md.modinfo.buttons.transform.Find("PermanentModWarning").Find("Tooltip").gameObject;
                md.modinfo.permanentModWarning.AddComponent<TooltipHandler>().tooltip = md.modinfo.permanentTooltip;
                md.modinfo.loadBtn = md.modinfo.buttons.transform.Find("LoadModBtn").gameObject;
                md.modinfo.loadBtnTooltip = md.modinfo.buttons.transform.Find("LoadModBtn").Find("Tooltip").gameObject;
                md.modinfo.loadBtn.AddComponent<TooltipHandler>().tooltip = md.modinfo.loadBtnTooltip;
                md.modinfo.unloadBtn = md.modinfo.buttons.transform.Find("UnloadModBtn").gameObject;
                md.modinfo.unloadBtnTooltip = md.modinfo.buttons.transform.Find("UnloadModBtn").Find("Tooltip").gameObject;
                md.modinfo.unloadBtn.AddComponent<TooltipHandler>().tooltip = md.modinfo.unloadBtnTooltip;
                md.modinfo.versionTooltip = md.modinfo.ModlistEntry.transform.Find("ModVersionText").Find("VersionTooltip").gameObject;

                md.modinfo.ModlistEntry.transform.Find("ModVersionText").gameObject.AddComponent<TooltipHandler>().tooltip = md.modinfo.versionTooltip;

                md.modinfo.infoBtnTooltip.SetActive(false);
                md.modinfo.loadBtnTooltip.SetActive(false);
                md.modinfo.unloadBtnTooltip.SetActive(false);
                md.modinfo.versionTooltip.SetActive(false);
                md.modinfo.permanentTooltip.SetActive(false);

                md.modinfo.permanentModWarning.SetActive(false);
                md.modinfo.loadBtn.SetActive(false);
                md.modinfo.unloadBtn.SetActive(false);
                Utils.DownloadCachedTexture(md.jsonmodinfo.ModIconUrl).ContinueWith((t) =>
                {
                    UnityMainThreadDispatcher.Instance().Enqueue(() =>
                    {
                        try
                        {
                            md.modinfo.ModIcon = (Texture2D)t.Result;
                            md.modinfo.ModlistEntry.transform.Find("ModIconMask").Find("ModIcon").GetComponent<RawImage>().texture = t.Result;
                        }
                        catch { }
                    });
                });
                Utils.DownloadCachedTexture(md.jsonmodinfo.ModBannerUrl).ContinueWith((t) =>
                {
                    UnityMainThreadDispatcher.Instance().Enqueue(() =>
                    {
                        try
                        {
                            md.modinfo.ModBanner = (Texture2D)t.Result;
                        }
                        catch { }
                    });
                });

                Utils.GetModVersion(md.jsonmodinfo.ModVersionCheckUrl).ContinueWith((t) =>
                {
                    UnityMainThreadDispatcher.Instance().Enqueue(() =>
                    {
                        try
                        {
                            if (t.Result == "Unknown" || t.Result == "")
                            {
                                md.modinfo.versionTooltip.GetComponentInChildren<TextMeshProUGUI>().text = "Unknown";
                                md.modinfo.ModlistEntry.transform.Find("ModVersionText").GetComponent<TextMeshProUGUI>().color = orangeColor;
                                md.modinfo.versionTooltip.GetComponentInChildren<TextMeshProUGUI>().color = orangeColor;
                            }
                            else
                            {
                                if (t.Result != md.jsonmodinfo.ModVersion)
                                {
                                    md.modinfo.versionTooltip.GetComponentInChildren<TextMeshProUGUI>().text = "New version available!";
                                    Debug.LogWarning("[ModManager] " + md.jsonmodinfo.ModName + " > The current installed version is outdated! A new version is available! (" + t.Result + ")");
                                    ComponentManager<RNotify>.Value.AddNotification(RNotify.NotificationType.scaling, "<color=#41f46b>" + md.jsonmodinfo.ModName + " has a new version available!</color>", 5, RML_Main.rmlAssets.LoadAsset<Sprite>("DownloadIcon"));
                                    md.modinfo.ModlistEntry.transform.Find("ModVersionText").GetComponent<TextMeshProUGUI>().color = redColor;
                                    md.modinfo.versionTooltip.GetComponentInChildren<TextMeshProUGUI>().color = redColor;
                                }
                                else
                                {
                                    md.modinfo.versionTooltip.GetComponentInChildren<TextMeshProUGUI>().text = "Up to date!";
                                    md.modinfo.ModlistEntry.transform.Find("ModVersionText").GetComponent<TextMeshProUGUI>().color = greenColor;
                                    md.modinfo.versionTooltip.GetComponentInChildren<TextMeshProUGUI>().color = greenColor;
                                }
                            }
                        }
                        catch { }
                    });
                });

                md.modinfo.unloadBtn.GetComponent<Button>().onClick.AddListener(() => md.modinfo.modHandler.UnloadMod(md));
                md.modinfo.loadBtn.GetComponent<Button>().onClick.AddListener(() => md.modinfo.modHandler.LoadMod(md));
                md.modinfo.infoBtn.GetComponent<Button>().onClick.AddListener(() =>
                {
                    ShowModInfo(md);
                });

                if (ShowNotifications)
                {
                    if (md.modinfo.modState == ModInfo.ModStateEnum.idle)
                    {
                        notif.Close();
                    }
                    else if (md.modinfo.modState == ModInfo.ModStateEnum.running)
                    {
                        notif.Close();
                    }
                    else
                    {
                        notif.SetText(md.jsonmodinfo.ModName + " errored!").SetNormal().SetIcon(RNotify.ErrorSprite).SetCloseDelay(5);
                    }
                }

                if (md.jsonmodinfo.ModIsPermanent || (OnStartUserPermanentMods.list.Contains(md.modinfo.modFile.Name.ToLower()) && firstRefresh))
                {
                    md.modinfo.modHandler.LoadMod(md);
                }

                RefreshModState(md);
                modList.Add(md);
            }
            catch (Exception e)
            {
                Debug.LogError("[ModManager] " + file.Name + " > A fatal error occured while loading the mod!\nStacktrace : " + e.ToString());
                if (notif != null)
                    notif.Close();
            }
            finally
            {
                if (notif != null)
                    notif.Close();
            }
        }

        public static ModData[] GetModDependencies(List<string> b)
        {
            List<ModData> dependencies = new List<ModData>();

            foreach (string a in b)
            {
                foreach (ModData md in modList.ToArray())
                {
                    if (md.jsonmodinfo.ModPackage == a.ToLower())
                    {
                        dependencies.Add(md);
                    }
                }
            }
            return dependencies.ToArray();
        }

        public static void ShowModInfo(ModData md)
        {
            modInfoObj.transform.Find("ModName").GetComponent<TextMeshProUGUI>().text = md.jsonmodinfo.ModName;
            modInfoObj.transform.Find("Author").GetComponent<TextMeshProUGUI>().text = "Author : " + md.jsonmodinfo.ModAuthor;
            modInfoObj.transform.Find("ModVersion").GetComponent<TextMeshProUGUI>().text = "Version : " + md.jsonmodinfo.ModVersion;
            modInfoObj.transform.Find("RaftVersion").GetComponent<TextMeshProUGUI>().text = "Raft Version : " + md.jsonmodinfo.RaftVersion;
            modInfoObj.transform.Find("BannerMask").Find("Banner").GetComponent<RawImage>().texture = md.modinfo.ModBanner;
            modInfoObj.transform.Find("BannerMask").Find("IconMask").Find("Icon").GetComponent<RawImage>().texture = md.modinfo.ModIcon;
            modInfoObj.transform.Find("Description").GetComponent<TextMeshProUGUI>().text = "Description : \n\n" + md.jsonmodinfo.ModDescription;
            modInfoObj.transform.Find("MakePermanent").gameObject.SetActive(!md.jsonmodinfo.ModIsPermanent);
            modInfoObj.transform.Find("MakePermanent").GetComponent<Toggle>().onValueChanged.RemoveAllListeners();
            modInfoObj.transform.Find("MakePermanent").GetComponent<Toggle>().isOn = UserPermanentMods.list.Contains(md.modinfo.modFile.Name.ToLower());
            modInfoObj.transform.Find("MakePermanent").GetComponent<Toggle>().onValueChanged.AddListener((val) =>
            {
                SetModPermanent(val, md.modinfo.modFile.Name.ToLower());
            });
            modInfoObj.SetActive(true);
        }

        public static void SetModPermanent(bool val, string fileName)
        {
            if (val)
            {
                if (!UserPermanentMods.list.Contains(fileName))
                {
                    UserPermanentMods.list.Add(fileName);
                }
            }
            else
            {
                if (UserPermanentMods.list.Contains(fileName))
                {
                    UserPermanentMods.list.Remove(fileName);
                }
            }

            PlayerPrefs.SetString("rmlSettings_UserPermanentMods", JsonUtility.ToJson(UserPermanentMods));
        }
    }

    [Serializable]
    public class ModData
    {
        public JsonModInfo jsonmodinfo = new JsonModInfo();
        public ModInfo modinfo = new ModInfo();
    }

    [Serializable]
    public class JsonModInfo
    {
        public string ModName;
        public string ModPackage;
        public string ModDescription;
        public string ModAuthor;
        public string ModVersion;
        public string RaftVersion;
        public string ModIconUrl;
        public string ModBannerUrl;
        public string ModVersionCheckUrl;
        public bool ModIsPermanent = false;
        public List<string> ModDependencies = new List<string>();
    }

    [Serializable]
    public class ModInfo
    {
        public BaseModHandler modHandler;
        public FileInfo modFile;
        public string fileHash;
        public GameObject ModlistEntry;
        public Assembly assembly;
        public string assemblyFile;
        public Dictionary<string, byte[]> modFiles = new Dictionary<string, byte[]>();

        public ModTypeEnum modType;
        public enum ModTypeEnum
        {
            cs,
            dll,
            lnk,
        }

        public ModStateEnum modState;
        public enum ModStateEnum
        {
            idle,
            running,
            errored,
            unknown,
            compiling
        }

        public Texture2D ModIcon;
        public Texture2D ModBanner;
        public GameObject buttons;
        public GameObject infoBtn;
        public GameObject loadBtn;
        public GameObject unloadBtn;
        public GameObject permanentModWarning;
        public GameObject permanentTooltip;
        public GameObject infoBtnTooltip;
        public GameObject loadBtnTooltip;
        public GameObject unloadBtnTooltip;
        public GameObject versionTooltip;
    }

    [Serializable]
    public class PermanentModsList
    {
        public List<string> list = new List<string>();
    }

}