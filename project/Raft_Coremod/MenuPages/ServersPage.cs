﻿using Harmony;
using RaftModLoader;
using SocketIO;
using Steamworks;
using System;
using System.Collections;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
namespace RaftModLoader
{
    public class ServersPage : MenuPage
    {
        public static ServersPage instance;
        public GameObject serverInfoObj;
        public GameObject serverInfoPluginEntry;
        public static GameObject serverlistWaitWindow;
        public static Button serverlistRefreshBtn;
        public static GameObject serverlistContent;
        public static GameObject ServerEntryPrefab;
        public static TextMeshProUGUI serverlistStatus;
        public static TextMeshProUGUI serversAmount;
        public static TextMeshProUGUI serversAmountTooltip;
        public static string lastPassword = "";
        public static string lastServerInfoBackgroundUrl = "";
        public static string searchServersValue = "";
        public static Sprite defaultIcon;
        public static Sprite defaultBanner;

        public void Awake()
        {
            instance = this;
            Semih_Network.OnJoinResult += OnJoinResult; // REPLACE THAT WITH AN HARMONY PATCH.

            ServerEntryPrefab = RML_Main.rmlAssets.LoadAsset<GameObject>("ServerEntry");
            serverlistContent = GameObject.Find("RMLServerListContent").gameObject;
            serverlistWaitWindow = transform.Find("ServerScrollView").Find("Viewport").Find("ConnectWindow").gameObject;
            serverlistWaitWindow.transform.Find("CloseBtn").GetComponent<Button>().onClick.AddListener(() =>
            {
                CustomLoadingScreen.loadingscreen_background.GetComponent<RawImage>().texture = CustomLoadingScreen.defaultBackground;
                ComponentManager<Semih_Network>.Value.CancelTryingToConnect();
                serverlistWaitWindow.SetActive(false);
            });

            serverlistStatus = transform.Find("ServerScrollView").Find("Viewport").Find("StatusText").GetComponent<TextMeshProUGUI>();

            transform.Find("KnowMoreRDS").GetComponent<Button>().onClick.AddListener(() => Application.OpenURL("https://www.raftmodding.com"));
            serverlistWaitWindow.SetActive(false);
            serverlistRefreshBtn = transform.Find("RML_RefreshServerList").GetComponent<Button>();
            serverlistRefreshBtn.onClick.AddListener(RefreshServerList);
            serverInfoObj = transform.Find("ServerScrollView").Find("Viewport").Find("ServerInfo").gameObject;
            transform.Find("SearchBar").GetComponent<TMP_InputField>().onValueChanged.AddListener((val) => OnSearchServersInputChanged(val));
            transform.Find("SearchBar").GetComponent<TMP_InputField>().onEndEdit.AddListener((val) => OnSearchServersInputChanged(val));
            serverInfoObj.SetActive(false);
            serverInfoPluginEntry = RML_Main.rmlAssets.LoadAsset<GameObject>("ServerInfoPluginEntry");
            serversAmount = GameObject.Find("RMLMainMenuLButton_Servers").transform.Find("Amount").Find("AmountValue").GetComponent<TextMeshProUGUI>();
            serversAmountTooltip = GameObject.Find("RMLMainMenuLButton_Servers").transform.Find("Amount").Find("Tooltip").GetComponentInChildren<TextMeshProUGUI>();
            serversAmount.gameObject.AddComponent<TooltipHandler>().tooltip = GameObject.Find("RMLMainMenuLButton_Servers").transform.Find("Amount").Find("Tooltip").gameObject;
            SceneManager.activeSceneChanged += OnSceneChanged;
            defaultIcon = RML_Main.rmlAssets.LoadAsset<Sprite>("defaultIcon");
            defaultBanner = RML_Main.rmlAssets.LoadAsset<Sprite>("defaultBanner");
        }

        public static void OnSearchServersInputChanged(string val)
        {
            searchServersValue = val;
            foreach (Transform t in serverlistContent.transform)
            {
                t.gameObject.SetActive(Utils.StripRichText(t.Find("Name").GetComponent<TextMeshProUGUI>().text).ToLower().Contains(val.ToLower()));
            }
        }

        public static async void RefreshServerList()
        {
            while (RSocket.socket == null) { await Task.Delay(100); }
            if (RSocket.socket.IsConnected && RSocket.socket.IsWsConnected)
            {
                serverlistStatus.text = "Retrieving servers...";
                RSocket.socket.Emit("getServerList");
                serverlistRefreshBtn.interactable = false;
                await Task.Delay(1000);
                serverlistRefreshBtn.interactable = true;
            }
            else
            {
                serverlistStatus.text = "The MasterServer is currently offline.\nPlease try again later!";
            }
        }

        public IEnumerator WaitUntilPlayerLeave(CSteamID id)
        {
            while (ComponentManager<Semih_Network>.Value.GetPlayerFromID(id) != null)
            {
                yield return new WaitForSeconds(1);
            }
            RSocket.updateServerStatus();
        }

        public void ReceiveServerlist(SocketIOEvent e)
        {
            serverlistStatus.text = "Initializing serverlist...";
            transform.Find("SearchBar").GetComponent<TMP_InputField>().text = "";
            foreach (Transform t in serverlistContent.transform)
            {
                Destroy(t.gameObject);
            }
            try
            {
                serverlistStatus.text = "";
                int serversAmount = e.data["servers"].Count;
                if (serversAmount <= 0)
                {
                    serverlistStatus.text = "No servers found!";
                }
                for (int i = 0; i < serversAmount; i++)
                {
                    try
                    {
                        GameObject item = Instantiate(ServerEntryPrefab, Vector3.zero, Quaternion.identity, serverlistContent.transform);

                        string serverName = e.data["servers"][i]["serverName"].str.RML_InsertEmoji();
                        string serverPlayers = "Players : " + e.data["servers"][i]["serverPlayers"] + "/" + e.data["servers"][i]["serverMaxPlayers"];
                        string serverGamemode = "Gamemode : " + e.data["servers"][i]["serverGamemode"].str;
                        string serverFriendlyFire = "Friendly Fire : " + (e.data["servers"][i]["serverFriendlyFire"].b ? "Yes" : "No");
                        string dedicatedServer = "Dedicated : " + (e.data["servers"][i]["dedicatedServer"].b ? "Yes" : "No");
                        string ping = "Ping : " + e.data["servers"][i]["serverPing"] + "ms";
                        string pluginslist = e.data["servers"][i]["serverPlugins"].str;
                        bool hasPassword = e.data["servers"][i]["serverHasPassword"].b;

                        if (!e.data["servers"][i]["dedicatedServer"].b)
                        {
                            serverName += "'s party";
                        }

                        item.transform.Find("Name").GetComponent<TextMeshProUGUI>().text = serverName;
                        item.transform.Find("Players").GetComponent<TextMeshProUGUI>().text = e.data["servers"][i]["serverPlayers"] + "/" + e.data["servers"][i]["serverMaxPlayers"];
                        item.transform.Find("Ping").GetComponent<TextMeshProUGUI>().text = e.data["servers"][i]["serverPing"] + "ms";

                        string iconUrl = e.data["servers"][i]["serverIconUrl"].str;
                        string bannerUrl = e.data["servers"][i]["serverBannerUrl"].str;
                        string backgroundUrl = e.data["servers"][i]["serverCustomLoadingScreenUrl"].str;
                        lastServerInfoBackgroundUrl = backgroundUrl;
                        item.transform.Find("Buttons").Find("InfoBtn").gameObject.AddComponent<TooltipHandler>().tooltip = item.transform.Find("Buttons").Find("InfoBtn").Find("Tooltip").gameObject;
                        item.transform.Find("Buttons").Find("ConnectBtn").gameObject.AddComponent<TooltipHandler>().tooltip = item.transform.Find("Buttons").Find("ConnectBtn").Find("Tooltip").gameObject;
                        if (!string.IsNullOrWhiteSpace(iconUrl) && iconUrl.Length > 5)
                        {
                            Utils.DownloadUncachedTexture(iconUrl).ContinueWith((t) =>
                            {
                                item.transform.Find("ServerIconMask").Find("ServerIcon").GetComponent<RawImage>().texture = t.Result;
                                item.transform.Find("_icon").GetComponent<RawImage>().texture = t.Result;
                            });
                        }
                        else
                        {
                            item.transform.Find("_icon").GetComponent<RawImage>().texture = defaultIcon.texture;
                        }

                        if (!string.IsNullOrWhiteSpace(bannerUrl) && bannerUrl.Length > 5)
                        {
                            Utils.DownloadUncachedTexture(bannerUrl).ContinueWith((t) =>
                            {
                                item.transform.Find("_banner").GetComponent<RawImage>().texture = t.Result;
                            });
                        }
                        else
                        {
                            item.transform.Find("_banner").GetComponent<RawImage>().texture = defaultBanner.texture;
                        }

                        item.transform.Find("Buttons").Find("InfoBtn").GetComponent<Button>().onClick.AddListener(() =>
                        {
                            if (serverlistWaitWindow.activeSelf) { return; }
                            instance.serverInfoObj.transform.Find("Name").GetComponent<TextMeshProUGUI>().text = serverName;
                            instance.serverInfoObj.transform.Find("Players").GetComponent<TextMeshProUGUI>().text = serverPlayers;
                            instance.serverInfoObj.transform.Find("Gamemode").GetComponent<TextMeshProUGUI>().text = serverGamemode;
                            instance.serverInfoObj.transform.Find("Friendlyfire").GetComponent<TextMeshProUGUI>().text = serverFriendlyFire;
                            instance.serverInfoObj.transform.Find("Dedicated").GetComponent<TextMeshProUGUI>().text = dedicatedServer;
                            instance.serverInfoObj.transform.Find("Ping").GetComponent<TextMeshProUGUI>().text = ping;
                            instance.serverInfoObj.transform.Find("BannerMask").Find("Banner").GetComponent<RawImage>().texture = item.transform.Find("_banner").GetComponent<RawImage>().texture;
                            instance.serverInfoObj.transform.Find("IconMask").Find("Icon").GetComponent<RawImage>().texture = item.transform.Find("_icon").GetComponent<RawImage>().texture;

                            string[] plugins = pluginslist.Split(new string[] { ";" }, StringSplitOptions.None);
                            foreach (Transform t in instance.serverInfoObj.transform.Find("PluginsList").Find("Scroll View").Find("Viewport").Find("Content"))
                            {
                                Destroy(t.gameObject);
                            }
                            if (!string.IsNullOrWhiteSpace(pluginslist) && plugins.Length > 0)
                            {
                                instance.serverInfoObj.transform.Find("PluginsList").Find("Scroll View").Find("Viewport").Find("NoPlugins").GetComponent<TextMeshProUGUI>().text = "";
                                foreach (string plugin in plugins)
                                {
                                    GameObject plobj = Instantiate(instance.serverInfoPluginEntry, Vector3.zero, Quaternion.identity, instance.serverInfoObj.transform.Find("PluginsList").Find("Scroll View").Find("Viewport").Find("Content"));
                                    plobj.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = plugin;
                                }
                            }
                            else
                            {
                                instance.serverInfoObj.transform.Find("PluginsList").Find("Scroll View").Find("Viewport").Find("NoPlugins").GetComponent<TextMeshProUGUI>().text = "This server isn't running any plugins.";
                            }
                            instance.serverInfoObj.SetActive(true);
                        });

                        uint accountid = 0;
                        uint.TryParse(e.data["servers"][i]["serverId"].str, out accountid);
                        item.transform.Find("Buttons").Find("ConnectBtn").GetComponent<Button>().onClick.AddListener(() =>
                        {
                            if (serverlistWaitWindow.activeSelf) { return; }
                            instance.serverInfoObj.SetActive(false);
                            serverlistWaitWindow.transform.Find("Password").Find("TextMeshPro - InputField").GetComponent<TMP_InputField>().text = "";
                            if (!string.IsNullOrWhiteSpace(lastServerInfoBackgroundUrl) && lastServerInfoBackgroundUrl.Length > 5)
                            {
                                string current = lastServerInfoBackgroundUrl;
                                Utils.DownloadUncachedTexture(lastServerInfoBackgroundUrl).ContinueWith((t) =>
                                {
                                    if (lastServerInfoBackgroundUrl == current && lastServerInfoBackgroundUrl != "")
                                    {
                                        CustomLoadingScreen.loadingscreen_background.GetComponent<RawImage>().texture = t.Result;
                                    }
                                });
                            }
                            else
                            {
                                CustomLoadingScreen.loadingscreen_background.GetComponent<RawImage>().texture = CustomLoadingScreen.defaultBackground;
                            }
                            serverlistWaitWindow.transform.Find("BannerMask").Find("Banner").GetComponent<RawImage>().texture = item.transform.Find("_banner").GetComponent<RawImage>().texture; ;
                            serverlistWaitWindow.transform.Find("IconMask").Find("Icon").GetComponent<RawImage>().texture = item.transform.Find("_icon").GetComponent<RawImage>().texture;
                            if (hasPassword)
                            {
                                serverlistWaitWindow.transform.Find("Connecting").Find("ServerName").GetComponent<TextMeshProUGUI>().text = "Connecting to " + serverName;
                                serverlistWaitWindow.transform.Find("Password").Find("ServerName").GetComponent<TextMeshProUGUI>().text = "Connecting to " + serverName;
                                serverlistWaitWindow.transform.Find("Connecting").gameObject.SetActive(false);
                                serverlistWaitWindow.transform.Find("Error").gameObject.SetActive(false);
                                serverlistWaitWindow.transform.Find("Password").gameObject.SetActive(true);
                                serverlistWaitWindow.transform.Find("Password").Find("Button").GetComponent<Button>().onClick.RemoveAllListeners();
                                serverlistWaitWindow.transform.Find("Password").Find("Button").GetComponent<Button>().onClick.AddListener(() =>
                                {
                                    serverlistWaitWindow.transform.Find("Connecting").gameObject.SetActive(true);
                                    serverlistWaitWindow.transform.Find("Error").gameObject.SetActive(false);
                                    serverlistWaitWindow.transform.Find("Password").gameObject.SetActive(false);
                                    CSteamID csteamid = new CSteamID(new AccountID_t(accountid), EUniverse.k_EUniversePublic, EAccountType.k_EAccountTypeIndividual);
                                    serverlistWaitWindow.transform.Find("Connecting").Find("ServerName").GetComponent<TextMeshProUGUI>().text = "Connecting to " + serverName;
                                    serverlistWaitWindow.transform.Find("Password").Find("ServerName").GetComponent<TextMeshProUGUI>().text = "Connecting to " + serverName;
                                    ConnectToServer(csteamid, serverlistWaitWindow.transform.Find("Password").Find("TextMeshPro - InputField").GetComponent<TMP_InputField>().text, true);
                                });
                                serverlistWaitWindow.SetActive(true);
                            }
                            else
                            {
                                serverlistWaitWindow.transform.Find("Connecting").gameObject.SetActive(true);
                                serverlistWaitWindow.transform.Find("Error").gameObject.SetActive(false);
                                serverlistWaitWindow.transform.Find("Password").gameObject.SetActive(false);
                                CSteamID csteamid = new CSteamID(new AccountID_t(accountid), EUniverse.k_EUniversePublic, EAccountType.k_EAccountTypeIndividual);
                                serverlistWaitWindow.transform.Find("Connecting").Find("ServerName").GetComponent<TextMeshProUGUI>().text = "Connecting to " + serverName;
                                serverlistWaitWindow.transform.Find("Password").Find("ServerName").GetComponent<TextMeshProUGUI>().text = "Connecting to " + serverName;
                                ConnectToServer(csteamid, "", true);
                                serverlistWaitWindow.SetActive(true);
                            }
                        });
                    }
                    catch (Exception ex)
                    {
                        Debug.Log("An error occured while retrieving a server in the serverlist!\n" + ex.ToString());
                    }
                }
            }
            catch
            {
                foreach (Transform t in serverlistContent.transform)
                {
                    Destroy(t.gameObject);
                }
                serverlistStatus.text = "An error occured while fetching servers";
            }
        }

        public static void ConnectToServer(CSteamID sid, string password, bool showUI)
        {
            if (!Semih_Network.InLobbyScene)
            {
                RConsole.LogWarning("You are already playing on a server! Please disconnect before joining another server!");
                serverlistWaitWindow.SetActive(showUI);
                serverlistWaitWindow.transform.Find("Connecting").gameObject.SetActive(false);
                serverlistWaitWindow.transform.Find("Password").gameObject.SetActive(false);
                serverlistWaitWindow.transform.Find("Error").gameObject.SetActive(true);
                serverlistWaitWindow.transform.Find("Error").Find("Status").GetComponent<TextMeshProUGUI>().text = "You are already playing on a server!";
                return;
            }
            ComponentManager<Semih_Network>.Value.CancelTryingToConnect();
            string username = SteamFriends.GetFriendPersonaName(sid);
            if (password != "")
            {
                RConsole.Log("Connecting to " + username + " with the specified password...");
            }
            else
            {
                RConsole.Log("Connecting to " + username + "...");
            }

            ComponentManager<Semih_Network>.Value.TryToJoinGame(sid, password);
            if (showUI)
            {
                serverlistWaitWindow.SetActive(true);
            }
        }

        private void OnSceneChanged(Scene scene1, Scene scene2)
        {
            Semih_Network.OnJoinResult += OnJoinResult;
        }

        public static void OnJoinResult(CSteamID remoteID, InitiateResult result)
        {
            if (!serverlistWaitWindow.activeSelf)
            {
                return;
            }
            serverlistWaitWindow.transform.Find("Connecting").gameObject.SetActive(false);
            serverlistWaitWindow.transform.Find("Password").gameObject.SetActive(false);
            serverlistWaitWindow.transform.Find("Error").gameObject.SetActive(false);
            TextMeshProUGUI TextMeshProUGUI = serverlistWaitWindow.transform.Find("Error").Find("Status").GetComponent<TextMeshProUGUI>();
            serverlistWaitWindow.transform.Find("Password").Find("TextMeshPro - InputField").GetComponent<TMP_InputField>().text = "";
            switch (result)
            {
                case InitiateResult.Fail_AllowFriends:
                    serverlistWaitWindow.transform.Find("Error").gameObject.SetActive(true);
                    TextMeshProUGUI.text = "The server doesn't accept players!";
                    break;
                case InitiateResult.Fail_TimeOut:
                    serverlistWaitWindow.transform.Find("Error").gameObject.SetActive(true);
                    TextMeshProUGUI.text = "Server Timed Out!";
                    break;
                case InitiateResult.Fail:
                    serverlistWaitWindow.transform.Find("Error").gameObject.SetActive(true);
                    TextMeshProUGUI.text = "An unknown error occured!";
                    break;
                case InitiateResult.Fail_NotHost:
                    serverlistWaitWindow.transform.Find("Error").gameObject.SetActive(true);
                    TextMeshProUGUI.text = "The server is full!";
                    break;
                case InitiateResult.Fail_AlreadyOnServer:
                    serverlistWaitWindow.transform.Find("Error").gameObject.SetActive(true);
                    TextMeshProUGUI.text = "You are already playing on the server!";
                    break;
                case InitiateResult.Fail_AlreadyConnecting:
                    serverlistWaitWindow.transform.Find("Error").gameObject.SetActive(true);
                    TextMeshProUGUI.text = "You are already connecting to this server!";
                    break;
                case InitiateResult.Fail_MissmatchAppBuildID:
                    serverlistWaitWindow.transform.Find("Error").gameObject.SetActive(true);
                    TextMeshProUGUI.text = "You don't have the same Raft Version as the server!";
                    break;
                case InitiateResult.Fail_WrongPassword:
                    serverlistWaitWindow.transform.Find("Password").gameObject.SetActive(true);
                    serverlistWaitWindow.transform.Find("Password").Find("Button").GetComponent<Button>().onClick.RemoveAllListeners();
                    serverlistWaitWindow.transform.Find("Password").Find("Button").GetComponent<Button>().onClick.AddListener(() =>
                    {
                        serverlistWaitWindow.transform.Find("Connecting").gameObject.SetActive(true);
                        serverlistWaitWindow.transform.Find("Password").gameObject.SetActive(false);
                        ConnectToServer(remoteID, serverlistWaitWindow.transform.Find("Password").Find("TextMeshPro - InputField").GetComponent<TMP_InputField>().text, true);
                        serverlistWaitWindow.SetActive(true);
                    });
                    break;
                case InitiateResult.Fail_NotFriendWithHost:
                    serverlistWaitWindow.transform.Find("Error").gameObject.SetActive(true);
                    TextMeshProUGUI.text = "The server is whitelisted!";
                    break;
                case InitiateResult.Fail_AllowNone:
                    serverlistWaitWindow.transform.Find("Error").gameObject.SetActive(true);
                    TextMeshProUGUI.text = "The server doesn't accept players!";
                    break;
                case InitiateResult.Success:
                    Traverse.Create(typeof(Semih_Network)).Field("isHost").SetValue(false);
                    Traverse.Create(ComponentManager<SaveAndLoad>.Value).Field("autoSaveTimer").SetValue(0);
                    TextMeshProUGUI.text = "Downloading World...";
                    serverlistWaitWindow.SetActive(false);
                    MainMenu.CloseMenu();
                    break;
                case (InitiateResult)666:
                    serverlistWaitWindow.transform.Find("Error").gameObject.SetActive(true);
                    TextMeshProUGUI.text = "You are permanently banned from this server!";
                    break;
                default:
                    TextMeshProUGUI.text = result.ToString();
                    break;
            }
        }
    }

    [HarmonyPatch(typeof(Semih_Network))]
    [HarmonyPatch("OnSceneLoaded")]
    class Patch_OnSceneLoaded
    {
        static async void Prefix(Semih_Network __instance, Scene scene)
        {
            if (scene.name == __instance.gameSceneName)
            {
                while (!AssetBundleManager.IsAllScenesLoaded)
                {
                    await Task.Delay(100);
                }
                await Task.Delay(250);
                if (Semih_Network.IsHost && Semih_Network.CurrentRequestJoinAuthSetting == RequestJoinAuthSetting.ALLOW_ALL)
                {
                    RSocket.updateServerStatus();
                }
            }
        }
    }

    [HarmonyPatch(typeof(Semih_Network))]
    [HarmonyPatch("AddPlayer")]
    [HarmonyPatch(new Type[] { typeof(CSteamID), typeof(RGD_Settings_Character) })]
    public class ServerSystem_AddPlayer
    {
        public static void Prefix(Semih_Network __instance, CSteamID steamID, RGD_Settings_Character characterSettings)
        {
            if (Semih_Network.IsHost && Semih_Network.CurrentRequestJoinAuthSetting == RequestJoinAuthSetting.ALLOW_ALL)
            {
                RSocket.updateServerStatus();
            }
        }
    }

    [HarmonyPatch(typeof(Semih_Network))]
    [HarmonyPatch("RemovePlayer")]
    public class ServerSystem_RemovePlayer
    {
        public static void Prefix(CSteamID id)
        {
            if (Semih_Network.IsHost && Semih_Network.CurrentRequestJoinAuthSetting == RequestJoinAuthSetting.ALLOW_ALL)
            {
                RSocket instance = ComponentManager<RSocket>.Value;
                instance.StartCoroutine(ServersPage.instance.WaitUntilPlayerLeave(id));
            }
        }
    }

    [HarmonyPatch(typeof(Semih_Network))]
    [HarmonyPatch("LeaveGame")]
    public class ServerSystem_LeaveGame
    {
        public static void Postfix()
        {
            RSocket.updateServerStatus();
        }
    }

    [HarmonyPatch(typeof(Semih_Network))]
    [HarmonyPatch("OnP2PSessionConnectFail")]
    public class ServerSystem_OnP2PSessionConnectFail
    {
        public static void Prefix(P2PSessionConnectFail_t callback)
        {
            if (ServersPage.serverlistWaitWindow.activeSelf)
            {
                ServersPage.OnJoinResult(new CSteamID(), InitiateResult.Fail_TimeOut);
            }

        }
    }
}