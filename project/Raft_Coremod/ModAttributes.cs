﻿using System;
using System.Collections.Generic;
using System.Linq;

public static class ModAttributes
{
    public static bool HasDefaultConstructor(this Type t)
    {
        return t.IsValueType || t.GetConstructor(Type.EmptyTypes) != null;
    }

    public static List<string> GetModInfo(this Type type)
    {
        List<string> infos = new List<string>();

        ModTitle modTitle = type.GetCustomAttributes(typeof(ModTitle), true).FirstOrDefault() as ModTitle;
        ModDescription modDescription = type.GetCustomAttributes(typeof(ModDescription), true).FirstOrDefault() as ModDescription;
        ModAuthor modAuthor = type.GetCustomAttributes(typeof(ModAuthor), true).FirstOrDefault() as ModAuthor;
        ModVersion modVersion = type.GetCustomAttributes(typeof(ModVersion), true).FirstOrDefault() as ModVersion;
        RaftVersion raftVersion = type.GetCustomAttributes(typeof(RaftVersion), true).FirstOrDefault() as RaftVersion;
        ModIconUrl modIconUrl = type.GetCustomAttributes(typeof(ModIconUrl), true).FirstOrDefault() as ModIconUrl;
        ModWallpaperUrl modWallpaperUrl = type.GetCustomAttributes(typeof(ModWallpaperUrl), true).FirstOrDefault() as ModWallpaperUrl;
        ModVersionCheckUrl modVersionCheckUrl = type.GetCustomAttributes(typeof(ModVersionCheckUrl), true).FirstOrDefault() as ModVersionCheckUrl;
        ModIsPermanent modIsPermanent = type.GetCustomAttributes(typeof(ModIsPermanent), true).FirstOrDefault() as ModIsPermanent;
        if (modTitle != null)
        {
            infos.Add(modTitle.ModTitleAttribute);
        }
        else
        {
            infos.Add("Unknown");
        }
        if (modDescription != null)
        {
            infos.Add(modDescription.ModDescriptionAttribute);
        }
        else
        {
            infos.Add("Unknown");
        }
        if (modAuthor != null)
        {
            infos.Add(modAuthor.ModAuthorAttribute);
        }
        else
        {
            infos.Add("Unknown");
        }
        if (modVersion != null)
        {
            infos.Add(modVersion.ModVersionAttribute);
        }
        else
        {
            infos.Add("Unknown");
        }
        if (raftVersion != null)
        {
            infos.Add(raftVersion.RaftVersionAttribute);
        }
        else
        {
            infos.Add("Unknown");
        }
        if (modIconUrl != null)
        {
            infos.Add(modIconUrl.ModIconUrlAttribute);
        }
        else
        {
            infos.Add("Unknown");
        }
        if (modWallpaperUrl != null)
        {
            infos.Add(modWallpaperUrl.ModWallpaperUrlAttribute);
        }
        else
        {
            infos.Add("Unknown");
        }
        if (modVersionCheckUrl != null)
        {
            infos.Add(modVersionCheckUrl.ModVersionCheckUrlAttribute);
        }
        else
        {
            infos.Add("Unknown");
        }
        if (modIsPermanent != null)
        {
            infos.Add(modIsPermanent.ModIsPermanentAttribute.ToString());
        }
        else
        {
            infos.Add("Unknown");
        }
        return infos;
    }
}

[AttributeUsage(AttributeTargets.Class)]
public class ModTitle : Attribute
{
    public string ModTitleAttribute { get; private set; }

    public ModTitle(string value)
    {
        this.ModTitleAttribute = value;
    }
}

[AttributeUsage(AttributeTargets.Class)]
public class ModDescription : Attribute
{
    public string ModDescriptionAttribute { get; private set; }

    public ModDescription(string value)
    {
        this.ModDescriptionAttribute = value;
    }
}

[AttributeUsage(AttributeTargets.Class)]
public class ModAuthor : Attribute
{
    public string ModAuthorAttribute { get; private set; }

    public ModAuthor(string value)
    {
        this.ModAuthorAttribute = value;
    }
}

[AttributeUsage(AttributeTargets.Class)]
public class ModVersion : Attribute
{
    public string ModVersionAttribute { get; private set; }

    public ModVersion(string value)
    {
        this.ModVersionAttribute = value;
    }
}

[AttributeUsage(AttributeTargets.Class)]
public class RaftVersion : Attribute
{
    public string RaftVersionAttribute { get; private set; }

    public RaftVersion(string value)
    {
        this.RaftVersionAttribute = value;
    }
}

[AttributeUsage(AttributeTargets.Class)]
public class ModIconUrl : Attribute
{
    public string ModIconUrlAttribute { get; private set; }

    public ModIconUrl(string value)
    {
        this.ModIconUrlAttribute = value;
    }
}

[AttributeUsage(AttributeTargets.Class)]
public class ModWallpaperUrl : Attribute
{
    public string ModWallpaperUrlAttribute { get; private set; }

    public ModWallpaperUrl(string value)
    {
        this.ModWallpaperUrlAttribute = value;
    }
}

[AttributeUsage(AttributeTargets.Class)]
public class ModVersionCheckUrl : Attribute
{
    public string ModVersionCheckUrlAttribute { get; private set; }

    public ModVersionCheckUrl(string value)
    {
        this.ModVersionCheckUrlAttribute = value;
    }
}

[AttributeUsage(AttributeTargets.Class)]
public class ModIsPermanent : Attribute
{
    public bool ModIsPermanentAttribute { get; private set; }

    public ModIsPermanent(bool value)
    {
        this.ModIsPermanentAttribute = value;
    }
}