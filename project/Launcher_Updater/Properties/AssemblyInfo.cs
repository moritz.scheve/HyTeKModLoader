﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("HyTeKModLoader Launcher Updater")]
[assembly: AssemblyDescription("This software installs HyTeKModLoader for you and keep it up to date!")]
[assembly: AssemblyCompany("https://www.hytekgames.net")]
[assembly: AssemblyProduct("HyTeK Mod Loader Launcher Updater")]
[assembly: AssemblyCopyright("Copyright HyTeKGames © 2020")]
[assembly: AssemblyTrademark("HyTeKGames")]
[assembly: Guid("2218f273-55d6-4977-9844-4173e97f76bf")]
[assembly: AssemblyVersion("2.0.0.0")]
[assembly: AssemblyFileVersion("2.0.0.0")]
