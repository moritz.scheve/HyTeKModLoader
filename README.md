# **HyTeK Mod Loader**

**This work is licensed under a Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.**

[Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License](https://creativecommons.org/licenses/by-nc-nd/4.0/)

![Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License](https://i.imgur.com/TxZglnD.png)

# **Recommended Tools/Softwares :**
- Visual Studio 2019
- Windows 10 OS (PostBuild Events won't work on Linux).

# **This repository consists of 2 main folders :**
- The **build** folder where the launcher updater executable and the launcher core executable are being built into.
- The **project** folder which contains **THE ENTIRE SOURCECODE** of the LauncherUpdater, Launcher Core, The Injected Assembly, The GreenHell Coremod Assembly & The Raft Coremod Assembly. Everything is in the **HyTeKModLoader.sln** solution file.

# **How to build it?**
- Clone the repository and open the **HyTeKModLoader.sln** solution file.
- Verify if all projects references are found, If its not the case, fix them. DM me on discord TeK#6884 for more info if you can't fix them.
- Select the build configuration that you want to build as shown below.
 
![Selecting the build configuration that you want to build](https://i.imgur.com/1eiW6KA.png)
- After selecting the build configuration simply right click the whole solution and build it as shown below. (I recommend to always click "Rebuild Solution" instead of "Build Solution" so everything is always kept up to date!)

![Building the solution using the selected build configuration](https://i.imgur.com/FGwCuBT.png)
- If the build has succeeded you should see an output similar to the one shown below.

![Succeeded Build Output](https://i.imgur.com/q16QrpM.png)

# **What happens during the build process?**
- The AssemblyLoader.dll gets copied to the Launcher project so its always kept up to date.
- The game coremods dll's are copied into their respective location (%Appdata%/RaftModLoader/binaries/coremod.dll and %Appdata%/GreenHellModLoader/binaries/coremod.dll)
- The code is pre processed to match the current build configuration. Thats why in the code you can see **#IF GAME_IS_RAFT** or **#IF GAME_IS_GREENHELL**
- The build process changes the icon of the LauncherUpdater and its name to match the current game/build configuration using a software named **ResourceHacker** that is located in **project/Resources/ResourceHacker.exe**
- As windows is shit, its also calling the explorer to clear the icon cache so it instantly update, using the ie4uinit_clone.exe file located in **project/Resources/ie4uinit_clone.exe**

**If you have any issue setuping the project or understanding something DM me on discord TeK#6884.
I hope i explained everything fine :D**

Again, remember that **this work is licensed under a Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.**

[Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License](https://creativecommons.org/licenses/by-nc-nd/4.0/)